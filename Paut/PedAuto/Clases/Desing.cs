﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Net;
using System.Net.Mail;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Drawing;

namespace PedAuto.Clases
{
   public  class Desing
    {
        public void ShowAsParentForm(Form myFrm, Form Parent, string Titulo)
        {
            myFrm.Text = Titulo;
            myFrm.MdiParent = Parent;
            myFrm.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            myFrm.Show();
        }


        public void ShowAsForm(Form myFrm, string Titulo)
        {
            myFrm.Text = Titulo;
            myFrm.ShowDialog();
        }

        public void SendMessage(string Message, string Tipo)
        {
            switch (Tipo)
            {
                case "1":
                    MessageBox.Show(Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case "2":
                    MessageBox.Show(Message, "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case "3":
                    MessageBox.Show(Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    break;
                default:
                    break;
            }
        }

        public string GetIp(string ComputerName)
        {
            System.Net.IPHostEntry myIPs = System.Net.Dns.GetHostEntry(ComputerName);
            foreach (IPAddress myIP in myIPs.AddressList)
            {
                if (myIP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    return myIP.ToString();
                }
            }

            return null;
        }

        public void LimpiarGrupo(Control Grp)
        {
            foreach (Control Ctl in Grp.Controls)
            {
                switch (Ctl.GetType().Name)
                {
                    case "MaskedTextBox":
                        ((MaskedTextBox)Ctl).Clear();
                        break;
                    case "TextBox":
                        ((TextBox)Ctl).Clear();
                        break;
                }
            }

        }

        public string String2Num(int Decimals, string str)
        {
            NumberFormatInfo nfi = new CultureInfo("es-SV", false).NumberFormat;
            nfi.NumberDecimalDigits = Decimals;
            nfi.NumberGroupSeparator = "";

            try
            {
                return System.Convert.ToDouble(str, nfi).ToString("N", nfi);
            }
            catch
            {
                return System.Convert.ToDouble("0", nfi).ToString("N", nfi); ;
            }
        }

        public bool ValidarEnGrupo(Control Grp)
        {
            foreach (Control Ctl in Grp.Controls)
            {              
                switch (Ctl.GetType().Name)
                {
                    case "MaskedTextBox":
                        if (((MaskedTextBox)Ctl).Text.Equals(string.Empty))
                        {                            
                            ((MaskedTextBox)Ctl).Focus();
                            return false;
                        }                       
                        break;
                    case "ComboBox":
                        if (((ComboBox)Ctl).Text.Equals(string.Empty))
                        {                          
                            ((ComboBox)Ctl).Focus();
                            return false;
                        }                        
                        break;
                    case "TextBox":
                        if (((TextBox)Ctl).Text.Equals(string.Empty))
                        {                            
                            ((TextBox)Ctl).Focus();
                            return false;
                        }                      
                        break;
                }                
            }
            return true;
        }


        public bool BuscarLINQ(string TextoABuscar, string Columna, DataGridView grid)
        {
            bool encontrado = false;
            if (TextoABuscar == string.Empty) return false;
            if (grid.RowCount == 0) return false;
            grid.ClearSelection();
            if (Columna == string.Empty)
            {
                IEnumerable<DataGridViewRow> obj = (from DataGridViewRow row in grid.Rows.Cast<DataGridViewRow>()
                                                    from DataGridViewCell cells in row.Cells
                                                    where cells.OwningRow.Equals(row) && cells.Value.ToString().Trim() == TextoABuscar
                                                    select row);
                if (obj.Any())
                {
                    grid.Rows[obj.FirstOrDefault().Index].Selected = true;
                    return true;
                }

            }
            else
            {
                IEnumerable<DataGridViewRow> obj = (from DataGridViewRow row in grid.Rows.Cast<DataGridViewRow>()
                                                    where row.Cells[Columna].Value.ToString().Trim() == TextoABuscar
                                                    select row);
                if (obj.Any())
                {
                    grid.Rows[obj.FirstOrDefault().Index].Selected = true;
                    return true;
                }

            }
            return encontrado;
        }

        public bool BuscarLINQ(string TextoABuscar, string Columna, DataGridView grid, int color)
        {
            bool encontrado = false;
            if (TextoABuscar == string.Empty) return false;
            if (grid.RowCount == 0) return false;
            grid.ClearSelection();
            if (Columna == string.Empty)
            {
                IEnumerable<DataGridViewRow> obj = (from DataGridViewRow row in grid.Rows.Cast<DataGridViewRow>()
                                                    select row);
                if (obj.Any())
                {
                    if (color == 1)
                    {
                        grid.Rows[obj.FirstOrDefault().Index].DefaultCellStyle.ForeColor = Color.CornflowerBlue;
                    }
                    else
                    {
                        grid.Rows[obj.FirstOrDefault().Index].Selected = true;
                    }
                    return true;
                }
            }
            else
            {
                IEnumerable<DataGridViewRow> obj = (from DataGridViewRow row in grid.Rows.Cast<DataGridViewRow>()
                                                    where row.Cells[Columna].Value.ToString().Trim() == TextoABuscar
                                                    select row);
                if (obj.Any())
                {
                    if (color == 1)
                    {
                        grid.Rows[obj.FirstOrDefault().Index].DefaultCellStyle.ForeColor = Color.CornflowerBlue;
                    }
                    else
                    {
                        grid.Rows[obj.FirstOrDefault().Index].Selected = true;
                    }
                    return true;
                }
            }
            return encontrado;
        }

        public void ValidaNum(KeyPressEventArgs e)
        {

            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
                {
                    e.Handled = false;
                }
                else
                {
                    //el resto de teclas pulsadas se desactivan 
                    e.Handled = true;
                }

        }

    }//fin clase
}
