﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Data;

namespace PedAuto.Clases
{
    public class Mail
    {
        #region Properties
        public string host { get; set; }
        public string usermail { get; set; }
        public string passwordmail { get; set; }
        #endregion

        /// <summary>
        /// Crea una nueva instancia y obtiene las credenciales del servidor para poder enviar correos
        /// </summary>
        public Mail()
        {
            Initialize();
        }

        /// <summary>
        /// Inicializa las propiedades a la cuenta principal de envio de correos
        /// </summary>
        public void Initialize()
        {
            DB db = new DB();
            DataTable dt = db.ExecuteQuery("SELECT Servidor, usuario, contraseña FROM [GLOBAL].[dbo].[In_Config_Correos_Servidor] WHERE IdServidor = 1");
            if (dt.Rows.Count > 0)
            {
                this.host = dt.Rows[0]["Servidor"].ToString().Trim();
                this.usermail = dt.Rows[0]["usuario"].ToString().Trim();
                this.passwordmail = dt.Rows[0]["contraseña"].ToString().Trim();
            }
        }

        /// <summary>
        /// Envia correo a la lista indicada que esta configurada en [GLOBAL].[dbo].[In_Config_Correos]
        /// </summary>
        /// <param name="lista">Lista a enviar el correo</param>
        /// <param name="emailsubject">Asunto</param>
        /// <param name="emailbody">Correo</param>
        /// <returns>Falso/Verdadero</returns>
        public bool SendToList(string lista, string emailsubject, string emailbody)
        {
            Dictionary<string, string> paramts = new Dictionary<string, string>();
            DB db = new DB();

            string query = "SELECT Correos, IdServidor FROM [GLOBAL].[dbo].[In_Config_Correos] WHERE CodConfCorreo = @lista";
            paramts["@lista"] = lista;
            DataTable dt = db.ExecuteQuery(query, paramts);

            if (dt.Rows.Count > 0)
            {
                string correos = dt.Rows[0]["Correos"].ToString();

                paramts = new Dictionary<string, string>();
                paramts["@id"] = dt.Rows[0]["IdServidor"].ToString();
                dt = db.ExecuteQuery("SELECT Servidor, usuario, contraseña FROM [GLOBAL].[dbo].[In_Config_Correos_Servidor] WHERE IdServidor = @id", paramts);
                if (dt.Rows.Count > 0)
                {
                    this.host = dt.Rows[0]["Servidor"].ToString().Trim();
                    this.usermail = dt.Rows[0]["usuario"].ToString().Trim();
                    this.passwordmail = dt.Rows[0]["contraseña"].ToString().Trim();
                }
                else
                {
                    return false;
                }
                return this.Send(correos, emailsubject, emailbody);
            }
            else
            {
                return false;
            }
        }

        public bool SendToList2(string lista, string listaCC, string listaBCC, string emailsubject, string emailbody)
        {
            Dictionary<string, string> paramts = new Dictionary<string, string>();
            Dictionary<string, string> paramtsCC = new Dictionary<string, string>();
            Dictionary<string, string> paramtsBCC = new Dictionary<string, string>();
            DB db = new DB();

            string query = "SELECT Correos, IdServidor FROM [GLOBAL].[dbo].[In_Config_Correos] WHERE CodConfCorreo = @lista";
            paramts["@lista"] = lista;
            DataTable dt = db.ExecuteQuery(query, paramts);

            string queryCC = "SELECT Correos, IdServidor FROM [GLOBAL].[dbo].[In_Config_Correos] WHERE CodConfCorreo = @lista";
            paramtsCC["@lista"] = listaCC;
            DataTable dtCC = db.ExecuteQuery(queryCC, paramtsCC);

            string queryBCC = "SELECT Correos, IdServidor FROM [GLOBAL].[dbo].[In_Config_Correos] WHERE CodConfCorreo = @lista";
            paramtsBCC["@lista"] = listaBCC;
            DataTable dtBCC = db.ExecuteQuery(queryBCC, paramtsBCC);

            if (dt.Rows.Count > 0 && dtCC.Rows.Count > 0 && dtBCC.Rows.Count > 0)
            {
                string correos = dt.Rows[0]["Correos"].ToString();
                string correosCC = dtCC.Rows[0]["Correos"].ToString();
                string correosBCC = dtBCC.Rows[0]["Correos"].ToString();

                paramts = new Dictionary<string, string>();
                paramts["@id"] = dt.Rows[0]["IdServidor"].ToString();
                dt = db.ExecuteQuery("SELECT Servidor, usuario, contraseña FROM [GLOBAL].[dbo].[In_Config_Correos_Servidor] WHERE IdServidor = @id", paramts);
                if (dt.Rows.Count > 0)
                {
                    this.host = dt.Rows[0]["Servidor"].ToString().Trim();
                    this.usermail = dt.Rows[0]["usuario"].ToString().Trim();
                    this.passwordmail = dt.Rows[0]["contraseña"].ToString().Trim();
                }
                else
                {
                    return false;
                }
                return this.Send2(correos,correosCC,correosBCC, emailsubject, emailbody);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Envio de correos
        /// </summary>
        /// <param name="emailaddress">Direccion de correo</param>
        /// <param name="emailsubject">Asunto</param>
        /// <param name="emailbody">Cuerpo del correo</param>
        /// <returns>Falso/Verdadero</returns>
        public bool Send(string emailaddress, string emailsubject, string emailbody)
        {
            return this.Send(emailaddress, emailsubject, emailbody, true);
        }

        public bool Send2(string emailaddress, string emailaddressCC, string emailaddressBcc, string emailsubject, string emailbody)
        {
            return this.Send2(emailaddress, emailaddressCC, emailaddressBcc,emailsubject, emailbody, true);
        }

        /// <summary>
        /// Envio de correos
        /// </summary>
        /// <param name="emailaddress">Direccion de correo</param>
        /// <param name="emailsubject">Asunto</param>
        /// <param name="emailbody">Cuerpo del correo</param>
        /// <param name="isHTML">Si el cuerpo es Html</param>
        /// <returns>Falso/Verdadero</returns>
        /// 

        public bool Send(string emailaddress, string emailsubject, string emailbody, bool isHTML)
        {
            bool resultado = false;
            if (this.host.Equals("") || this.usermail.Equals("") || this.passwordmail.Equals("") || emailaddress.Equals(""))
                return resultado;

            // Cabecera de Correo
            MailMessage email = new MailMessage();

            string[] mails = emailaddress.Split(',');
            foreach (string correo in mails)
            {
                email.To.Add(new MailAddress(correo));
            }

            email.From = new MailAddress(this.usermail);
            //email.CC = 

            // Cuerpo de Correo
            email.Subject = emailsubject;
            email.Body = emailbody;
            email.IsBodyHtml = isHTML;
            email.Priority = MailPriority.Normal;

            // Credenciales del Servidor
            SmtpClient smtp = new SmtpClient();
            smtp.Host = this.host;
            smtp.EnableSsl = false;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(this.usermail, this.passwordmail);

            try
            {
                smtp.Send(email);                
                email.Dispose();
                resultado = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                resultado = false;
            }

            return resultado;
        }


        public bool Send2(string emailaddress,string emailaddressCC,string emailaddressCO, string emailsubject, string emailbody, bool isHTML)
        {
            bool resultado = false;
            if (this.host.Equals("") || this.usermail.Equals("") || this.passwordmail.Equals("") || emailaddress.Equals(""))
                return resultado;

            // Cabecera de Correo
            MailMessage email = new MailMessage();

            string[] mails = emailaddress.Split(',');
            foreach (string correo in mails)
            {
                email.To.Add(new MailAddress(correo));
            }

            if (emailaddressCC != "")
            {
                string[] mailsCC = emailaddressCC.Split(',');
                foreach (string correoCC in mailsCC)
                {
                    email.CC.Add(new MailAddress(correoCC));
                }
            }

            if (emailaddressCO != "")
            {
                string[] mailsBcc = emailaddressCO.Split(',');
                foreach (string correoBcc in mailsBcc)
                {
                    email.Bcc.Add(new MailAddress(correoBcc));
                }
            }

            email.From = new MailAddress(this.usermail);
            

            // Cuerpo de Correo
            email.Subject = emailsubject;
            email.Body = emailbody;
            email.IsBodyHtml = isHTML;
            email.Priority = MailPriority.Normal;

            // Credenciales del Servidor
            SmtpClient smtp = new SmtpClient();
            smtp.Host = this.host;
            smtp.EnableSsl = false;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(this.usermail, this.passwordmail);

            try
            {
                smtp.Send(email);
                email.Dispose();
                resultado = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                resultado = false;
            }

            return resultado;
        }

    }
}