﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace PedAuto.Clases
{
   public  class DB
    {
        #region Properties

        public string server { get; set; }
        public string user { get; set; }
        public string password { get; set; }
        public string db { get; set; }
        public string DataBase = "Intrade";

        public SqlConnection connection;

        #endregion

        #region General
        public DB()
        {
            this.server = Properties.Settings.Default.autServer;
            this.db = Properties.Settings.Default.autDataBase;
            this.user = Properties.Settings.Default.autUser;
            this.password = Properties.Settings.Default.autPass;
            //this.server = @"192.0.0.106\MSSQL2012";
            //this.db = "GLOBAL";
            //this.user = "sa";
            //this.password = "pats";

        }

        public bool connect(string servi,string bdc , string usu, string pass)
        {
            bool boo = false;
            try
            {
                string sc;
                sc = "data source = " + servi + ";" +
                     "initial catalog = " + bdc + ";" +
                     "user = " + usu + ";" +
                     "password = " + pass + ";";

                connection = new SqlConnection(sc);
                connection.Open();
                boo = true;
            }
            catch (Exception)
            {


                //No se pudo conectar a la base de Datos
                //No se pudo asignar el valor a boo de true
                //Se retornara false indicando que la conexion no se pudo realizar
            }
            return boo;
        }
        public bool connect()
        {
            bool boo = false;
            try
            {
                string sc;
                sc = "data source = " + server + ";" +
                     "initial catalog = " + db + ";" +
                     "user = " + user + ";" +
                     "password = " + password + ";";

                connection = new SqlConnection(sc);
                connection.Open();
                boo = true;
            }
            catch (Exception)
            {


                //No se pudo conectar a la base de Datos
                //No se pudo asignar el valor a boo de true
                //Se retornara false indicando que la conexion no se pudo realizar
            }
            return boo;
        }

        public bool disconect()
        {
            if (connection.State.Equals(ConnectionState.Open))
            {
                connection.Close();
            }
            return true;
        }

        private SqlDbType getDBType(string type)
        {
            SqlDbType sdt;
            switch (type.ToLower())
            {
                case "varchar":
                    sdt = SqlDbType.VarChar;
                    break;
                case "int":
                    sdt = SqlDbType.Int;
                    break;
                case "money":
                    sdt = SqlDbType.Money;
                    break;
                case "datetime":
                    sdt = SqlDbType.DateTime;
                    break;
                case "decimal":
                    sdt = SqlDbType.Decimal;
                    break;
                default:
                    sdt = SqlDbType.VarChar;
                    break;
                case "date":
                    sdt = SqlDbType.Date;
                    break;
            }

            return sdt;
        }

        #endregion



        #region DB Interaction

        public DataTable ExecuteQuery(string query)
        {
            DataTable dt = new DataTable("Datos");
            if (this.connect())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(query, connection);
                    SqlDataReader rd = cmd.ExecuteReader();
                    dt.Load(rd);
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                    dt = new DataTable("datos");
                }
                this.disconect();
            }
            return dt;
        }

        public DataTable ExecuteQuery(string query, Dictionary<string, string> paramts)
        {
            DataTable dt = new DataTable("Datos");
            if (this.connect())
            {
                SqlCommand cmd = new SqlCommand(query, connection);
                try
                {
                    //Llenando parametros
                    if (paramts != null)
                    {
                        foreach (KeyValuePair<string, string> pair in paramts)
                        {
                            SqlParameter param;
                            param = new SqlParameter();
                            param.ParameterName = pair.Key.ToString();
                            param.Value = pair.Value.ToString();
                            cmd.Parameters.Add(param);
                        }
                    }
                    SqlDataReader rd = cmd.ExecuteReader();
                    dt.Load(rd);
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                    dt = new DataTable("Datos");
                }
                this.disconect();
            }
            return dt;
        }

        public DataTable ExecuteProcedure(string procedure)
        {
            DataTable dt = new DataTable("Datos");
            if (this.connect())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = this.connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = procedure;
                try
                {
                    SqlDataReader rd = cmd.ExecuteReader();
                    dt.Load(rd);
                }
                catch (Exception e)
                {
                    Console.Write(e.ToString());
                    dt = new DataTable("Datos");
                }
                this.disconect();
            }
            return dt;
        }

        public DataTable ExecuteProcedure(string procedure, Dictionary<string, string> paramts)
        {
            DataTable dt = new DataTable("Datos");
            if (this.connect())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = this.connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = procedure;
                try
                {
                    //Llenando parametros
                    if (paramts != null)
                    {
                        foreach (KeyValuePair<string, string> pair in paramts)
                        {
                            string[] value_type = pair.Value.ToString().Split('|');
                            if (value_type.Length == 2)
                            {
                                cmd.Parameters.Add(pair.Key.ToString(), getDBType(value_type[1])).Value = value_type[0];
                            }
                            else
                            {
                                return new DataTable();
                            }
                        }
                    }
                    SqlDataReader rd = cmd.ExecuteReader();
                    dt.Load(rd);
                }
                catch (Exception e)
                {
                    Console.Write(e.ToString());
                    dt = new DataTable("Datos");
                }
                this.disconect();
            }
            return dt;
        }


        public bool ExecuteNoResult(string query)
        {
            int n = 0;
            if (this.connect())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(query, connection);
                    n = cmd.ExecuteNonQuery();
                    this.disconect();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    n = 0;
                }
            }

            if (n > 0)
                return true;
            else
                return false;
        }

        public bool ExecuteNoResult(string query, Dictionary<string, string> paramts)
        {
            int n = 0;
            if (this.connect())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(query, connection);
                    //Llenando parametros
                    if (paramts != null)
                    {
                        foreach (KeyValuePair<string, string> pair in paramts)
                        {
                            SqlParameter param;
                            param = new SqlParameter();
                            param.ParameterName = pair.Key.ToString();
                            param.Value = (pair.Value != null) ? pair.Value : "";
                            cmd.Parameters.Add(param);
                        }
                    }
                    n = cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    n = 0;
                }
                this.disconect();
            }

            if (n > 0)
                return true;
            else
                return false;
        }
        public bool ExecuteProcedureNR(string procedure)
        {
            bool result = false;
            if (this.connect())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = this.connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = procedure;
                try
                {
                    int n = cmd.ExecuteNonQuery();

                    if (n == -1)
                        result = true;
                }
                catch (Exception e)
                {
                    Console.Write(e.ToString());
                    return result;
                }
                this.disconect();
            }
            return result;
        }

        public bool ExecuteProcedureNR_1(string procedure, Dictionary<string, string> paramts)
        {
            bool result = false;
            if (this.connect())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = this.connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = procedure;
                try
                {
                    //Llenando parametros
                    if (paramts != null)
                    {
                        foreach (KeyValuePair<string, string> pair in paramts)
                        {
                            string[] value_type = pair.Value.ToString().Split('|');
                            if (value_type.Length == 2)
                            {
                                cmd.Parameters.Add(pair.Key.ToString(), getDBType(value_type[1])).Value = value_type[0];
                            }
                            else
                            {
                                return result;
                            }
                        }
                        int n = cmd.ExecuteNonQuery();
                        if (n == -1) ;
                    }
                    //int n = cmd.ExecuteNonQuery();
                    //if (n == -1) ;
                   
                        result = true;
                }
                catch (Exception e)
                {
                    Console.Write(e.ToString());
                    return result;
                }
                this.disconect();
            }
            return result;
        }



        #endregion

    }
}
