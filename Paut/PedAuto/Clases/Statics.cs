﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;

namespace PedAuto.Clases
{
   public  class Statics
    {
        #region Properties
       
        public static DataTable TbAccesos; 

        public static string usuario = "";
        public static string empresa = "";
        public static string Empresa = "";
        public static string owner = "";
        public static string usuario_sucursal = ""; 

        public static string nombresuc = "";
        public static string cliente = "";
        public static int maxlineas = 14;
        public static string sociedad = "";

        public static string cod_dis = "";
        public static string cod_proveedor = "";
        public static string sucursal = "";
        public static string bodega = "";
        
        public static string conjunto = "";     
        public static int i = 0;

        //public static string pantalla1 = "";
        public static string pantalla_consulta = "";
        public static string pantalla_configuracion = "";
        public static string pantalla_paut = "";
        public static string pantalla_registro = "";
        public static string pantalla_excluir = "";
                  
        #endregion

       public static string codsuc_ususario(string user)
       {
           usuario_sucursal = "";
           DB db = new DB();
           DataTable tb_suc = db.ExecuteQuery(string.Format("SELECT E.UBICACION "+
                                                               "FROM "+
	                                                           "     GLOBAL.dbo.Grl_Seg_Usuario AS U "+
	                                                           "    INNER JOIN GLOBAL.dbo.Empleados as E on U.codemp = E.CODEMP "+
                                                               "WHERE "+
	                                                                "U.usuario = '{0}' ", user));

           if (tb_suc.Rows.Count > 0)
           {
               usuario_sucursal = tb_suc.Rows[0][0].ToString().Trim();
           }

           return usuario_sucursal;
       }


       #region Conexiones
       // coneccion sucursal
 

        #endregion

        #region Metodos


       public bool BuscarLINQ(string TextoABuscar, string Columna, DataGridView grid)
       {
           bool encontrado = false;
           if (TextoABuscar == string.Empty) return false;
           if (grid.RowCount == 0) return false;
           grid.ClearSelection();
           if (Columna == string.Empty)
           {
               IEnumerable<DataGridViewRow> obj = (from DataGridViewRow row in grid.Rows.Cast<DataGridViewRow>()
                                                   from DataGridViewCell cells in row.Cells
                                                   where cells.OwningRow.Equals(row) && cells.Value.ToString().Trim() == TextoABuscar
                                                   select row);
               if (obj.Any())
               {
                   grid.Rows[obj.FirstOrDefault().Index].Selected = true;
                   return true;
               }

           }
           else
           {
               IEnumerable<DataGridViewRow> obj = (from DataGridViewRow row in grid.Rows.Cast<DataGridViewRow>()
                                                   where row.Cells[Columna].Value.ToString().Trim() == TextoABuscar
                                                   select row);
               if (obj.Any())
               {
                   grid.Rows[obj.FirstOrDefault().Index].Selected = true;
                   return true;
               }

           }
           return encontrado;
       }

       public static string MakeHTML(string texto, DataTable dt)
       {
           StringBuilder sb = new StringBuilder();

           sb.AppendLine("<div style='font-family:Calibri'>" + texto + "</div><br />");
           sb.AppendLine("<table style='border:solid 1px black; font-family:Calibri'>");
           sb.Append("<tr style='border:solid 1px white;background-color: black; color:#FFFFFF'>");
           // Cabeceras.
           foreach (DataColumn dc in dt.Columns)
           { sb.AppendFormat("<th>{0}</th>", dc.ColumnName.Replace('_', ' ')); }

           sb.AppendLine("</tr>");
           // Datos 
           foreach (DataRow dr in dt.Rows)
           {
               sb.Append("<tr>");
               foreach (DataColumn dc in dt.Columns)
               {
                   string cellValue = (dr[dc] != null) ? dr[dc].ToString() : "";
                   sb.AppendFormat("<td style='border:solid 1px black'>{0}</td>", cellValue);
               }
               sb.AppendLine("</tr>");
           }
           sb.AppendLine("</table>");

           sb.AppendLine("<hr>");
           sb.AppendLine("Registros: <strong>" + dt.Rows.Count + "</strong>");
           sb.AppendLine("&nbsp;&nbsp;|&nbsp;&nbsp;<i>Este correo es automatico, fue generado a la fecha: " + DateTime.Now + " Por: Pedidos Automaticos (PAUT) </i>");

           return sb.ToString();
       }

        #endregion
    }
}
