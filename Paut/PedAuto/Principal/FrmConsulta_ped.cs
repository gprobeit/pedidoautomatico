﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PedAuto.Clases;

namespace PedAuto.Formularios
{
    public partial class FrmConsulta_ped : Form
    {
        public FrmConsulta_ped()
        {
            InitializeComponent();
        }

        #region var

            public Desing _Desing = new Desing();
            public DB _Sql = new DB();
            public string table;
        #endregion

        #region Metodos
                  
            public void control_pantalla(string pantalla)
            {
                try
                {
                    string query = string.Format("exec Paut_spControlAcceso 2, '" + Clases.Statics.usuario + "',  '" + pantalla + "', '' ");
                    _Sql.ExecuteNoResult(query);

                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }

            private void cargarSuc()
            {
                try
                {
                    DataTable TbSuc = new DataTable();

                    if (Clases.Statics.sociedad == "VS")
                    {
                        TbSuc = _Sql.ExecuteQuery(string.Format(" SELECT DISTINCT CODSUC,NOMSUC,CONJUNTO  FROM Paut_VwSucursal WHERE TIPO='VTA' and CONJUNTO IN (" + Clases.Statics.conjunto + ")"));
                    }
                    else if (Clases.Statics.sociedad == "I")
                {
                        TbSuc = _Sql.ExecuteQuery(string.Format(" SELECT DISTINCT CODSUC,NOMSUC,CONJUNTO  FROM Paut_VwSucursal WHERE TIPO='VTA' AND CONJUNTO IN (" + Clases.Statics.conjunto + ")"));
                    }
                      else
                    {
                    TbSuc = _Sql.ExecuteQuery(string.Format(" SELECT DISTINCT CODSUC,NOMSUC,CONJUNTO  FROM Paut_VwSucursal WHERE TIPO='VTA' AND CONJUNTO IN (" + Clases.Statics.conjunto + ")"));
                    }

                //para agregar un item al combo
                DataRow row = TbSuc.NewRow();
                    row["CODSUC"] = "";
                    row["NOMSUC"] = "TODOS";
                    row["CONJUNTO"] = "";
                    TbSuc.Rows.InsertAt(row, 0);
                    //.....................

                    cmb_sucursal.DataSource = TbSuc;
                    cmb_sucursal.ValueMember = "CODSUC";
                    cmb_sucursal.DisplayMember = "NOMSUC";
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }         
            }

            private void limpiar()
            {
                dtp_fecha_inicio.Value = DateTime.Today; 
                dtp_fecha_inicio.Value = dtp_fecha_inicio.Value.AddDays(-7);
                dtp_fecha_fin.Value = DateTime.Today; 

                txt_pedido.Text = "";
                cmb_distribuidor.SelectedIndex = 0;
                cmb_sucursal.SelectedIndex = 0;                
                txt_articulo.Text = "";
                txt_cantidad.Text = "";
            }

            private void cargarSoc()
            {
                try
                {
                    DataTable Tbsoc = _Sql.ExecuteQuery(" SELECT  codsoc ,modulo,BD  FROM Paut_VwSociedad ");
                    cmb_distribuidor.DataSource = Tbsoc;
                    cmb_distribuidor.ValueMember = "codsoc";
                    cmb_distribuidor.DisplayMember = "modulo";  
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }           
            }

            private void cargar_pedidos()
            {                                
                try
                {
                    Dictionary<string, string> parametro = new Dictionary<string, string>();

                    formato_fecha(3);

                    parametro.Add("@PEDIDO", txt_pedido.Text.ToString().Trim());
                    parametro.Add("@codsuc", cmb_sucursal.SelectedValue.ToString());
                    parametro.Add("@fecha", dtp_fecha_inicio.Text.ToString().Trim());
                    parametro.Add("@fechafin", dtp_fecha_fin.Text.ToString().Trim());
                    parametro.Add("@sociedad", cmb_distribuidor.SelectedValue.ToString());
                    parametro.Add("@conjunto", Clases.Statics.conjunto);

                    DataTable tb_ped = _Sql.ExecuteQuery(string.Format("EXEC Paut_spVerPedido @PEDIDO, @codsuc, @fecha, @fechafin, @sociedad, @conjunto"), parametro);
                    
                    Dg.DataSource = tb_ped;
                    Dg.Columns["codsuc"].Visible = false;
                    
                    formato_fecha(2);
                    Dg.EnableHeadersVisualStyles = false;
                    Dg.ColumnHeadersDefaultCellStyle.BackColor = Color.LightSkyBlue; 
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }     
            }

            private void cargar_pedidos_deta()
            {
                try
                {
                    Dictionary<string, string> parametro = new Dictionary<string, string>();

                    formato_fecha(3);

                    parametro.Add("@PEDIDO", txt_pedido.Text.ToString().Trim());
                    parametro.Add("@ARTICULO", txt_articulo.Text.ToString().Trim());
                    if (txt_cantidad.Text.Trim() == "")
                    {
                        parametro.Add("@CANTIDAD", "null"); 
                    }
                    else
                    {
                        parametro.Add("@CANTIDAD", txt_cantidad.Text.Trim()); 
                    }                  
                    parametro.Add("@sociedad", cmb_distribuidor.SelectedValue.ToString());
                    parametro.Add("@conjunto", Clases.Statics.conjunto);
                    parametro.Add("@fecha", dtp_fecha_inicio.Text.ToString().Trim());
                    parametro.Add("@fecha_fin", dtp_fecha_fin.Text.ToString().Trim());
                    parametro.Add("@codsuc", cmb_sucursal.SelectedValue.ToString());

                    DataTable tb_ped_deta = _Sql.ExecuteQuery(string.Format("EXEC Paut_spVerPedidoDeta @PEDIDO, @ARTICULO, @CANTIDAD, @sociedad, @conjunto, @fecha, @fecha_fin, @codsuc"), parametro);
                    Dg_detalle.DataSource = tb_ped_deta;
                    Dg_detalle.Columns["SUCURSAL"].Visible = false;

                    formato_fecha(2);
                    Dg_detalle.EnableHeadersVisualStyles = false;
                    Dg_detalle.ColumnHeadersDefaultCellStyle.BackColor = Color.LightSkyBlue;                     
                }             
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }   
            }

            private void formato_fecha(int tipo)
            {
                if (tipo == 1)
                {
                    dtp_fecha_inicio.Value = DateTime.Today; 
                    dtp_fecha_inicio.Value = dtp_fecha_inicio.Value.AddDays(-7);
                    dtp_fecha_inicio.Format = DateTimePickerFormat.Custom;
                    dtp_fecha_inicio.CustomFormat = "dd-MM-yyyy";

                    dtp_fecha_fin.Value = DateTime.Today; 
                    dtp_fecha_fin.Format = DateTimePickerFormat.Custom;
                    dtp_fecha_fin.CustomFormat = "dd-MM-yyyy";
                }
                if (tipo == 2) //formato pantalla
                {
                    dtp_fecha_inicio.Format = DateTimePickerFormat.Custom;
                    dtp_fecha_inicio.CustomFormat = "dd-MM-yyyy";

                    dtp_fecha_fin.Format = DateTimePickerFormat.Custom;
                    dtp_fecha_fin.CustomFormat = "dd-MM-yyyy";
                }
                if (tipo == 3) //formato base
                {
                    dtp_fecha_inicio.Format = DateTimePickerFormat.Custom;
                    dtp_fecha_inicio.CustomFormat = "yyyy-MM-dd";

                    dtp_fecha_fin.Format = DateTimePickerFormat.Custom;
                    dtp_fecha_fin.CustomFormat = "yyyy-MM-dd";
                }
            }
                 
        #endregion
        
        private void FrmConsulta_ped_Load(object sender, EventArgs e)
        {
            try
            {
                formato_fecha(1);
                cargarSoc();
                cargarSuc();            
                cargar_pedidos();
                cargar_pedidos_deta();               
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }          
        }

        private void FrmConsulta_ped_Resize(object sender, EventArgs e)
        {
            groupBox1.Left = (this.Width - groupBox1.Width) / 2;
            label5.Left = (this.Width - label5.Width) / 2;               
        }

        private void FrmConsulta_ped_FormClosed(object sender, FormClosedEventArgs e)
        {
            control_pantalla(Clases.Statics.pantalla_consulta);
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            cargar_pedidos();
            cargar_pedidos_deta();
        }

        private void txt_cantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
                {
                    e.Handled = false;
                }
                else
                {
                    //el resto de teclas pulsadas se desactivan 
                    e.Handled = true;
                } 
        }
        
        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            try
            {
                limpiar();
                cargar_pedidos();
                cargar_pedidos_deta();
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void Dg_detalle_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txt_pedido.Text = Dg_detalle.CurrentRow.Cells["PEDIDO"].Value.ToString().Trim();
                txt_articulo.Text = Dg_detalle.CurrentRow.Cells["ARTICULO"].Value.ToString().Trim();
                txt_cantidad.Text = Dg_detalle.CurrentRow.Cells["CANTIDAD"].Value.ToString().Trim();
                cmb_sucursal.SelectedValue = Dg_detalle.CurrentRow.Cells["SUCURSAL"].Value.ToString().Trim();
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void Dg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txt_pedido.Text = Dg.CurrentRow.Cells["PEDIDO"].Value.ToString().Trim();
                cmb_sucursal.SelectedValue = Dg.CurrentRow.Cells["CODSUC"].Value.ToString().Trim();
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }    
        }

      
    }
}
