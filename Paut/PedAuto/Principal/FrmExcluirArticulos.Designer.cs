﻿namespace PedAuto.Formularios
{
    partial class FrmExcluirArticulos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmExcluirArticulos));
            this.Dg = new System.Windows.Forms.DataGridView();
            this.ribbon1 = new System.Windows.Forms.Ribbon();
            this.ribbonTab1 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel1 = new System.Windows.Forms.RibbonPanel();
            this.rbb_Nuevo = new System.Windows.Forms.RibbonButton();
            this.rbb_Actualizar = new System.Windows.Forms.RibbonButton();
            this.rbb_Filtrar = new System.Windows.Forms.RibbonButton();
            this.rbb_Qfiltro = new System.Windows.Forms.RibbonButton();
            this.lbl_total = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Dg)).BeginInit();
            this.SuspendLayout();
            // 
            // Dg
            // 
            this.Dg.AllowUserToAddRows = false;
            this.Dg.AllowUserToDeleteRows = false;
            this.Dg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Dg.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.Dg.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Dg.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.Dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Dg.DefaultCellStyle = dataGridViewCellStyle5;
            this.Dg.Location = new System.Drawing.Point(23, 158);
            this.Dg.Name = "Dg";
            this.Dg.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Dg.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.Dg.RowHeadersVisible = false;
            this.Dg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dg.Size = new System.Drawing.Size(873, 304);
            this.Dg.TabIndex = 10;
            this.Dg.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dg_CellDoubleClick);
            // 
            // ribbon1
            // 
            this.ribbon1.CaptionBarVisible = false;
            this.ribbon1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ribbon1.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.Minimized = false;
            this.ribbon1.Name = "ribbon1";
            // 
            // 
            // 
            this.ribbon1.OrbDropDown.BorderRoundness = 8;
            this.ribbon1.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.OrbDropDown.Name = "";
            this.ribbon1.OrbDropDown.Size = new System.Drawing.Size(527, 447);
            this.ribbon1.OrbDropDown.TabIndex = 0;
            this.ribbon1.OrbImage = null;
            this.ribbon1.OrbVisible = false;
            this.ribbon1.Size = new System.Drawing.Size(920, 117);
            this.ribbon1.TabIndex = 11;
            this.ribbon1.Tabs.Add(this.ribbonTab1);
            this.ribbon1.TabsMargin = new System.Windows.Forms.Padding(12, 2, 20, 0);
            this.ribbon1.Text = "ribbon1";
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Panels.Add(this.ribbonPanel1);
            this.ribbonTab1.Text = "Proceso";
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.Items.Add(this.rbb_Nuevo);
            this.ribbonPanel1.Items.Add(this.rbb_Actualizar);
            this.ribbonPanel1.Items.Add(this.rbb_Filtrar);
            this.ribbonPanel1.Items.Add(this.rbb_Qfiltro);
            this.ribbonPanel1.Text = "";
            // 
            // rbb_Nuevo
            // 
            this.rbb_Nuevo.Image = global::PedAuto.Properties.Resources.nuevo;
            this.rbb_Nuevo.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbb_Nuevo.SmallImage")));
            this.rbb_Nuevo.Text = "Nuevo";
            this.rbb_Nuevo.Click += new System.EventHandler(this.rbb_Nuevo_Click);
            // 
            // rbb_Actualizar
            // 
            this.rbb_Actualizar.Image = global::PedAuto.Properties.Resources.actualizar_restaure_todas_las_pestanas_icono_7808_32;
            this.rbb_Actualizar.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbb_Actualizar.SmallImage")));
            this.rbb_Actualizar.Text = "Actualizar";
            this.rbb_Actualizar.Click += new System.EventHandler(this.rbb_Actualizar_Click);
            // 
            // rbb_Filtrar
            // 
            this.rbb_Filtrar.Image = global::PedAuto.Properties.Resources.search;
            this.rbb_Filtrar.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbb_Filtrar.SmallImage")));
            this.rbb_Filtrar.Text = "Buscar";
            this.rbb_Filtrar.Click += new System.EventHandler(this.rbb_Filtrar_Click);
            // 
            // rbb_Qfiltro
            // 
            this.rbb_Qfiltro.Image = global::PedAuto.Properties.Resources.lente_de_zoom_de_busqueda_icono_7035_32;
            this.rbb_Qfiltro.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbb_Qfiltro.SmallImage")));
            this.rbb_Qfiltro.Text = "Quitar Filtro";
            this.rbb_Qfiltro.Click += new System.EventHandler(this.rbb_Qfiltro_Click);
            // 
            // lbl_total
            // 
            this.lbl_total.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_total.AutoSize = true;
            this.lbl_total.Location = new System.Drawing.Point(797, 131);
            this.lbl_total.Name = "lbl_total";
            this.lbl_total.Size = new System.Drawing.Size(99, 13);
            this.lbl_total.TabIndex = 12;
            this.lbl_total.Text = "Total de Registros: ";
            // 
            // FrmExcluirArticulos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 474);
            this.Controls.Add(this.lbl_total);
            this.Controls.Add(this.ribbon1);
            this.Controls.Add(this.Dg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmExcluirArticulos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Excluir Artículos";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmExcluirArticulos_FormClosed);
            this.Load += new System.EventHandler(this.FrmExcluirArticulos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Dg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Dg;
        private System.Windows.Forms.Ribbon ribbon1;
        private System.Windows.Forms.RibbonTab ribbonTab1;
        private System.Windows.Forms.RibbonPanel ribbonPanel1;
        private System.Windows.Forms.RibbonButton rbb_Nuevo;
        private System.Windows.Forms.RibbonButton rbb_Actualizar;
        private System.Windows.Forms.RibbonButton rbb_Filtrar;
        private System.Windows.Forms.RibbonButton rbb_Qfiltro;
        private System.Windows.Forms.Label lbl_total;
    }
}