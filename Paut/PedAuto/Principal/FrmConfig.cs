﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PedAuto.Formularios
{
    public partial class FrmConfig : PedAuto.Formularios.FrmBase
    {
        public FrmConfig()
        {
            InitializeComponent();
        }

        public void control_pantalla(string pantalla)
        {
            try
            {
                string query = string.Format("exec Paut_spControlAcceso 2, '" + Clases.Statics.usuario + "',  '" + pantalla + "', '' ");
                _Sql.ExecuteNoResult(query);

            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAceptar_Click(object sender, EventArgs e)
        {

            try
            {
                _Sql.server = TxtServer.Text.Trim();
                _Sql.db = TxtBase.Text.Trim();
                _Sql.user = TxtUser.Text.Trim();
                _Sql.password = TxtPass.Text.Trim();
                if (_Desing.ValidarEnGrupo(groupBox1))
                {
                    if (_Sql.connect())
                    {
                        Properties.Settings.Default.autServer = TxtServer.Text.Trim();
                        Properties.Settings.Default.autDataBase = TxtBase.Text.Trim();
                        Properties.Settings.Default.autUser = TxtUser.Text.Trim();
                        Properties.Settings.Default.autPass = TxtPass.Text.Trim();
                        Properties.Settings.Default.Save();
                        _Desing.SendMessage("Configuracion Correcta", "2");
                    }
                    else
                    {
                        _Desing.SendMessage("Datos Incorrectos", "3");
                    }
                }
                else {

                    _Desing.SendMessage("Por favor, Complete los Datos", "3");
                }
                
            
            }
            catch (Exception Ex)
            {

                _Desing.SendMessage(Ex.Message ,"1");
            }
        }

        private void FrmConfig_Load(object sender, EventArgs e)
        {
            TxtServer.Text = Properties.Settings.Default.autServer;
            TxtBase.Text = Properties.Settings.Default.autDataBase;
            TxtUser.Text =Properties.Settings.Default.autUser  ;
            TxtPass.Text=Properties.Settings.Default.autPass  ;
        }

        private void FrmConfig_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Properties.Settings.Default.autServer==null)
            {
                Application.Exit();
            }
        }

        private void FrmConfig_FormClosed(object sender, FormClosedEventArgs e)
        {
            control_pantalla(Clases.Statics.pantalla_configuracion);
        }

        
    }
}
