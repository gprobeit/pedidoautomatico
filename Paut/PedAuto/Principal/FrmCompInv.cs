﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PedAuto.Clases;

namespace PedAuto.Formularios
{
    public partial class FrmCompInv : Form
    {
        public FrmCompInv()
        {
            InitializeComponent();
        }

        #region var
            string CODART2 = "";
            bool pActivo = false;
            //datatables
            DataTable Articulos = new DataTable();
            DataTable Detalle  = new DataTable();
            DataTable Mvp_Pedido = new DataTable();
            string PedidoMod = "";
            public Desing _Desing = new Desing();
            public DB _Sql = new DB();
        #endregion

        #region metodo
                 
          
            private void cargaContenido()
            {
                try
                {
                    string sqlsend = string.Format(" EXECUTE intrade.dbo.[Dva_spDetArticulos]  @CODART='{0}' ,@NOMART='{1}',@CODCLA1='{2}' ,@CODCLA2='{3}' " +
                    " ,@CODCLA3='{4}'  ,@CODCLA4='{5}'  ,@CODCLA5 ='{6}'  ,@CODCLA6='{7}'  ,@EMPRESA ='{8}' ", TxtCodArt.TextBoxText.Trim(), TxtDesc.TextBoxText.Trim(), TxtCla1.TextBoxText.Trim(), TxtCla2.TextBoxText.Trim(), TxtCla3.TextBoxText.Trim(), TxtCla4.TextBoxText.Trim(), TxtCla5.TextBoxText.Trim(), TxtCla6.TextBoxText.Trim(), cmb_distribuidora.Text.Trim());

                    Articulos = _Sql.ExecuteQuery(sqlsend);
                    Articulos.DefaultView.Sort = "INVENTARIO DESC";

                    if (chExistencia.Checked)
                    {
                        DataRow[] Rw = Articulos.Select("INVENTARIO > 0");
                        if (Rw.Length > 0)
                        {
                            DgArt.DataSource = Rw.CopyToDataTable();                            
                        }
                    }
                    else
                    {
                        DgArt.DataSource = Articulos;
                    }
                    Dgcomp.DataSource = null;
                    lbl_total.Text = "Cantidad de Artículos: " + DgArt.RowCount;
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.ToString(), "1");
                }
            }

            private void cargadetalle(string articulo_ )
            {
                try
                {
                    if (articulo_ == "")
                    {
                        //Dgcomp.DataSource = null;
                        string sqlq = string.Format(" EXECUTE INTRADE.dbo.[Dva_spDetallRotAfil]  @pFI='{0}' ,@pFF='{1}',@CodArt='{2}' ,@NomArt='{3}' " +
                        " ,@CodCla1='{4}'  ,@CodCla2='{5}'  ,@CodCla3 ='{6}'  ,@CodCla4='{7}'  ,@CodCla5 ='{8}'  ,@CodCla6='{9}'  ", TxtDesde.TextBoxText.Trim(), TxtHasta.TextBoxText.Trim(), articulo_, TxtDesc.TextBoxText.Trim(), TxtCla1.TextBoxText.Trim(), TxtCla2.TextBoxText.Trim(), TxtCla3.TextBoxText.Trim(), TxtCla4.TextBoxText.Trim(), TxtCla5.TextBoxText.Trim(), TxtCla6.TextBoxText.Trim());
                        Detalle = _Sql.ExecuteQuery(sqlq);
                        Detalle.Dispose();
                    }
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.ToString(), "1");
                }

            }

            private void marcarExistencia()
            {
                foreach (DataGridViewRow item in Dgcomp.Rows)
                {
                    if (Convert.ToDouble(item.Cells[7].Value) <= 0)
                    {
                        item.DefaultCellStyle.ForeColor = Color.Red;
                    }
                }
            }

            private void ConsultaArt(string articulo)
            {
                DataRow[] Rw = Detalle.Select(string.Format(" CODART='{0}' ", articulo));
            }

            private void seleccionarFecha(RibbonTextBox Txt)
            {
                FrmFecha Frm = new FrmFecha();
                Frm.monthCalendar1.SelectionStart = Convert.ToDateTime(Txt.TextBoxText).Date;
                Frm.monthCalendar1.SelectionEnd = Convert.ToDateTime(Txt.TextBoxText).Date;
                DialogResult Rs = Frm.ShowDialog();
                if (Rs == System.Windows.Forms.DialogResult.OK)
                {
                    string FechaAnt = Txt.TextBoxText;
                    Txt.TextBoxText = Frm.monthCalendar1.SelectionStart.ToString("yyyy-MM-dd");
                    if (!(Convert.ToDateTime(TxtDesde.TextBoxText).Date <= Convert.ToDateTime(TxtHasta.TextBoxText).Date))
                    {
                        _Desing.SendMessage("Fechas Incorrectas", "2");
                        Txt.TextBoxText = FechaAnt;
                    }
                }
            }

            private void LimpiarDatos()
            {
                TxtCodArt.TextBoxText = "";
                TxtDesc.TextBoxText = "";
                TxtCla1.TextBoxText = "";
                TxtCla2.TextBoxText = "";
                TxtCla3.TextBoxText = "";
                TxtCla4.TextBoxText = "";
                TxtCla5.TextBoxText = "";
                TxtCla6.TextBoxText = "";
                lblArticulo.Text = "Articulo Seleccionado:";
                CODART2 = "";
            }

            private void MostrarColumnas()
            {
                try
                {
                    if (Dgcomp.Columns.Count > 0)
                    {
                        Dgcomp.Columns["CODART"].Visible = false;
                        Dgcomp.Columns["TIPO_INV"].Visible = false;
                    }

                    if (RbPro.Checked)
                    {
                        if (Dgcomp.Columns.Count > 0)
                        {
                            //ocultar campos de venta
                            Dgcomp.Columns["VTA"].Visible = false;
                            Dgcomp.Columns["SEMANAS"].Visible = false;
                            Dgcomp.Columns["MESES"].Visible = false;
                            Dgcomp.Columns["ROT_PROM_SEM"].Visible = false;
                            Dgcomp.Columns["PROYECC_SEM"].Visible = false;
                            Dgcomp.Columns["ROT_PROM_MESES"].Visible = false;
                            Dgcomp.Columns["PROYECC_MENS"].Visible = false;
                        }
                    }
                    else
                    {
                        if (RbVe.Checked)
                        {
                            if (Dgcomp.Columns.Count > 0)
                            {
                                Dgcomp.Columns["VTA"].Visible = true;
                                if (ChMen.Checked)
                                {
                                    Dgcomp.Columns["PROYECC_MENS"].Visible = true;
                                    Dgcomp.Columns["ROT_PROM_MESES"].Visible = true;
                                    Dgcomp.Columns["MESES"].Visible = true;
                                    //ocultar
                                    Dgcomp.Columns["PROYECC_SEM"].Visible = false;
                                    Dgcomp.Columns["ROT_PROM_SEM"].Visible = false;
                                    Dgcomp.Columns["SEMANAS"].Visible = false;
                                }
                                else
                                {
                                    if (ChSem.Checked)
                                    {
                                        Dgcomp.Columns["PROYECC_MENS"].Visible = false;
                                        Dgcomp.Columns["ROT_PROM_MESES"].Visible = false;
                                        Dgcomp.Columns["MESES"].Visible = false;
                                        //mostrar
                                        Dgcomp.Columns["PROYECC_SEM"].Visible = true;
                                        Dgcomp.Columns["ROT_PROM_SEM"].Visible = true;
                                        Dgcomp.Columns["SEMANAS"].Visible = true;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _Desing.SendMessage(ex.ToString(), "1");
                }
            }
                   
            private void habilitarTitulo()
            {
                lblaviso.Text = "PEDIDO DE " + Convert.ToString(RbPro.Checked ? "PRODUCCION" : "VENTA");
                lblaviso.Visible = true;
                panelTipoInv.Enabled = false;
            }

        #endregion
                 
        private void FrmCompInv_Load(object sender, EventArgs e)
        {
           try
           {
                cmb_distribuidora.Text = "Intrade";

                TxtDesde.Text = "Desde";
                TxtHasta.Text = "Hasta";
                TxtDesde.TextBoxText = DateTime.Now.AddMonths(-3).ToString("yyyy-MM-dd");
                TxtHasta.TextBoxText = DateTime.Now.ToString("yyyy-MM-dd");
                DgArt.AutoGenerateColumns = false;

                Mvp_Pedido.Columns.Add("Negocio");
                Mvp_Pedido.Columns.Add("Cod_Sucursal");
                Mvp_Pedido.Columns.Add("Sucursal");
                Mvp_Pedido.Columns.Add("Cantidad");
                Mvp_Pedido.Columns.Add("Articulo");
                Mvp_Pedido.Columns.Add("Cliente");
                Mvp_Pedido.Columns.Add("Pedido");
                Mvp_Pedido.Columns.Add("Descripcion");
           }
           catch (Exception Ex)
           {
               _Desing.SendMessage(Ex.Message, "1");
           }
        }
               
        private void DgArt_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Down || e.KeyCode == Keys.Up)
                {              

                    if (DgArt.Rows.Count > 0 & DgArt.CurrentCell != null)
                    {
                        int row=DgArt.CurrentCell.RowIndex ;
                        switch (e.KeyCode)
                        {
                            case Keys.Up:
                                row = DgArt.CurrentCell.RowIndex - 1;
                                break;
                            case Keys.Down:
                                row = DgArt.CurrentCell.RowIndex + 1;
                                break;
                        }

                        if (row < 0 || row > DgArt.Rows.Count - 1)
                        {
                            row = DgArt.CurrentCell.RowIndex;
                        }               

                        CODART2 = DgArt.Rows[row].Cells[0].Value.ToString().Trim();
                        lblArticulo.Text = "Articulo Seleccionado: " + DgArt.Rows[row].Cells[0].Value.ToString().Trim();
                        filtrarContenido(CODART2,false);
          
                        if (chExistencia.Checked )
                        {
                            marcarExistencia();
                        }
                    }
                    else
                    {
                        //CODART2 = "";
                    }
                }
            }
            catch (Exception ex)
            {
                _Desing.SendMessage(ex.ToString(), "1");
            }
        }
            
        private void DgArt_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {    
                if (e.RowIndex >=0 & e.ColumnIndex >=0 & DgArt.CurrentCell != null)
                {
                    int row = DgArt.CurrentCell.RowIndex;
                    lblArticulo.Text = "Articulo Seleccionado: " + DgArt.Rows[row].Cells[0].Value.ToString().Trim();
                    CODART2 = DgArt.Rows[row].Cells[0].Value.ToString().Trim();
                    filtrarContenido(CODART2,false );
                    MostrarColumnas();
              
                    if (chExistencia.Checked)
                    {
                        marcarExistencia();
                    }
                }          
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void ButMostrar_Click(object sender, EventArgs e)
        {
            cargaContenido();
            cargadetalle("");
            filtrarContenido("",true);
            MostrarColumnas();
        }

        private void TxtHasta_Click(object sender, EventArgs e)
        {
            seleccionarFecha(TxtHasta);
        }

        private void ButQuitar_Click(object sender, EventArgs e)
        {
            LimpiarDatos();
            cargaContenido();
            cargadetalle("");
            filtrarContenido("",true);
        }

        private void TxtDesde_TextBoxKeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void TxtHasta_TextBoxKeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void RbPro_CheckBoxCheckChanged(object sender, EventArgs e)
        {
            if (RbPro.Checked)
            {
                RbVe.Checked = false;
                ChMen.Visible = false;
                ChSem.Visible = false;               
                filtrarContenido( CODART2,true );
                MostrarColumnas();
            }
        }

        private void filtrarContenido(string articulo, bool catalogo) 
        {
            try
            {
                if (Detalle .Rows.Count >0)
                {
                    string TipoInv;
           
                    if (RbPro.Checked)                
                        TipoInv = "PRD";              
                    else                
                        TipoInv = "VTA";                

                    string Seleccionar = "";

                    if (articulo == "")
                        Seleccionar = string.Format("TIPO_INV='{0}'", TipoInv);
                    else
                        Seleccionar = string.Format("TIPO_INV='{0}' and CODART='{1}' ", TipoInv, articulo);

                    if (catalogo ==true)
                    {
                        DataRow[] rwar = Articulos.Select(TipoInv + " = 'SI' or " + TipoInv + " ='' ");
                        if (rwar.Length > 0)
                        {
                            DgArt.DataSource = rwar.CopyToDataTable();
                        }
                        else
                        {
                            DgArt.DataSource = null;
                        }

                    }

                    DataRow[] Rw = Detalle.Select(Seleccionar);
                    if (Rw.Length > 0 && DgArt .Rows.Count>0)
                        Dgcomp.DataSource = Rw.CopyToDataTable();
                    else
                        Dgcomp.DataSource = null;

                    MostrarColumnas();
                }
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }
        
        private void RbVe_CheckBoxCheckChanged(object sender, EventArgs e)
        {
            if (RbVe .Checked )
            {
                RbPro.Checked = false;
                ChMen.Visible = true;
                ChSem.Visible = true;
                MostrarColumnas();
                filtrarContenido(CODART2,true );
            }
        }

        private void ChMen_CheckBoxCheckChanged(object sender, EventArgs e)
        {
            if (ChMen.Checked)
            {
                ChSem.Checked = false;
                MostrarColumnas();
            }
        }

        private void ChSem_CheckBoxCheckChanged(object sender, EventArgs e)
        {
            if (ChSem.Checked)
            {
                ChMen.Checked = false;
                MostrarColumnas();
            }
        }

        private void TxtDesde_Click(object sender, MouseEventArgs e)
        {
            seleccionarFecha(TxtDesde); 
        }

        private void TxtHasta_MouseUp(object sender, MouseEventArgs e)
        {
            seleccionarFecha(TxtHasta); 
        }

        private void ButLimpiar_Click(object sender, EventArgs e)
        {
            try
            {
                Mvp_Pedido.Rows.Clear();            
            }
            catch (Exception ex)
            {
                _Desing.SendMessage(ex.ToString(), "1");
            }
        }
              
        private void ButMar_Click(object sender, EventArgs e)
        {
           try
           {

               if (DgArt.Rows.Count > 0 && Mvp_Pedido.Rows.Count > 0)
               {
                   DataTable Art = Mvp_Pedido.DefaultView.ToTable(true, "Articulo");
                   foreach (DataRow item in Art.Rows)
                   {
                       _Desing.BuscarLINQ(item["Articulo"].ToString().Trim(), "Articulo", DgArt, 1);
                   }

               }
               else DgArt.RowsDefaultCellStyle.ForeColor = Color.Black;
            }
           catch (Exception ex)
           {
               _Desing.SendMessage(ex.ToString(), "1");
           }
        }
        
        //private void BuscaClasificacion(RibbonTextBox Txt, int numero) {

        //    try
        //    {
        //        DataTable TbCla = _Sql.ExecuteQuery(string.Format(" SELECT  CLASIFICACION ,DESCRIPCION FROM Dva_VwClasificacion WHERE AGRUPACION='{0}' ",numero));
        //        FrmBusqueda Bus = new FrmBusqueda(TbCla, _Global.TipoBusqueda.Global);
        //        Bus.ShowDialog();
        //        if (Bus.Control != null)
        //        {
        //            Txt.TextBoxText = Bus.Control.ToString().Trim();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _Desing.SendMessage(ex.ToString(), "1");
        //    }
        //}

        private void TxtCla1_DoubleClick(object sender, EventArgs e)
        {
            //BuscaClasificacion(TxtCla1,1);
        }

        private void TxtCla2_DoubleClick(object sender, EventArgs e)
        {
            //BuscaClasificacion(TxtCla2,2);
        }

        private void TxtCla3_DoubleClick(object sender, EventArgs e)
        {
            //BuscaClasificacion(TxtCla3,3);
        }

        private void TxtCla4_DoubleClick(object sender, EventArgs e)
        {
            //BuscaClasificacion(TxtCla4,4);
        }

        private void TxtCla5_DoubleClick(object sender, EventArgs e)
        {
            //BuscaClasificacion(TxtCla5,5);
        }

        private void TxtCla6_DoubleClick(object sender, EventArgs e)
        {
            //BuscaClasificacion(TxtCla6,6);
        }   

    
    }//clase
  }
