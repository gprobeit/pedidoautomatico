﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using PedAuto.Clases;
using System.Configuration;


namespace PedAuto.Formularios
{
    public partial class FrmPedAuto : PedAuto.Formularios.FrmBase
    {
        public FrmPedAuto()
        {
            InitializeComponent();
        }


        #region Variables
        DataTable TbPendientes = new DataTable();
      
        #endregion

        #region metodos
                     
        
        private void cargarSuc()
        {
            DataTable TbSuc;

            if ( ConfigurationSettings.AppSettings["sociedad"] == "VS" )
            {
                TbSuc = _Sql.ExecuteQuery(string.Format(" SELECT DISTINCT CODSUC,NOMSUC,CONJUNTO  FROM Paut_VwSucursal WHERE TIPO='VTA' and CONJUNTO IN (1,2)"));
            }
            else
            {
                TbSuc = _Sql.ExecuteQuery(string.Format(" SELECT DISTINCT CODSUC,NOMSUC,CONJUNTO  FROM Paut_VwSucursal WHERE TIPO='VTA' AND CONJUNTO IN (3)"));     
            }
           
            cmbSuc.DropDownItems.Clear();

            foreach (DataRow fs in TbSuc.Rows)
            {
                RibbonButton item = new RibbonButton();
                item.Value = fs["CODSUC"].ToString().Trim();
                item.Text = fs["CODSUC"].ToString().Trim() + "-" + fs["NOMSUC"].ToString().Trim();
                item.ToolTip = fs["NOMSUC"].ToString().Trim();
                item.ToolTipTitle = fs["CONJUNTO"].ToString().Trim();
                cmbSuc.DropDownItems.Add(item);   
            }
            if (TbSuc.Rows.Count > 0)
            {
                cmbSuc.SelectedValue = TbSuc.Rows[0]["CODSUC"].ToString().Trim();
            }

            //RibbonButton item0 = new RibbonButton();
            //item0.Text = "---SELECCIONAR---";
            //item0.Value = "";
            //cmbSuc.DropDownItems.Insert(0, item0);
            //cmbSuc.SelectedValue = ("").ToString(); 
           
        }
       
        private void cargarSoc()
        {
            cmbSociedad.DropDownItems.Clear();
            DataTable Tbsoc = _Sql.ExecuteQuery(" SELECT  codsoc ,modulo,BD  FROM Paut_VwSociedad ");
            
            cmbSuc.DropDownItems.Clear();

            foreach (DataRow fs in Tbsoc.Rows)
            {
                RibbonButton item = new RibbonButton();
                item.Value = fs["codsoc"].ToString().Trim();
                item.Text = fs["modulo"].ToString().Trim();
                item.Tag = fs["BD"].ToString().Trim();
                this.cmbSociedad.DropDownItems.Add(item);

            }
            if (Tbsoc.Rows.Count > 0)
            {
                cmbSociedad.SelectedValue = Tbsoc.Rows[0]["codsoc"].ToString().Trim();
            }



        }

        private void cargarBodega()
        {
            cmbBodega.DropDownItems.Clear();
            DataTable TbBod = _Sql.ExecuteQuery(string.Format(" SELECT BODEGA ,NOMBRE  FROM Paut_VwBodega WHERE SOCIEDAD ='{0}' ", cmbSociedad.SelectedValue));

            cmbBodega.DropDownItems.Clear();

            foreach (DataRow fs in TbBod.Rows)
            {
                RibbonButton item = new RibbonButton();
                item.Value = fs["BODEGA"].ToString().Trim();
                item.Text = fs["NOMBRE"].ToString().Trim();
                cmbBodega.DropDownItems.Add(item);
                cmbBodega.SelectedValue = fs["BODEGA"].ToString().Trim();
            }
            if (TbBod.Rows.Count > 0)
            {
                cmbBodega.SelectedValue = TbBod.Rows[0]["BODEGA"].ToString().Trim();
            }


        }

        //private string retornartxt() {
        //    string name="";

        //    foreach (RibbonItem ctl in ribbonPanel5.Items)
        //    {
        //        RibbonTextBox tx = ctl as RibbonTextBox;
        //        if (tx.TextBoxText != "")
        //        {
        //            name= tx.Tag.ToString () + "?" + tx.TextBoxText;
        //            break;

        //        }
        //    }

        //    return name;

        //}

        private void Reinicializar()
        {
            try
            {
                Dictionary<string, string> par = new Dictionary<string, string>();
                
                TbPendientes = _Sql.ExecuteQuery(string.Format(" EXEC Paut_spTabControl_UID "), par);


            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        
        private void LoadPendientes_new(bool filtrar)
        {

            Dg.AutoGenerateColumns = false;
            Dg.Rows.Clear();

            Dictionary<string, string> par = new Dictionary<string, string>();
            par.Add("@suc", cmbSuc.SelectedValue);
            par.Add("@bod", cmbBodega.SelectedValue);
            par.Add("@soc", cmbSociedad.SelectedValue);
            par.Add("@conj", cmbSuc.SelectedItem.ToolTipTitle);

            TbPendientes = _Sql.ExecuteQuery(string.Format(" SELECT  Codigo, Descripcion, Cantidad, Existencia, Facturar,Proveedor, Fecha " +
            " FROM Paut_FnTableroControl (@bod,@soc,@conj,@suc) "), par);
           
            if (TbPendientes.Rows.Count > 0)
            {                  
                foreach (DataRow Fg in TbPendientes.Rows)
                {
                    Dg.Rows.Add(false, Fg["Codigo"], Fg["Descripcion"], Fg["Cantidad"], Fg["Existencia"], Fg["Facturar"], Fg["Proveedor"], Fg["Fecha"]);
                }
                TbPendientes.Dispose();
                string fecha = TbPendientes.Compute("max(Fecha)", "").ToString();
                lblfecha.Text = "Actualizado al: " + fecha;

            }
            else
            {
                lblfecha.Text = "Actualizado al: ";
            }
        }

        private void LoadPendientes(bool filtrar)
        {

            if (cmbSociedad.DropDownItems.Count > 0 && cmbSuc.DropDownItems.Count > 0 && cmbBodega.DropDownItems.Count > 0 )
            {
                Dg.AutoGenerateColumns = false;
                Dg.Rows.Clear();

                Dictionary<string, string> par = new Dictionary<string, string>();
                par.Add("@suc", cmbSuc.SelectedValue);
                par.Add("@bod", cmbBodega.SelectedValue);
                par.Add("@soc", cmbSociedad.SelectedValue);
                par.Add("@conj", cmbSuc.SelectedItem.ToolTipTitle);

                TbPendientes = _Sql.ExecuteQuery(string.Format(" SELECT  Codigo, Descripcion, Cantidad, Existencia, Facturar,Proveedor, Fecha " +
                " FROM Paut_FnTableroControl (@bod,@soc,@conj,@suc) "), par);
                //" , CLASIFICACION_1 , CLASIFICACION_2 , CLASIFICACION_3  , CLASIFICACION_4, CLASIFICACION_5, CLASIFICACION_6 " +
                if (TbPendientes.Rows.Count > 0)
                {
                    //string[] datosb = retornartxt().Split('?');
                    //if (filtrar ==true && datosb.Length ==2)
                    //if (filtrar == true )
                    //{                     
                    //    string texto = datosb[0] + "='"  +datosb[1]+"'";
                    //    DataRow[] Rw = TbPendientes.Select(texto);

                    //    if (Rw.Length > 0)
                    //    {
                    //        TbPendientes = Rw.CopyToDataTable();
                    //    }
                    //    else {
                    //        TbPendientes.Clear();
                    //    }                 
                    //}                

                    foreach (DataRow Fg in TbPendientes.Rows)
                    {
                        Dg.Rows.Add(false, Fg["Codigo"], Fg["Descripcion"], Fg["Cantidad"], Fg["Existencia"], Fg["Facturar"], Fg["Proveedor"], Fg["Fecha"]);
                    }
                    TbPendientes.Dispose();
                    string fecha = TbPendientes.Compute("max(Fecha)", "").ToString();
                    lblfecha.Text = "Actualizado al: " + fecha;

                }
                else
                {
                    lblfecha.Text = "Actualizado al: ";
                }
            }
        }

        private void formatoColumnas()
        {

            Dg.Width = this.Width - 50;
            Dg.Height = this.Height - 175;


            Sel.Width = Convert.ToInt32(Dg.Width * 0.04);
            Fecha.Width = Convert.ToInt32(Dg.Width * 0.15);
            Codigo.Width = Convert.ToInt32(Dg.Width * 0.10);
            Descripcion.Width = Convert.ToInt32(Dg.Width * 0.35);
            Cantidad.Width = Convert.ToInt32(Dg.Width * 0.08);
            Existencia.Width = Convert.ToInt32(Dg.Width * 0.08);
            Facturar.Width = Convert.ToInt32(Dg.Width * 0.08);
            Proveedor.Width = Convert.ToInt32(Dg.Width * 0.10);
        }

        private void removerfilas(bool borrar)  //ya
        {
            try
            {
                string query = "";
                Dg.ClearSelection();

                foreach (DataGridViewRow Gr in Dg.Rows)
                {
                    if (Convert.ToBoolean(Gr.Cells["Sel"].Value) == true)
                    {
                        query += string.Format(" DELETE FROM Paut_TabControl WHERE sucursal='{0}' AND articulo='{1}' ", cmbSuc.SelectedValue, Gr.Cells["Codigo"].Value);
                        Gr.Selected = true;
                    }
                }                               
                foreach (DataGridViewRow gd in Dg.SelectedRows)
                {
                    Dg.Rows.Remove(gd);
                }
                if (borrar == true && query != "")
                {
                    _Sql.ExecuteNoResult(query);
                }
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void removerfilas_actualizar(bool borrar)
        {
            try
            {
                string query = "";
                Dg.ClearSelection();

                foreach (DataGridViewRow Gr in Dg.Rows)
                {
                    if (Convert.ToBoolean(Gr.Cells["Sel"].Value) == true)
                    {
                        query += string.Format(" DELETE FROM Paut_TabControl WHERE sucursal='{0}' AND articulo='{1}' AND cantidad=0 ", cmbSuc.SelectedValue, Gr.Cells["Codigo"].Value);
                        Gr.Selected = true;
                    }
                }

                foreach (DataGridViewRow gd in Dg.SelectedRows)
                {
                    
                    Dg.Rows.Remove(gd);
                    
                    
                }
                if (borrar == true && query != "")
                {
                    _Sql.ExecuteNoResult(query);
                }

            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void FrmPedAuto_Load(object sender, EventArgs e)
        {
            try
            {
                cargarSoc();
                cargarBodega();
                cargarSuc();
                LoadPendientes(false);
                formatoColumnas();

            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }                   

        private void actualiza_existencia()
        {
            try
            {
                Dictionary<string, string> par = new Dictionary<string, string>();
                foreach (DataGridViewRow Rw in Dg.Rows)
                {
                    par.Add("@bod", cmbBodega.SelectedValue.Trim());
                    par.Add("@art", Rw.Cells["Proveedor"].Value.ToString().Trim());
                    par.Add("@soc", cmbSociedad.SelectedValue);
                    DataTable Tb = _Sql.ExecuteQuery(" SELECT CANT_DISPONIBLE FROM Paut_VwExistencia where BODEGA=@bod and ARTICULO=@art and sociedad=@soc ", par);
                    if (Tb.Rows.Count > 0)
                        Rw.Cells["Existencia"].Value = Tb.Rows[0]["CANT_DISPONIBLE"];
                    else
                        Rw.Cells["Existencia"].Value = 0;
                    par.Clear();
                }              
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }

        }

        #endregion

               

        private void BtnActualiza_Click(object sender, EventArgs e)
        {
            actualiza_existencia();
        }

        private void cmbSuc_DropDownItemClicked(object sender, RibbonItemEventArgs e)
        {
            try            
            {
                LoadPendientes(false);
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
            
        }

        private void cmbBodega_DropDownItemClicked(object sender, RibbonItemEventArgs e)
        {            
            try
            {
                actualiza_existencia();
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void FrmPedAuto_Resize(object sender, EventArgs e)
        {
            formatoColumnas();
        }

        private void BtnPend_Click(object sender, EventArgs e)
        {
            try
            {               
                removerfilas(false);            
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
           
        }

        private void BtnDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaArticulos(Dg, "Sel") == false)
                {
                    MessageBox.Show("Por favor, Seleccione al menos un Articulo", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    if (MessageBox.Show("¿Desea Eliminar Los artículos seleccionados?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        removerfilas(true);
                    }
                }
                
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
           
        }


        private bool validaArticulos(string Columna, DataGridView grid, string TextoABuscar,string colsel)
        {
            Dg.ClearSelection();
            IEnumerable<DataGridViewRow> obj = (from DataGridViewRow row in grid.Rows.Cast<DataGridViewRow>()
                                                   where row.Cells[Columna].Value.ToString().Trim() == TextoABuscar
                                                   && Convert.ToBoolean(row.Cells[colsel].Value) ==true                                              
                                                   select row);           
         if (obj.Any())
         {
             grid.Rows[obj.FirstOrDefault().Index].Selected = true;
             return true;
         }

         return false   ;
        
        }

        private bool validaArticulos(DataGridView grid, string colsel)  // ya
        {
            Dg.ClearSelection();
            IEnumerable<DataGridViewRow> obj = (from DataGridViewRow row in grid.Rows.Cast<DataGridViewRow>()
                                                where Convert.ToBoolean(row.Cells[colsel].Value) == true
                                                select row);
            if (obj.Any())
            {              
                return true;
            }

            return false;

        }


        private void BtnPed_Click(object sender, EventArgs e)
        {
            try 
	        {

                if (validaArticulos(Dg, "Sel") == false)
                {
                    MessageBox.Show("Por favor, Seleccione al menos un Articulo", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else {
                    if (validaArticulos("Proveedor", Dg, "", "Sel") == true)
                    {
                        MessageBox.Show("Articulo sin proveedor", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                                      
                        //buscar cliente
                        Dictionary<string, string> par = new Dictionary<string, string>();
                   
                        par.Add("@SUC", cmbSuc.SelectedValue);
                        par.Add("@SOC", cmbSociedad.SelectedValue);
                        string CONSULTA = " SELECT CODSUC,NOMSUC,codcli_exactus FROM Paut_VwSucursal WHERE TIPO='VTA' AND CODSUC=@SUC AND SOCIEDAD=@SOC ";
                        DataTable Tbcod = _Sql.ExecuteQuery(CONSULTA, par);

                       

                        if (Tbcod.Rows.Count >0)
                        {
                            par.Clear();
                            par.Add("@cliente", Tbcod.Rows[0]["codcli_exactus"].ToString().Trim());
                            par.Add("@soc", cmbSociedad.SelectedItem.Text);
                        }


                        DataTable Tb = _Sql.ExecuteQuery("SELECT  cliente,condicion_pago,clase_documento FROM  Paut_VwClientes  " +
                        " WHERE cliente =@cliente and sociedad = @soc",par);
                        if (Tb.Rows.Count > 0)
                        {

                            string query = "";

                            Dictionary<string,string> parm = new Dictionary<string,string> ();
                            parm.Add("@Cliente", Tbcod.Rows[0]["codcli_exactus"].ToString().Trim());
                            parm.Add("@Vendedor", "9999");
                            parm.Add("@Condicion_Pago", Tb.Rows[0]["condicion_pago"].ToString());
                            parm.Add("@Clase_Documento", Tb.Rows[0]["clase_documento"].ToString());
                            parm.Add("@UsuCrea", Clases.Statics.usuario);                            
                            parm.Add("@CargadoERP", "N");
                            parm.Add("@codsoc", cmbSociedad.SelectedValue);
                            parm.Add("@Nota", "Pedido Automatico General");
                            parm.Add("@Estado", "0");
                            parm.Add("@Origen", "PAUT");
                            parm.Add("@Total", "0");
                            parm.Add("@ImprimeCh", "1");

                            query += " INSERT INTO " + cmbSociedad.SelectedItem.Tag  + ".[dbo].[Mvp_Solicitudes] " +
                            " (Cliente ,Vendedor ,Condicion_Pago ,Clase_Documento ,UsuCrea ,FechaCrea ,CargadoERP ,codsoc " +
                            " ,Nota ,Estado ,Origen ,Total ,ImprimeCh )  VALUES " +
                            " (@Cliente ,@Vendedor ,@Condicion_Pago ,@Clase_Documento ,@UsuCrea ,GETDATE() ,@CargadoERP ,@codsoc " +
                            " ,@Nota ,@Estado ,@Origen ,@Total ,@ImprimeCh ) ";

                            query += " DECLARE @CODP AS INT = @@IDENTITY ";

                            int contador = 1;
                            int PROCESADO = 0;
                            int procesar = 0;
                            foreach (DataGridViewRow gd in Dg.Rows)
                            {
                                if (Convert.ToBoolean(gd.Cells ["Sel"].Value )==true)
                                {
                                    Dictionary<string, string> parametro = new Dictionary<string, string>();
                                    parametro.Add("@soc", (cmbSociedad.SelectedValue).ToString());
                                    parametro.Add("@articulo", (gd.Cells["Codigo"].Value).ToString().Trim());
                                    parametro.Add("@proveedor", (gd.Cells["Proveedor"].Value).ToString().Trim());
                                    parametro.Add("@bodega", cmbBodega.SelectedValue.ToString());
                                    parametro.Add("@sucursal", cmbSuc.SelectedValue.ToString());
                                    
                                   
                                    if (Convert.ToInt32(gd.Cells["Existencia"].Value) <  Convert.ToInt32(gd.Cells["Cantidad"].Value))
                                    {
                                        if (Convert.ToInt32(gd.Cells["Existencia"].Value) == 0)
                                        {
                                            MessageBox.Show("No se poseen existencias del articulo", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);                                          
                                        }
                                        else
                                        {
                                            query += string.Format(" INSERT INTO " + cmbSociedad.SelectedItem.Tag + ".[dbo].[Mvp_SolicitudDetalle] " +
                                             "( IdSolicitud ,Oferta ,Articulo ,Cantidad ,Descuento ,ItemId  ,Selectivo ) " +
                                             " VALUES (@CODP,'','{0}','{1}',0,'{2}',0) ", gd.Cells["Proveedor"].Value, gd.Cells["Existencia"].Value, contador);

                                            parametro.Add("@existencia", (gd.Cells["Existencia"].Value).ToString());
                                        
                                            TbPendientes = _Sql.ExecuteQuery(string.Format(" EXEC Paut_spActualizarExistencia  " + 1 + ", @soc, @articulo, @proveedor, @bodega, @sucursal, @existencia"), parametro);
                                                                                                                                                                          
                                            MessageBox.Show("Las existencias del articulo son menores a la cantidad de Pedido");
                                          
                                            PROCESADO++;
                                        }
                                        
                                    }
                                    else
                                    {
                                        query += string.Format(" INSERT INTO " + cmbSociedad.SelectedItem.Tag + ".[dbo].[Mvp_SolicitudDetalle] " +
                                       "( IdSolicitud ,Oferta ,Articulo ,Cantidad ,Descuento ,ItemId  ,Selectivo ) " +
                                       " VALUES (@CODP,'','{0}','{1}',0,'{2}',0) ", gd.Cells["Proveedor"].Value, gd.Cells["Facturar"].Value, contador);

                                        parametro.Add("@existencia", (gd.Cells["Cantidad"].Value).ToString());
                                        TbPendientes = _Sql.ExecuteQuery(string.Format(" EXEC Paut_spActualizarExistencia " + 2 +", @soc, @articulo, @proveedor, @bodega, @sucursal, @existencia"), parametro);
                                        
                                        PROCESADO++;    
                                    }

                                    contador++;                                   
                                }                                
                            }

                            
                            bool st = _Sql.ExecuteNoResult(query, parm);
                            if (st == true)
                            {
                                if (PROCESADO > 0)
                                {
                                    _Desing.SendMessage("Pedido Creado Correctamente", "2");
                                   // BtnDel_Click(null, null);
                                    removerfilas_actualizar(true); 
                                }
                               
                            }
                            else
                            {
                                _Desing.SendMessage("Error al Crear el Pedido", "1");
                            }
                        }
                        else {

                            MessageBox.Show("Informacion de sucursal incompleta", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }

                    }
            
                } 
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void Dg_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (Dg.CurrentCell != null)
            {
                if (Dg.Columns[Dg.CurrentCell.ColumnIndex].Name == "Sel")
                {
                    if (Dg.IsCurrentCellDirty)
                    {
                       // Dg.CommitEdit(DataGridViewDataErrorContexts.Commit);
                        Dg.CommitEdit(DataGridViewDataErrorContexts.CurrentCellChange);
                    }
                }
            }            
        }

        private void chtodos_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ch = sender as CheckBox ;
            foreach (DataGridViewRow item in Dg.Rows)
            {
                item.Cells["Sel"].Value = ch.Checked;
            }
        }


        public  void BuscaClasificacion(RibbonTextBox Txt, int numero)
        {
            try
            {
                DataTable TbCla = _Sql.ExecuteQuery(string.Format(" SELECT  CLASIFICACION ,DESCRIPCION FROM Dva_VwClasificacion WHERE AGRUPACION='{0}' ", numero));
                FrmBusqueda Bus = new FrmBusqueda(1,TbCla);
                Bus.ShowDialog();
                if (Bus.Codigo != "")
                {
                    Txt.TextBoxText = Bus.Codigo.ToString().Trim();
                }
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        //private void TxtCla1_DoubleClick(object sender, EventArgs e)
        //{
        //    BuscaClasificacion(TxtCla1, 1);
        //}

        //private void TxtCla2_DoubleClick(object sender, EventArgs e)
        //{
        //    BuscaClasificacion(TxtCla2, 2);
        //}

        //private void TxtCla3_DoubleClick(object sender, EventArgs e)
        //{
        //    BuscaClasificacion(TxtCla3, 3);
        //}

        //private void TxtCla4_DoubleClick(object sender, EventArgs e)
        //{
        //    BuscaClasificacion(TxtCla4, 4);
        //}

        //private void TxtCla5_DoubleClick(object sender, EventArgs e)
        //{
        //    BuscaClasificacion(TxtCla5, 5);
        //}

        //private void TxtCla6_DoubleClick(object sender, EventArgs e)
        //{
        //    BuscaClasificacion(TxtCla6, 6);
        //}

        //private void BtnBuscar_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        LoadPendientes(true);
        //    }
        //    catch (Exception Ex)
        //    {
        //        _Desing.SendMessage(Ex.Message, "1");
        //    }
        //}

        //private void TxtCla1_TextBoxTextChanged(object sender, EventArgs e)
        //{
        //    RibbonTextBox ct = sender as RibbonTextBox;
        //    string dato = ct.TextBoxText.Trim();

        //    if (dato!="")
        //    {
        //        foreach (RibbonItem ctl in ribbonPanel5.Items)
        //        {
        //            RibbonTextBox tx = ctl as RibbonTextBox;
        //            if (tx.Tag != ct.Tag )
        //            {
        //                tx.TextBoxText = "";
        //            }  
        //        } 
        //    }       
        //}

        private void cmbSociedad_DropDownItemClicked(object sender, RibbonItemEventArgs e)
        {
            try
            {
                cargarBodega();
                LoadPendientes(false);
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
           
            
        }

        private void btn_Reset_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("¿Desea Inicializar el tablero?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Reinicializar();
                    LoadPendientes(false);
                    _Desing.SendMessage("Proceso realizado con exito", "2");
                }
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }           
           
        }

        private void Dg_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
            

    }//fin clase
}
 