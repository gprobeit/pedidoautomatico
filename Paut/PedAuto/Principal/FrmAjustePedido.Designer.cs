﻿namespace PedAuto.Formularios
{
    partial class FrmAjustePedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_aceptar = new System.Windows.Forms.Button();
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_metro_sur = new System.Windows.Forms.TextBox();
            this.txt_merliot = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_mascota = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_proveedor = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_existencias = new System.Windows.Forms.TextBox();
            this.txt_abastecido = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_santa_elena = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_sento = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_los_sueños = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_metrocentro = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_galerias = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txt_bambu = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.lbl_mascota = new System.Windows.Forms.Label();
            this.lbl_merliot = new System.Windows.Forms.Label();
            this.lbl_metrosur = new System.Windows.Forms.Label();
            this.lbl_bambu = new System.Windows.Forms.Label();
            this.lbl_metrocentro = new System.Windows.Forms.Label();
            this.lbl_galerias = new System.Windows.Forms.Label();
            this.lbl_santa_elena = new System.Windows.Forms.Label();
            this.lbl_sento = new System.Windows.Forms.Label();
            this.lbl_sueños = new System.Windows.Forms.Label();
            this.lbl_sueños2 = new System.Windows.Forms.Label();
            this.txt_los_sueños2 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.lbl_multiplaza = new System.Windows.Forms.Label();
            this.txt_multiplaza = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lbl_santarosa = new System.Windows.Forms.Label();
            this.txt_santarosa = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(230)))), ((int)(((byte)(246)))));
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(342, 30);
            this.panel1.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(112, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "Ajuste de Pedido";
            // 
            // btn_aceptar
            // 
            this.btn_aceptar.Image = global::PedAuto.Properties.Resources.save_small;
            this.btn_aceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_aceptar.Location = new System.Drawing.Point(68, 489);
            this.btn_aceptar.Name = "btn_aceptar";
            this.btn_aceptar.Size = new System.Drawing.Size(85, 33);
            this.btn_aceptar.TabIndex = 5;
            this.btn_aceptar.Text = "Aceptar";
            this.btn_aceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_aceptar.UseVisualStyleBackColor = true;
            this.btn_aceptar.Click += new System.EventHandler(this.btn_aceptar_Click);
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Image = global::PedAuto.Properties.Resources.borrar_small;
            this.btn_cancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancelar.Location = new System.Drawing.Point(189, 489);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(85, 33);
            this.btn_cancelar.TabIndex = 6;
            this.btn_cancelar.Text = "Cancelar";
            this.btn_cancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(83, 191);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Metro Sur:";
            // 
            // txt_metro_sur
            // 
            this.txt_metro_sur.Location = new System.Drawing.Point(156, 188);
            this.txt_metro_sur.MaxLength = 6;
            this.txt_metro_sur.Name = "txt_metro_sur";
            this.txt_metro_sur.Size = new System.Drawing.Size(71, 20);
            this.txt_metro_sur.TabIndex = 4;
            this.txt_metro_sur.Text = "0";
            this.txt_metro_sur.TextChanged += new System.EventHandler(this.txt_metro_sur_TextChanged);
            this.txt_metro_sur.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_mascota_KeyPress);
            // 
            // txt_merliot
            // 
            this.txt_merliot.Location = new System.Drawing.Point(156, 158);
            this.txt_merliot.MaxLength = 6;
            this.txt_merliot.Name = "txt_merliot";
            this.txt_merliot.Size = new System.Drawing.Size(71, 20);
            this.txt_merliot.TabIndex = 3;
            this.txt_merliot.Text = "0";
            this.txt_merliot.TextChanged += new System.EventHandler(this.txt_merliot_TextChanged);
            this.txt_merliot.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_mascota_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(98, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Merliot:";
            // 
            // txt_mascota
            // 
            this.txt_mascota.Location = new System.Drawing.Point(156, 128);
            this.txt_mascota.MaxLength = 6;
            this.txt_mascota.Name = "txt_mascota";
            this.txt_mascota.Size = new System.Drawing.Size(71, 20);
            this.txt_mascota.TabIndex = 2;
            this.txt_mascota.Text = "0";
            this.txt_mascota.TextChanged += new System.EventHandler(this.txt_mascota_TextChanged);
            this.txt_mascota.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_mascota_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(73, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "La Mascota:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(39, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Cod. Proveedor:";
            // 
            // txt_proveedor
            // 
            this.txt_proveedor.Location = new System.Drawing.Point(156, 40);
            this.txt_proveedor.MaxLength = 20;
            this.txt_proveedor.Name = "txt_proveedor";
            this.txt_proveedor.ReadOnly = true;
            this.txt_proveedor.Size = new System.Drawing.Size(119, 20);
            this.txt_proveedor.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(63, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Existencias:";
            // 
            // txt_existencias
            // 
            this.txt_existencias.Location = new System.Drawing.Point(156, 69);
            this.txt_existencias.MaxLength = 20;
            this.txt_existencias.Name = "txt_existencias";
            this.txt_existencias.ReadOnly = true;
            this.txt_existencias.Size = new System.Drawing.Size(100, 20);
            this.txt_existencias.TabIndex = 17;
            // 
            // txt_abastecido
            // 
            this.txt_abastecido.Location = new System.Drawing.Point(156, 98);
            this.txt_abastecido.MaxLength = 20;
            this.txt_abastecido.Name = "txt_abastecido";
            this.txt_abastecido.ReadOnly = true;
            this.txt_abastecido.Size = new System.Drawing.Size(100, 20);
            this.txt_abastecido.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(54, 101);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Reabastecer:";
            // 
            // txt_santa_elena
            // 
            this.txt_santa_elena.Location = new System.Drawing.Point(156, 425);
            this.txt_santa_elena.MaxLength = 6;
            this.txt_santa_elena.Name = "txt_santa_elena";
            this.txt_santa_elena.Size = new System.Drawing.Size(71, 20);
            this.txt_santa_elena.TabIndex = 20;
            this.txt_santa_elena.Text = "0";
            this.txt_santa_elena.TextChanged += new System.EventHandler(this.txt_santa_elena_TextChanged);
            this.txt_santa_elena.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_santa_elena_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(69, 425);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "Santa Elena:";
            // 
            // txt_sento
            // 
            this.txt_sento.Location = new System.Drawing.Point(156, 394);
            this.txt_sento.MaxLength = 6;
            this.txt_sento.Name = "txt_sento";
            this.txt_sento.Size = new System.Drawing.Size(71, 20);
            this.txt_sento.TabIndex = 21;
            this.txt_sento.Text = "0";
            this.txt_sento.TextChanged += new System.EventHandler(this.txt_sento_TextChanged);
            this.txt_sento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_santa_elena_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(99, 397);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Sento:";
            // 
            // txt_los_sueños
            // 
            this.txt_los_sueños.Location = new System.Drawing.Point(156, 308);
            this.txt_los_sueños.MaxLength = 6;
            this.txt_los_sueños.Name = "txt_los_sueños";
            this.txt_los_sueños.Size = new System.Drawing.Size(71, 20);
            this.txt_los_sueños.TabIndex = 22;
            this.txt_los_sueños.Text = "0";
            this.txt_los_sueños.TextChanged += new System.EventHandler(this.txt_los_sueños_TextChanged);
            this.txt_los_sueños.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_santa_elena_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(73, 311);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Los Sueños:";
            // 
            // txt_metrocentro
            // 
            this.txt_metrocentro.Location = new System.Drawing.Point(156, 248);
            this.txt_metrocentro.MaxLength = 6;
            this.txt_metrocentro.Name = "txt_metrocentro";
            this.txt_metrocentro.Size = new System.Drawing.Size(71, 20);
            this.txt_metrocentro.TabIndex = 26;
            this.txt_metrocentro.Text = "0";
            this.txt_metrocentro.TextChanged += new System.EventHandler(this.txt_metrocentro_TextChanged);
            this.txt_metrocentro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_santa_elena_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(70, 251);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "MetroCentro:";
            // 
            // txt_galerias
            // 
            this.txt_galerias.Location = new System.Drawing.Point(156, 218);
            this.txt_galerias.MaxLength = 6;
            this.txt_galerias.Name = "txt_galerias";
            this.txt_galerias.Size = new System.Drawing.Size(71, 20);
            this.txt_galerias.TabIndex = 27;
            this.txt_galerias.Text = "0";
            this.txt_galerias.TextChanged += new System.EventHandler(this.txt_galerias_TextChanged);
            this.txt_galerias.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_santa_elena_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(90, 221);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "Galerias:";
            // 
            // txt_bambu
            // 
            this.txt_bambu.Location = new System.Drawing.Point(156, 278);
            this.txt_bambu.MaxLength = 6;
            this.txt_bambu.Name = "txt_bambu";
            this.txt_bambu.Size = new System.Drawing.Size(71, 20);
            this.txt_bambu.TabIndex = 30;
            this.txt_bambu.Text = "0";
            this.txt_bambu.TextChanged += new System.EventHandler(this.txt_bambu_TextChanged);
            this.txt_bambu.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_santa_elena_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(94, 281);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "Bambu:";
            // 
            // lbl_mascota
            // 
            this.lbl_mascota.AutoSize = true;
            this.lbl_mascota.Location = new System.Drawing.Point(248, 131);
            this.lbl_mascota.Name = "lbl_mascota";
            this.lbl_mascota.Size = new System.Drawing.Size(13, 13);
            this.lbl_mascota.TabIndex = 32;
            this.lbl_mascota.Text = "0";
            // 
            // lbl_merliot
            // 
            this.lbl_merliot.AutoSize = true;
            this.lbl_merliot.Location = new System.Drawing.Point(248, 161);
            this.lbl_merliot.Name = "lbl_merliot";
            this.lbl_merliot.Size = new System.Drawing.Size(13, 13);
            this.lbl_merliot.TabIndex = 33;
            this.lbl_merliot.Text = "0";
            // 
            // lbl_metrosur
            // 
            this.lbl_metrosur.AutoSize = true;
            this.lbl_metrosur.Location = new System.Drawing.Point(248, 192);
            this.lbl_metrosur.Name = "lbl_metrosur";
            this.lbl_metrosur.Size = new System.Drawing.Size(13, 13);
            this.lbl_metrosur.TabIndex = 34;
            this.lbl_metrosur.Text = "0";
            // 
            // lbl_bambu
            // 
            this.lbl_bambu.AutoSize = true;
            this.lbl_bambu.Location = new System.Drawing.Point(248, 281);
            this.lbl_bambu.Name = "lbl_bambu";
            this.lbl_bambu.Size = new System.Drawing.Size(13, 13);
            this.lbl_bambu.TabIndex = 37;
            this.lbl_bambu.Text = "0";
            // 
            // lbl_metrocentro
            // 
            this.lbl_metrocentro.AutoSize = true;
            this.lbl_metrocentro.Location = new System.Drawing.Point(248, 251);
            this.lbl_metrocentro.Name = "lbl_metrocentro";
            this.lbl_metrocentro.Size = new System.Drawing.Size(13, 13);
            this.lbl_metrocentro.TabIndex = 36;
            this.lbl_metrocentro.Text = "0";
            // 
            // lbl_galerias
            // 
            this.lbl_galerias.AutoSize = true;
            this.lbl_galerias.Location = new System.Drawing.Point(248, 221);
            this.lbl_galerias.Name = "lbl_galerias";
            this.lbl_galerias.Size = new System.Drawing.Size(13, 13);
            this.lbl_galerias.TabIndex = 35;
            this.lbl_galerias.Text = "0";
            // 
            // lbl_santa_elena
            // 
            this.lbl_santa_elena.AutoSize = true;
            this.lbl_santa_elena.Location = new System.Drawing.Point(248, 425);
            this.lbl_santa_elena.Name = "lbl_santa_elena";
            this.lbl_santa_elena.Size = new System.Drawing.Size(13, 13);
            this.lbl_santa_elena.TabIndex = 40;
            this.lbl_santa_elena.Text = "0";
            // 
            // lbl_sento
            // 
            this.lbl_sento.AutoSize = true;
            this.lbl_sento.Location = new System.Drawing.Point(248, 397);
            this.lbl_sento.Name = "lbl_sento";
            this.lbl_sento.Size = new System.Drawing.Size(13, 13);
            this.lbl_sento.TabIndex = 39;
            this.lbl_sento.Text = "0";
            // 
            // lbl_sueños
            // 
            this.lbl_sueños.AutoSize = true;
            this.lbl_sueños.Location = new System.Drawing.Point(248, 311);
            this.lbl_sueños.Name = "lbl_sueños";
            this.lbl_sueños.Size = new System.Drawing.Size(13, 13);
            this.lbl_sueños.TabIndex = 38;
            this.lbl_sueños.Text = "0";
            // 
            // lbl_sueños2
            // 
            this.lbl_sueños2.AutoSize = true;
            this.lbl_sueños2.Location = new System.Drawing.Point(248, 340);
            this.lbl_sueños2.Name = "lbl_sueños2";
            this.lbl_sueños2.Size = new System.Drawing.Size(13, 13);
            this.lbl_sueños2.TabIndex = 43;
            this.lbl_sueños2.Text = "0";
            // 
            // txt_los_sueños2
            // 
            this.txt_los_sueños2.Location = new System.Drawing.Point(156, 337);
            this.txt_los_sueños2.MaxLength = 6;
            this.txt_los_sueños2.Name = "txt_los_sueños2";
            this.txt_los_sueños2.Size = new System.Drawing.Size(71, 20);
            this.txt_los_sueños2.TabIndex = 41;
            this.txt_los_sueños2.Text = "0";
            this.txt_los_sueños2.TextChanged += new System.EventHandler(this.txt_los_sueños2_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(66, 340);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 13);
            this.label15.TabIndex = 42;
            this.label15.Text = "Los Sueños2:";
            // 
            // lbl_multiplaza
            // 
            this.lbl_multiplaza.AutoSize = true;
            this.lbl_multiplaza.Location = new System.Drawing.Point(248, 368);
            this.lbl_multiplaza.Name = "lbl_multiplaza";
            this.lbl_multiplaza.Size = new System.Drawing.Size(13, 13);
            this.lbl_multiplaza.TabIndex = 46;
            this.lbl_multiplaza.Text = "0";
            // 
            // txt_multiplaza
            // 
            this.txt_multiplaza.Location = new System.Drawing.Point(156, 365);
            this.txt_multiplaza.MaxLength = 6;
            this.txt_multiplaza.Name = "txt_multiplaza";
            this.txt_multiplaza.Size = new System.Drawing.Size(71, 20);
            this.txt_multiplaza.TabIndex = 44;
            this.txt_multiplaza.Text = "0";
            this.txt_multiplaza.TextChanged += new System.EventHandler(this.txt_multiplaza_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(81, 368);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(57, 13);
            this.label17.TabIndex = 45;
            this.label17.Text = "Multiplaza:";
            // 
            // lbl_santarosa
            // 
            this.lbl_santarosa.AutoSize = true;
            this.lbl_santarosa.Location = new System.Drawing.Point(248, 457);
            this.lbl_santarosa.Name = "lbl_santarosa";
            this.lbl_santarosa.Size = new System.Drawing.Size(13, 13);
            this.lbl_santarosa.TabIndex = 49;
            this.lbl_santarosa.Text = "0";
            // 
            // txt_santarosa
            // 
            this.txt_santarosa.Location = new System.Drawing.Point(156, 454);
            this.txt_santarosa.MaxLength = 6;
            this.txt_santarosa.Name = "txt_santarosa";
            this.txt_santarosa.Size = new System.Drawing.Size(71, 20);
            this.txt_santarosa.TabIndex = 47;
            this.txt_santarosa.Text = "0";
            this.txt_santarosa.TextChanged += new System.EventHandler(this.txt_santarosa_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(71, 457);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 13);
            this.label16.TabIndex = 48;
            this.label16.Text = "Santa Rosa:";
            // 
            // FrmAjustePedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(342, 530);
            this.ControlBox = false;
            this.Controls.Add(this.lbl_santarosa);
            this.Controls.Add(this.txt_santarosa);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lbl_multiplaza);
            this.Controls.Add(this.txt_multiplaza);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.lbl_sueños2);
            this.Controls.Add(this.txt_los_sueños2);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.lbl_santa_elena);
            this.Controls.Add(this.lbl_sento);
            this.Controls.Add(this.lbl_sueños);
            this.Controls.Add(this.lbl_bambu);
            this.Controls.Add(this.lbl_metrocentro);
            this.Controls.Add(this.lbl_galerias);
            this.Controls.Add(this.lbl_metrosur);
            this.Controls.Add(this.lbl_merliot);
            this.Controls.Add(this.lbl_mascota);
            this.Controls.Add(this.txt_bambu);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txt_metrocentro);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txt_galerias);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txt_santa_elena);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txt_sento);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txt_los_sueños);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txt_abastecido);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txt_existencias);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txt_proveedor);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txt_mascota);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_merliot);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_metro_sur);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.btn_aceptar);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAjustePedido";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmAjustePedido_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_aceptar;
        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txt_metro_sur;
        public System.Windows.Forms.TextBox txt_merliot;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txt_mascota;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txt_proveedor;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txt_existencias;
        public System.Windows.Forms.TextBox txt_abastecido;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txt_santa_elena;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txt_sento;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txt_los_sueños;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txt_metrocentro;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox txt_galerias;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.TextBox txt_bambu;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbl_mascota;
        private System.Windows.Forms.Label lbl_merliot;
        private System.Windows.Forms.Label lbl_metrosur;
        private System.Windows.Forms.Label lbl_bambu;
        private System.Windows.Forms.Label lbl_metrocentro;
        private System.Windows.Forms.Label lbl_galerias;
        private System.Windows.Forms.Label lbl_santa_elena;
        private System.Windows.Forms.Label lbl_sento;
        private System.Windows.Forms.Label lbl_sueños;
        private System.Windows.Forms.Label lbl_sueños2;
        public System.Windows.Forms.TextBox txt_los_sueños2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbl_multiplaza;
        public System.Windows.Forms.TextBox txt_multiplaza;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbl_santarosa;
        public System.Windows.Forms.TextBox txt_santarosa;
        private System.Windows.Forms.Label label16;
    }
}