﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;


namespace PedAuto.Formularios
{
    public partial class FrmLogueo : PedAuto.Formularios.FrmBase
    {

        #region var
        FrmPrincipal FrmP = new FrmPrincipal();
        #endregion

        #region metodos

        public void cargar_soc()
        {
            try 
	        {	        
		        DataTable dt = _Sql.ExecuteQuery(string.Format("SELECT * FROM Paut_VwSociedad_login  WHERE (tipo_sociedad IS NOT NULL)"));
                cmb_soc.DataSource = dt;
                cmb_soc.ValueMember = "tipo_sociedad";
                cmb_soc.DisplayMember = "sociedad";     
	        }
	        catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        public FrmLogueo(FrmPrincipal Pr)
        {
            InitializeComponent();
            FrmP = Pr;
        }

        private void loguin()
        {
            try
            {
                if (TxtUser.Text.Trim() != "")
                {
                    DataTable Dt2 = _Sql.ExecuteQuery(String.Format("EXEC Grl_spUsrclvdsf 'V', '{0}', '{1}'", TxtUser.Text, TxtPass.Text));
                    if (Dt2.Rows.Count > 0)
                    {
                        if (Dt2.Rows[0][0].ToString().Equals("S"))
                        {
                            string sqlSend = string.Format("SELECT nivel FROM Grl_Seg_Usuario WHERE usuario='{0}' and activo='S'", TxtUser.Text);
                            DataTable Tabla = _Sql.ExecuteQuery(sqlSend);
                            if (!Tabla.Rows.Count.Equals(0))
                            {
                                Clases.Statics.TbAccesos = _Sql.ExecuteQuery(string.Format("SELECT  CodigoOpcion ,Compania FROM Grl_Seg_Accesos WHERE CodigoSistema =55 and usuario ='{0}' ", TxtUser.Text));

                                if (Clases.Statics.TbAccesos.Rows.Count > 0)
                                {
                                    if (cmb_soc.SelectedValue.ToString() == "VS")
                                    {
                                        if (Clases.Statics.TbAccesos.Select("CodigoOpcion='5.0'").Length > 0 ? true : false)
                                        {
                                            Clases.Statics.codsuc_ususario(TxtUser.Text.Trim());

                                            Clases.Statics.usuario = TxtUser.Text.Trim();
                                            FrmP.TxtUsuario.Text = "Usuario: " + TxtUser.Text.Trim();
                                            FrmP.txtsociedad.Text = cmb_soc.Text;
                                            Clases.Statics.sociedad = cmb_soc.SelectedValue.ToString();
                                            FrmP.toolconfig.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='4.0'").Length > 0 ? true : false;
                                            FrmP.tooltablero.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='1.0'").Length > 0 ? true : false;
                                            FrmP.PAUT_tool.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='1.1'").Length > 0 ? true : false;
                                            FrmP.toolConsulta.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='7.0'").Length > 0 ? true : false;
                                            FrmP.ToolsInventario.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='7.1'").Length > 0 ? true : false;
                                            FrmP.tool_Administracion.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='8.0'").Length > 0 ? true : false;
                                            FrmP.excluir_articulo_tool.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='8.1'").Length > 0 ? true : false;
                                            Clases.Statics.conjunto = "1,2";
                                            this.Close();
                                        }
                                        else 
                                        { 
                                            _Desing.SendMessage("Usuario No Tiene Acceso a Ingresar con Sociedad " + cmb_soc.Text, "3"); 
                                        }
                                    }
                                    else if (cmb_soc.SelectedValue.ToString() == "I")
                                    {
                                        if (Clases.Statics.TbAccesos.Select("CodigoOpcion='6.0'").Length > 0 ? true : false)
                                        {
                                            Clases.Statics.codsuc_ususario(TxtUser.Text.Trim());

                                            Clases.Statics.usuario = TxtUser.Text.Trim();
                                            FrmP.TxtUsuario.Text = "Usuario: " + TxtUser.Text.Trim();
                                            FrmP.txtsociedad.Text = cmb_soc.Text;
                                            Clases.Statics.sociedad = cmb_soc.SelectedValue.ToString();
                                            FrmP.toolconfig.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='4.0'").Length > 0 ? true : false;
                                            FrmP.PAUT_tool.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='1.1'").Length > 0 ? true : false;
                                            FrmP.tooltablero.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='1.0'").Length > 0 ? true : false;
                                            FrmP.toolConsulta.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='7.0'").Length > 0 ? true : false;
                                            FrmP.ToolsInventario.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='7.1'").Length > 0 ? true : false;
                                            FrmP.tool_Administracion.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='8.0'").Length > 0 ? true : false;
                                            FrmP.excluir_articulo_tool.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='8.1'").Length > 0 ? true : false;
                                            Clases.Statics.conjunto = "3";

                                            this.Close();
                                        }
                                        else { _Desing.SendMessage("Usuario No Tiene Acceso a Ingresar con Sociedad " + cmb_soc.Text, "3"); }
                                    }
                                    else if (cmb_soc.SelectedValue.ToString() == "T")
                                    {
                                        if (Clases.Statics.TbAccesos.Select("CodigoOpcion='9.0'").Length > 0 ? true : false)
                                        {
                                            Clases.Statics.codsuc_ususario(TxtUser.Text.Trim());

                                            Clases.Statics.usuario = TxtUser.Text.Trim();
                                            FrmP.TxtUsuario.Text = "Usuario: " + TxtUser.Text.Trim();
                                            FrmP.txtsociedad.Text = cmb_soc.Text;
                                            Clases.Statics.sociedad = cmb_soc.SelectedValue.ToString();
                                            FrmP.toolconfig.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='4.0'").Length > 0 ? true : false;
                                            FrmP.PAUT_tool.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='1.1'").Length > 0 ? true : false;
                                            FrmP.tooltablero.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='1.0'").Length > 0 ? true : false;
                                            FrmP.toolConsulta.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='7.0'").Length > 0 ? true : false;
                                            FrmP.ToolsInventario.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='7.1'").Length > 0 ? true : false;
                                            FrmP.tool_Administracion.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='8.0'").Length > 0 ? true : false;
                                            FrmP.excluir_articulo_tool.Visible = Clases.Statics.TbAccesos.Select("CodigoOpcion='8.1'").Length > 0 ? true : false;
                                            Clases.Statics.conjunto = "4";

                                            this.Close();
                                        }
                                        else { _Desing.SendMessage("Usuario No Tiene Acceso a Ingresar con Sociedad " + cmb_soc.Text, "3"); }
                                    }
                                }
                                else { _Desing.SendMessage("Usuario No Tiene Acceso a Ingresar al Sistema", "3"); }
                            }
                        }
                        else { _Desing.SendMessage("Usuario No Encontrado", "3"); }
                    }
                    else { _Desing.SendMessage("Datos Incorrectos", "3"); }
                }
                else _Desing.SendMessage("Por favor, Ingrese su Usuario", "3");
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        #endregion
        
        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                loguin();
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
            FrmP.Close();
        }
               
        private void FrmLogueo_Load(object sender, EventArgs e)
        {         
            cargar_soc();
            if (cmb_soc.Items.Count == 0)
            {
                FrmConfig frm = new FrmConfig();
                frm.ShowDialog();
            }
        }

        private void FrmLogueo_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (cmb_soc.Items.Count > 0)
            {
                if (Clases.Statics.usuario == "")
                {                  
                    FrmP.Close();
                }
            }
        }

      
    }
}
