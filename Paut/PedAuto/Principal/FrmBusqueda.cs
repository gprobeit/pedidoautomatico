﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PedAuto.Formularios
{
    public partial class FrmBusqueda : PedAuto.Formularios.FrmBase
    {
        int _TipoBusqueda;
        DataTable _Catalogo;
        string[] _Columnas;
        string _Tabla;



        public string Codigo="";


        public FrmBusqueda(int tipoB, DataTable Ca )
        {
            InitializeComponent();
            _TipoBusqueda = tipoB;
            _Catalogo = Ca;
        }

        public FrmBusqueda(int tipoB, string[] Co,string Tb)
        {
            InitializeComponent();
            _TipoBusqueda = tipoB;
            _Columnas = Co;
            _Tabla = Tb;
        }

        private void FiltrarItems( )
        {
            try
            {
                switch (_TipoBusqueda)
                {
                    case 1:
                        string Sql = string.Empty;

                        foreach (DataColumn Cl in _Catalogo.Columns)
                        {
                            if (Sql.Equals(string.Empty))
                            {
                                Sql = string.Format("{0} LIKE '%{1}%'", Cl.ColumnName.ToString(), textBox1.Text);
                            }
                            else
                            {
                                Sql += string.Format(" OR {0} LIKE '%{1}%'", Cl.ColumnName.ToString(), textBox1.Text);
                            }

                        }

                        DataRow[] dr = _Catalogo.Select("(" + Sql + ")");
                        if (dr.Length > 0)
                        {
                            DataTable dt1 = dr.CopyToDataTable<DataRow>();
                            if (dt1.Rows.Count > 0)
                            {
                                dataGridView1.DataSource = dt1;
                                //dataGridView1.Columns[1].Width = 300;
                                dataGridView1.Focus();
                            }
                        }
                        else
                        {
                            dataGridView1.DataSource = null;
                        }
                        break;
                    case 2:
                        string Condi = string.Empty;
                        string Cols = string.Empty;

                        for (int i = 0; i < _Columnas.Length; i++)
                        {
                            if (Condi.Equals(string.Empty))
                            {
                                Condi = string.Format(_Columnas[i] + " LIKE '%{0}%'", textBox1.Text);
                                Cols = _Columnas[i];
                            }
                            else
                            {
                                Condi += string.Format(" OR " + _Columnas[i] + " LIKE '%{0}%'", textBox1.Text);
                                Cols += "," + _Columnas[i];
                            }
                        }
                        dataGridView1.DataSource =
                        _Sql.ExecuteQuery("SELECT " + Cols + " FROM " + _Tabla + " WHERE " + Condi);

                        break;
                }
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            FiltrarItems();
        }

        private void FrmBusqueda_Load(object sender, EventArgs e)
        {
            FiltrarItems();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode ==  Keys.Enter )
            {
                FiltrarItems();
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >=0 )
            {
                Codigo = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString().Trim();
                this.Close();
            }
        }

       

    }
}
