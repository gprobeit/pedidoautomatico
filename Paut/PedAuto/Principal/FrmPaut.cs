﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using PedAuto.Clases;
using System.Threading;


namespace PedAuto.Formularios
{
    public partial class FrmPaut : Form
    {
        public FrmPaut()
        {
            InitializeComponent();
        }

        #region Variables
            DataTable TbPendientes = new DataTable();
            FrmAjustePedido AP = new FrmAjustePedido();
            FrmBuscarArticulo f = new FrmBuscarArticulo();
            public Desing _Desing = new Desing();
            public DB _Sql = new DB();
            Mail mail = new Mail();
            public int pgb = 0;           
        #endregion

        #region metodos
                   
            private  void limpiar_var()
            {
                 f.articulo = "";
                 f.proveedor = "";
                 f.descripcion = "";
            }

            private void cargar_lbl_actualizacion()
            {
                 DataTable dt = _Sql.ExecuteQuery(string.Format("select MAX(fecha_sug) from Paut_TabControl"));

                 if (Convert.ToDateTime(dt.Rows[0][0]) > DateTime.Today)
                 {                     
                      MessageBox.Show("Los artículos necesitan ser Actualizados", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                      ep1.SetError(chtodos, "Debe actualizar el tablero");
                 }
                 lblfecha.Text = "Ultima Actualización: " + dt.Rows[0][0].ToString().Substring(0, 10);              
            }

            private void cambiar_color_fila()
            {
                int filla = 0;
                if (Dg.RowCount > 0)
                {
                    foreach (DataGridViewRow row in Dg.Rows) //para despacho
                    {
                        if (Convert.ToInt32(row.Cells["EXISTENCIA"].Value) > 0 && Convert.ToInt32(row.Cells["CANT_ABASTECER"].Value) > Convert.ToInt32(row.Cells["EXISTENCIA"].Value))
                        {
                            Dg.Rows[filla].DefaultCellStyle.BackColor = System.Drawing.Color.Yellow;
                        }
                        else if (Convert.ToInt32(row.Cells["EXISTENCIA"].Value) == 0)
                        {
                            Dg.Rows[filla].DefaultCellStyle.BackColor = System.Drawing.Color.OrangeRed;
                        }
                        else if (Convert.ToInt32(row.Cells["EXISTENCIA"].Value) >= Convert.ToInt32(row.Cells["CANT_ABASTECER"].Value))
                        {
                            Dg.Rows[filla].DefaultCellStyle.BackColor = System.Drawing.Color.White;
                        }
                        filla++;

                        
                    }
                }             
            }

            public void control_pantalla(string pantalla)
            {
                try
                {                                               
                    string query = string.Format("exec Paut_spControlAcceso 2, '" + Clases.Statics.usuario + "',  '" + pantalla + "', '' ");
                    _Sql.ExecuteNoResult(query);                    
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }                                  

            private bool validaArticulos(DataGridView grid, string colsel)
            {
                Dg.ClearSelection();
                IEnumerable<DataGridViewRow> obj = (from DataGridViewRow row in grid.Rows.Cast<DataGridViewRow>()
                                                    where Convert.ToBoolean(row.Cells[colsel].Value) == true
                                                    select row);
                if (obj.Any())
                {
                    return true;
                }

                return false;

            }

            private void cargarSoc()
            {
                cmb_soc.DropDownItems.Clear();
                DataTable Tbsoc = _Sql.ExecuteQuery(" SELECT  codsoc ,modulo,BD  FROM Paut_VwSociedad ");
                
                foreach (DataRow fs in Tbsoc.Rows)
                {
                    RibbonButton item = new RibbonButton();
                    item.Value = fs["codsoc"].ToString().Trim();
                    item.Text = fs["modulo"].ToString().Trim();
                    item.Tag = fs["BD"].ToString().Trim();
                    this.cmb_soc.DropDownItems.Add(item);

                }
                if (Tbsoc.Rows.Count > 0)
                {
                    cmb_soc.SelectedValue = Tbsoc.Rows[0]["codsoc"].ToString().Trim();
                }
            }

            private void cargarSuc()
            {
                try
                    {
                        DataTable TbSuc;

                    if (Clases.Statics.sociedad == "VS")
                    {
                        TbSuc = _Sql.ExecuteQuery(string.Format(" SELECT DISTINCT CODSUC,NOMSUC,CONJUNTO  FROM Paut_VwSucursal WHERE TIPO='VTA' and CONJUNTO IN (" + Clases.Statics.conjunto + ")"));
                    }
                    else if (Clases.Statics.sociedad == "I")
                    {
                        TbSuc = _Sql.ExecuteQuery(string.Format(" SELECT DISTINCT CODSUC,NOMSUC,CONJUNTO  FROM Paut_VwSucursal WHERE TIPO='VTA' AND CONJUNTO IN (" + Clases.Statics.conjunto + ")"));
                    }
                    else
                    {
                    TbSuc = _Sql.ExecuteQuery(string.Format(" SELECT DISTINCT CODSUC,NOMSUC,CONJUNTO  FROM Paut_VwSucursal WHERE TIPO='VTA' AND CONJUNTO IN (" + Clases.Statics.conjunto + ")"));
                    }

                cmb_suc.DropDownItems.Clear();

                    foreach (DataRow fs in TbSuc.Rows)
                    {
                        RibbonButton item = new RibbonButton();
                        item.Value = fs["CODSUC"].ToString().Trim();
                        item.Text = fs["CODSUC"].ToString().Trim() + "-" + fs["NOMSUC"].ToString().Trim();
                        item.ToolTip = fs["NOMSUC"].ToString().Trim();
                        item.ToolTipTitle = fs["CONJUNTO"].ToString().Trim();
                        cmb_suc.DropDownItems.Add(item);
                    }
                    if (TbSuc.Rows.Count > 0)
                    {
                        cmb_suc.SelectedValue = TbSuc.Rows[0]["CODSUC"].ToString().Trim();
                    }

                    RibbonButton item0 = new RibbonButton();
                    item0.Text = "TODAS";
                    item0.Value = "";
                    cmb_suc.DropDownItems.Insert(0, item0);
                    cmb_suc.SelectedValue = ("").ToString();
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }

            }

            private void cargarBodega()  
            {
                try
                {
                    cmb_bod.DropDownItems.Clear();
                    DataTable TbBod = _Sql.ExecuteQuery(string.Format(" SELECT BODEGA ,NOMBRE  FROM Paut_VwBodega WHERE SOCIEDAD ='{0}' order by nombre ", cmb_soc.SelectedValue));

                    cmb_bod.DropDownItems.Clear();

                    foreach (DataRow fs in TbBod.Rows)
                    {
                        RibbonButton item = new RibbonButton();
                        item.Value = fs["BODEGA"].ToString().Trim();
                        item.Text = fs["NOMBRE"].ToString().Trim();
                        cmb_bod.DropDownItems.Add(item);
                        cmb_bod.SelectedValue = fs["BODEGA"].ToString().Trim();
                    }
                    if (TbBod.Rows.Count > 0)
                    {
                        cmb_bod.SelectedValue = TbBod.Rows[0]["BODEGA"].ToString().Trim();
                    }
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }

            private void LoadPendientes(bool filtrar)
            {
                try
                {
                    if (cmb_soc.DropDownItems.Count > 0 && cmb_suc.DropDownItems.Count > 0 && cmb_bod.DropDownItems.Count > 0)
                    {
                        Dictionary<string, string> par = new Dictionary<string, string>();
                        par.Add("@cod_soc", cmb_soc.SelectedValue.ToString());
                        par.Add("@bodega", cmb_bod.SelectedValue.ToString());                   
                        if (cmb_suc.SelectedValue == "")
                        {
                            par.Add("@conjunto", Clases.Statics.conjunto);
                        }
                        else
                        {
                            par.Add("@conjunto", cmb_suc.SelectedItem.ToolTipTitle);
                        }
                        par.Add("@sucursal", cmb_suc.SelectedValue.ToString());       
                        if (chb_existencias.Checked == true)
                        {
                            par.Add("@existencias", "1");
                        }
                        else
                        {
                            par.Add("@existencias", "0");
                        }
                    
                        par.Add("@articulo", f.articulo);
                        par.Add("@cod_prove", f.proveedor);
                        par.Add("@descripcion", f.descripcion);
                   
                        TbPendientes = _Sql.ExecuteQuery(string.Format("EXEC Paut_spCargarPendientes @cod_soc, @bodega, @conjunto, @sucursal, @existencias, @articulo, @cod_prove, @descripcion"), par);
                    
                        if (Dg.Columns.Count >= TbPendientes.Columns.Count )
                        {
                            Dg.DataSource = null;
                        }                        
                        if (Dg.Columns.Count < TbPendientes.Columns.Count)
                        { 
                            int i = 1;
                            foreach (DataColumn dt in TbPendientes.Columns)
                            {                                                                                            
                                DataGridViewColumn dg_column = new DataGridViewTextBoxColumn();
                                dg_column.DataPropertyName = dt.ColumnName;
                                dg_column.Name = dt.ColumnName;
                                dg_column.HeaderText = dt.ColumnName;
                                dg_column.DisplayIndex = i;
                                Dg.Columns.Add(dg_column);
                                dg_column.ReadOnly = true;
                                                        
                                i++;
                            }
                            if (Dg.Columns[0].Name != "SEL")
                            {
                                DataGridViewCheckBoxColumn chk = new DataGridViewCheckBoxColumn();
                                Dg.Columns.Add(chk);
                                chk.HeaderText = "SEL";
                                chk.Name = "SEL";
                                chk.DisplayIndex = 0;
                            }
                            Dg.DataSource = TbPendientes;
                        }                       
                        TbPendientes.Dispose();               
                    }
                    lblconteo.Text = "Numero de Artículos: " + Dg.RowCount.ToString();
                    Dg.EnableHeadersVisualStyles = false;
                    Dg.ColumnHeadersDefaultCellStyle.BackColor = Color.LightSkyBlue;  
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }

            private void Reporte_filtrar()
            {
                try
                {
                    Dictionary<string, string> par = new Dictionary<string, string>();
                    par.Add("@cod_soc", cmb_soc.SelectedValue.ToString());
                    par.Add("@bodega", cmb_bod.SelectedValue.ToString());
                    if (cmb_suc.SelectedValue == "")
                    {
                        par.Add("@conjunto", Clases.Statics.conjunto);
                    }
                    else
                    {
                        par.Add("@conjunto", cmb_suc.SelectedItem.ToolTipTitle);
                    }
                    par.Add("@sucursal", cmb_suc.SelectedValue.ToString());
                    if (chb_existencias.Checked == true)
                    {
                        par.Add("@existencias", "1");
                    }
                    else
                    {
                        par.Add("@existencias", "0");
                    }

                    par.Add("@articulo", f.articulo);
                    par.Add("@cod_prove", f.proveedor);
                    par.Add("@descripcion", f.descripcion);

                    TbPendientes = _Sql.ExecuteQuery(string.Format("EXEC Paut_spCargarPendientes @cod_soc, @bodega, @conjunto, @sucursal, @existencias, @articulo, @cod_prove, @descripcion"), par);
                }
                catch (Exception)
                {
                    
                    throw;
                }                        
            }

            private void Reinicializar()
            {
                try
                {
                    Dictionary<string, string> par = new Dictionary<string, string>();
                    TbPendientes = _Sql.ExecuteQuery(string.Format(" EXEC Paut_spTabControl_UID "), par);
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }

            private void actualiza_existencia()
            {
                try
                {
                    Dictionary<string, string> par = new Dictionary<string, string>();
                    foreach (DataGridViewRow Rw in Dg.Rows)
                    {
                        par.Add("@bod", cmb_bod.SelectedValue.Trim());
                        par.Add("@art", Rw.Cells["CODIGO"].Value.ToString().Trim());
                        par.Add("@soc", cmb_soc.SelectedValue);
                        DataTable Tb = _Sql.ExecuteQuery(" SELECT CANT_DISPONIBLE FROM Paut_VwExistencia where BODEGA=@bod and ARTICULO=@art and sociedad=@soc ", par);
                        if (Tb.Rows.Count > 0)
                            Rw.Cells["Existencia"].Value = Tb.Rows[0]["CANT_DISPONIBLE"];
                        else
                            Rw.Cells["Existencia"].Value = 0;
                        par.Clear();
                    }
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }

            }

            private void removerfilas(bool borrar)
            {
                try
                {
                    string query = "";
                    Dg.ClearSelection();

                    foreach (DataGridViewRow Gr in Dg.Rows)
                    {
                        if (Convert.ToBoolean(Gr.Cells["Sel"].Value) == true) 
                        {
                            if (cmb_suc.SelectedValue != "")
                            {
                                query += string.Format("delete CTRL	FROM  GLOBAL.dbo.Paut_TabControl AS CTRL " +
                                "LEFT JOIN(select articulo,  COD_PROVEEDOR FROM Paut_VwCatalogoArticulos where (sociedad = '" + cmb_soc.SelectedValue + "')" +
                                "and CONJUNTO IN(" + cmb_suc.SelectedItem.ToolTipTitle + ") ) AS PC ON CTRL.articulo = PC.ARTICULO	" +
                                "where PC.COD_PROVEEDOR = '" + Gr.Cells["CODIGO"].Value.ToString().Trim() + "' and  CTRL.sucursal = '" + cmb_suc.SelectedValue + "'");
                                Gr.Selected = true;
                            }
                            else
                            {
                                query += string.Format("delete CTRL	FROM  GLOBAL.dbo.Paut_TabControl AS CTRL " +
                                "LEFT JOIN(select articulo,  COD_PROVEEDOR FROM Paut_VwCatalogoArticulos where (sociedad = '" + cmb_soc.SelectedValue + "')" +
                                "and CONJUNTO IN(" + Clases.Statics.conjunto + ") ) AS PC ON CTRL.articulo = PC.ARTICULO	" +
                                "where PC.COD_PROVEEDOR = '" + Gr.Cells["CODIGO"].Value.ToString().Trim() + "'");
                                Gr.Selected = true;
                            }
                        }
                    }

                    foreach (DataGridViewRow gd in Dg.SelectedRows)
                    {
                        Dg.Rows.Remove(gd);
                    }
                    if (borrar == true && query != "")
                    {
                        _Sql.ExecuteNoResult(query);
                    }

                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }

            private void removerfilas_actualizar(bool borrar)
            {
                try
                {
                    string query = "";
                    Dg.ClearSelection();

                    foreach (DataGridViewRow Gr in Dg.Rows)
                    {
                        if (Convert.ToBoolean(Gr.Cells["Sel"].Value) == true)
                        {
                            if (cmb_suc.SelectedValue != "")
                            {
                                query += string.Format("delete CTRL" + "	FROM  GLOBAL.dbo.Paut_TabControl AS CTRL " +
                                "LEFT JOIN(select articulo,  COD_PROVEEDOR FROM Paut_VwCatalogoArticulos where (sociedad = '" + cmb_soc.SelectedValue + "')" +
                                "and ( CONJUNTO in (" + cmb_suc.SelectedItem.ToolTipTitle + ") )) AS PC ON CTRL.articulo = PC.ARTICULO	" +
                                " where PC.COD_PROVEEDOR = '" + Gr.Cells["CODIGO"].Value.ToString().Trim() + "' and  CTRL.sucursal = '" + cmb_suc.SelectedValue + "' and  CTRL.cantidad = 0");
                                Gr.Selected = true;
                            }                        
                            else
                            {
                                query += string.Format("delete CTRL" + " FROM  GLOBAL.dbo.Paut_TabControl AS CTRL " +
                                "LEFT JOIN(select articulo,  COD_PROVEEDOR FROM Paut_VwCatalogoArticulos where (sociedad = '" + cmb_soc.SelectedValue + "')" +
                                "and ( CONJUNTO in (" + Clases.Statics.conjunto + ") )) AS PC ON CTRL.articulo = PC.ARTICULO	" +
                                " where PC.COD_PROVEEDOR = '" + Gr.Cells["CODIGO"].Value.ToString().Trim() + "' and CTRL.cantidad = 0");
                                Gr.Selected = true;
                            }
                        }

                    }

                    foreach (DataGridViewRow gd in Dg.SelectedRows)
                    {
                        Dg.Rows.Remove(gd);
                    }
                    if (borrar == true && query != "")
                    {
                        _Sql.ExecuteNoResult(query);
                    }

                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }

            private void cargar_progress(int columna)
            {
                progressBar1.Maximum = Dg.RowCount * columna;
                progressBar1.Minimum = pgb;
                visible_progress(1);          
            }

            private void hilo()
            {
                while (true)
                {
                    Thread.Sleep(100);
                }
            }

            private void visible_progress(int tipo)
            {
                if (tipo == 2)
                {                   
                    lbl_procesando.Visible = false;
                    progressBar1.Visible = false;
                    pgb = 0; 
                }
                if (tipo == 1)
                {
                    lbl_procesando.Visible = true;
                    progressBar1.Visible = true;
                }
                
            }

            private void Generar_Pedido_intrade(int tipo)
            {
                try
                {
                    actualiza_existencia();
                               
                    #region tipo1 pedidos por suc
                    if (tipo == 1)
                    {
                        Dictionary<string, string> par = new Dictionary<string, string>();
                        par.Add("@SUC", cmb_suc.SelectedValue);
                        par.Add("@SOC", cmb_soc.SelectedValue);
                        par.Add("@CON", Clases.Statics.conjunto);


                    //string CONSULTA = " SELECT CODSUC,NOMSUC,codcli_exactus FROM Paut_VwSucursal WHERE TIPO='VTA' and coddis=@SOC and CODSUC=@SUC and conjunto= @CON";
                    string CONSULTA = " SELECT CODSUC,NOMSUC,codcli_exactus FROM Paut_VwSucursal WHERE TIPO='VTA' and coddis=@SOC and CODSUC=@SUC";

                    DataTable Tbcod = _Sql.ExecuteQuery(CONSULTA, par);

                        if (Tbcod.Rows.Count > 0)
                        {
                            par.Clear();
                            par.Add("@cliente", Tbcod.Rows[0]["codcli_exactus"].ToString().Trim());
                            par.Add("@soc", cmb_soc.SelectedItem.Text);
                        }

                        cargar_progress(Tbcod.Rows.Count);
                        Thread trd = new Thread(hilo);
                        trd.IsBackground = true;
                        trd.Start();

                        DataTable Tb = _Sql.ExecuteQuery("SELECT  cliente,condicion_pago,clase_documento FROM  Paut_VwClientes  " +
                                                        " WHERE cliente = @cliente and sociedad = @soc", par);

                        DataTable dt = _Sql.ExecuteQuery("select codsuc, nomsuc, conjunto, venta, sugerido from Paut_vwSucursal_conjunto where conjunto='" + cmb_suc.SelectedItem.ToolTipTitle + "' and codsuc='" + cmb_suc.SelectedValue + "'  ");
                        string column_sugerido = dt.Rows[0]["sugerido"].ToString().Trim();

                        if (Tb.Rows.Count > 0)
                        {
                            string query = "";
                            string query_deta = "";

                            Dictionary<string, string> parm = new Dictionary<string, string>();
                            parm.Add("@Cliente", Tbcod.Rows[0]["codcli_exactus"].ToString().Trim());
                            parm.Add("@Vendedor", "9999");
                            parm.Add("@Condicion_Pago", Tb.Rows[0]["condicion_pago"].ToString());
                            parm.Add("@Clase_Documento", Tb.Rows[0]["clase_documento"].ToString());
                            parm.Add("@UsuCrea", Clases.Statics.usuario);
                            parm.Add("@CargadoERP", "N");
                            parm.Add("@codsoc", cmb_soc.SelectedValue);
                            parm.Add("@Nota", "Pedido Automatico General");
                            parm.Add("@Estado", "11"); // aprobado credito
                            parm.Add("@Origen", "PAUG");
                            parm.Add("@Total", "0");
                            parm.Add("@ImprimeCh", "1");

                            query += " INSERT INTO " + cmb_soc.SelectedItem.Tag + ".[dbo].[Mvp_Solicitudes] " +
                            " (Cliente ,Vendedor ,Condicion_Pago ,Clase_Documento ,UsuCrea ,FechaCrea ,CargadoERP ,codsoc " +
                            " ,Nota ,Estado ,Origen ,Total ,ImprimeCh, GrpVta)  VALUES " +
                            " (@Cliente ,@Vendedor ,@Condicion_Pago ,@Clase_Documento ,@UsuCrea ,GETDATE() ,@CargadoERP ,@codsoc " +
                            " ,@Nota ,@Estado ,@Origen ,@Total ,@ImprimeCh, 3 ) ";

                            query += " DECLARE @CODP AS INT = @@IDENTITY ";

                            int contador = 1;
                            int menor = 0;
                            foreach (DataGridViewRow gd in Dg.Rows)
                            {
                                if (Convert.ToBoolean(gd.Cells["Sel"].Value) == true)
                                {
                                    if (Convert.ToInt32(gd.Cells["EXISTENCIA"].Value) >= Convert.ToInt32(gd.Cells["CANT_ABASTECER"].Value) && Convert.ToInt32(gd.Cells[column_sugerido].Value) > 0)
                                    {
                                        DataTable dt_top = _Sql.ExecuteQuery("SELECT VALOR from  GLOBAL.dbo.Paut_parametros where TIPO = 'Existencia' and CONJUNTO in (" + Clases.Statics.conjunto + ")");

                                        int sug = Convert.ToInt32(gd.Cells[column_sugerido].Value);
                                        if (sug > Convert.ToInt32(dt_top.Rows[0][0]))
                                        {
                                            sug = Convert.ToInt32(dt_top.Rows[0][0]);
                                        }

                                        string soc = (cmb_soc.SelectedValue).ToString();
                                        DataTable Tb2 = _Sql.ExecuteQuery(" SELECT CTRL.articulo, PC.ARTICULO	FROM  GLOBAL.dbo.Paut_TabControl AS CTRL " +
                                        "LEFT JOIN(select articulo,  COD_PROVEEDOR FROM Paut_VwCatalogoArticulos where (sociedad = '" + cmb_soc.SelectedValue + "')" +
                                        "and ( CONJUNTO =  '" + cmb_suc.SelectedItem.ToolTipTitle + "' )) AS PC ON CTRL.articulo = PC.ARTICULO	" +
                                        "where PC.COD_PROVEEDOR = '" + (gd.Cells["codigo"].Value).ToString().Trim() + "' and  CTRL.sucursal = '" + cmb_suc.SelectedValue + "'");
                                        string art = Tb2.Rows[0][0].ToString().Trim();
                                        string suc = cmb_suc.SelectedValue.ToString();
                                       
                                        query += string.Format(" INSERT INTO " + cmb_soc.SelectedItem.Tag + ".[dbo].[Mvp_SolicitudDetalle] " +
                                            "( IdSolicitud ,Oferta ,Articulo ,Cantidad ,Descuento ,ItemId  ,Selectivo ) " +
                                            " VALUES (@CODP,'','{0}','{1}',0,'{2}',0) ", gd.Cells["codigo"].Value, sug, contador);

                                        query_deta += string.Format(" EXEC GLOBAL.dbo.Paut_spActualizarExistencia " + 2 + ", '" + soc + "','" + art + "','" + suc + "'," + sug);                                  
                                        contador++;
                                    }
                                    else if (Convert.ToInt32(gd.Cells["EXISTENCIA"].Value) < Convert.ToInt32(gd.Cells["CANT_ABASTECER"].Value) || Convert.ToInt32(gd.Cells["EXISTENCIA"].Value) < 1)
                                    {
                                        menor++;
                                    }
                                }
                                pgb++;
                                progressBar1.Value = pgb;
                            }

                            if (contador > 1)
                            {
                                bool st = _Sql.ExecuteNoResult(query, parm);
                                bool sp = _Sql.ExecuteNoResult(query_deta);
                                if (st == true && sp == true)
                                {
                                    visible_progress(2);
                                    /* Comentareado 26/072016 RRE  */
                                    //DataTable dt_email = _Sql.ExecuteQuery(string.Format("EXEC GLOBAL.DBO.Paut_spExistenciasCero 1,'"+ cmb_soc.SelectedValue.ToString() +"', '"+ cmb_bod.SelectedValue.ToString() +"', '"+ Clases.Statics.conjunto +"', '', 0, '', '', ''"));
                                    //mail.SendToList("164", "Advertencia de artículos con existencia 0 - PAUT", Clases.Statics.MakeHTML("Los siguientes artículos no pueden ser abastecidos de la bodega '" + cmb_bod.SelectedValue.ToString() +"'"  , dt_email));
                                    removerfilas_actualizar(true);
                                    _Desing.SendMessage("Pedido Creado Correctamente", "2");
                                }
                                else
                                {
                                    visible_progress(2);     
                                    _Desing.SendMessage("Error al Crear el Pedido", "1");
                                }                        
                            }                       
                            if (menor > 0)
                            {
                                _Desing.SendMessage("Verificar Existencia de algunos de los artículos", "3");
                            } 
                        }
                        else
                        {
                            MessageBox.Show("Informacion de sucursal incompleta", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }

                    }
                    #endregion                                     

                    #region tipo2   TRASLADOS TODAS
                    if (tipo == 2)
                    {              
                        Dictionary<string, string> par = new Dictionary<string, string>();
                 
                        string CONSULTA = "select  S.CODSUC, S.NOMSUC, S.DESCRIPCION, S.BODEGA, C.Conjunto, C.Sugerido from  INTRADE.dbo.Grl_vwSucursales  as S " +
                                            "inner join Paut_vwSucursal_conjunto as C on S.CODSUC = C.codsuc where C.Conjunto in(" + Clases.Statics.conjunto + ")";

                        DataTable Tbcod = _Sql.ExecuteQuery(CONSULTA);
                   
                        int i = 0;
                        int menor = 0;                   
                        int cont = 1;                                    
                        string nomsuc = "";

                        cargar_progress(Tbcod.Rows.Count);
                        Thread trd = new Thread(hilo);
                        trd.IsBackground = true;
                        trd.Start();

                        foreach (DataRow dt_row in Tbcod.Rows)
                        {                              
                            if (Tbcod.Rows.Count > 0)
                            {
                                par.Clear();
                                par.Add("@bod_des", Tbcod.Rows[i]["BODEGA"].ToString().Trim());
                            }
                            string query = "";
                            string query_deta = "";
                            int contador = 1;
                            string column_sugerido = Tbcod.Rows[i]["sugerido"].ToString().Trim();
                            string suc = Tbcod.Rows[i]["codsuc"].ToString().Trim();

                            DataTable Tb = _Sql.ExecuteQuery("select SIGUIENTE_CONSEC from  ExactusERP6.intrade.CONSECUTIVO_CI where CONSECUTIVO= 'CNSCTRA'");

                            Dictionary<string, string> parm = new Dictionary<string, string>();
                            parm.Add("@paquete", "SI");
                            parm.Add("@COD_SEC", Tb.Rows[0][0].ToString().Trim());
                            parm.Add("@consecutivo", "CNSCTRA");
                            parm.Add("@referencia", "Pedido Automatico General");
                            parm.Add("@seleccionado", "N");
                            parm.Add("@usuario", "APPUSER");
                            parm.Add("@noteExistsflag", "0");

                            query += " UPDATE ExactusERP6.intrade.CONSECUTIVO_CI " +
                                   "SET ULTIMO_USUARIO = @usuario, SIGUIENTE_CONSEC = RIGHT('000000'+ CAST(@COD_SEC+1 AS VARCHAR(6)),6), ULT_FECHA_HORA = GETDATE() " +
                                   "WHERE CONSECUTIVO = @consecutivo ";

                            query += " UPDATE ExactusERP6.intrade.PAQUETE_INVENTARIO SET ULTIMO_USUARIO = @usuario, FECHA_ULT_ACCESO = GETDATE() WHERE PAQUETE_INVENTARIO = @paquete ";

                            query += " INSERT INTO ExactusERP6.intrade.DOCUMENTO_INV " +
                                    "(PAQUETE_INVENTARIO, DOCUMENTO_INV, CONSECUTIVO, REFERENCIA, FECHA_HOR_CREACION, FECHA_DOCUMENTO, SELECCIONADO, USUARIO, NoteExistsFlag) " +
                                    "VALUES(@paquete, @COD_SEC, @consecutivo, @referencia, GETDATE(), cast(GETDATE() as date  ), @seleccionado, @usuario, @noteExistsflag ) ";
                           
                            if (Tb.Rows.Count > 0)
                            {                            
                                foreach (DataGridViewRow gd in Dg.Rows)
                                {
                                    if (Convert.ToBoolean(gd.Cells["Sel"].Value) == true)
                                    {
                                        if (Convert.ToInt32(gd.Cells["existencia"].Value) >= Convert.ToInt32(gd.Cells["CANT_ABASTECER"].Value) && Convert.ToInt32(gd.Cells[column_sugerido].Value) > 0)
                                        {
                                            string soc = (cmb_soc.SelectedValue).ToString();
                                            string art = (gd.Cells["codigo"].Value).ToString().Trim();
                                            int sug = Convert.ToInt32(gd.Cells[column_sugerido].Value);                                     
                                        
                                            query += (string.Format(" INSERT INTO ExactusERP6.intrade.LINEA_DOC_INV " +
                                            "(PAQUETE_INVENTARIO, DOCUMENTO_INV, LINEA_DOC_INV, AJUSTE_CONFIG, ARTICULO, BODEGA, TIPO, SUBTIPO, SUBSUBTIPO, CANTIDAD, " +
                                            " COSTO_TOTAL_LOCAL, COSTO_TOTAL_DOLAR, PRECIO_TOTAL_LOCAL, PRECIO_TOTAL_DOLAR, BODEGA_DESTINO, NoteExistsFlag)" +
                                            "values('SI', '" + Tb.Rows[0][0].ToString().Trim() + "', " + contador + ", '~TT~', '" + gd.Cells["codigo"].Value + "' , '" + cmb_bod.SelectedValue + "', 'T', 'D', '', '" +
                                            gd.Cells[column_sugerido].Value + "'  , 0 , 0 , 0, 0,'" + Tbcod.Rows[i]["BODEGA"].ToString().Trim() + "', '0' )"));

                                            query_deta += string.Format(" EXEC GLOBAL.dbo.Paut_spActualizarExistencia " + 2 + ", '" + soc + "','" + art + "','" + suc + "'," + sug);
                                            cont++;
                                            contador++;                                        
                                        }
                                        else if (Convert.ToInt32(gd.Cells["EXISTENCIA"].Value) < Convert.ToInt32(gd.Cells["CANT_ABASTECER"].Value) || Convert.ToInt32(gd.Cells["EXISTENCIA"].Value) < 1)
                                        {
                                            menor++;
                                        }
                                    }                              
                                    pgb++;                                
                                    progressBar1.Value = pgb;
                                }
                                if (contador > 1)
                                {
                                    DataTable dt1 = _Sql.ExecuteQuery(string.Format("select * from ExactusERP6.intrade.LOCKS where LLAVE='paquete_inventarioSI'"));
                                    if (dt1.Rows.Count == 0)
                                    {
                                        bool st = _Sql.ExecuteNoResult(query, parm);
                                        bool sp = _Sql.ExecuteNoResult(query_deta);
                                        if (st == true && sp == true)
                                        {
                                            nomsuc += Tbcod.Rows[i]["DESCRIPCION"].ToString().Trim() + Environment.NewLine;
                                        }
                                        else
                                        {
                                            _Desing.SendMessage("Error al Crear el Pedido", "1");
                                        }
                                    }
                                    else
                                    {
                                        _Desing.SendMessage("El paquete de inventario SI esta siendo utilizado", "3");
                                        visible_progress(2);
                                    }
                                } 
                            }
                            else
                            {
                                MessageBox.Show("Informacion de sucursal incompleta", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                            i++;
                        }
                        visible_progress(2);
                        if (cont > 1)
                        {
                            removerfilas_actualizar(true);
                            /* Comentareado 26/072016 RRE  */
                            //DataTable dt_email = _Sql.ExecuteQuery(string.Format("EXEC GLOBAL.DBO.Paut_spExistenciasCero 2,'" + cmb_soc.SelectedValue.ToString() + "', '" + cmb_bod.SelectedValue.ToString() + "', '" + Clases.Statics.conjunto + "', '', 0, '', '', ''"));
                            //mail.SendToList("164", "Advertencia de artículos con existencia 0 - PAUT", Clases.Statics.MakeHTML("Los siguientes artículos no pueden ser abastecidos de la bodega '" + cmb_bod.SelectedValue.ToString() + "'", dt_email));
                            _Desing.SendMessage("Pedido Creado Correctamente para las Sucursales: " + "\n" + "\n" + nomsuc, "2");
                        }
                        if (menor > 0)
                        {
                            _Desing.SendMessage("Verificar Existencia de algunos de los artículos", "3");
                        }

                    }
                    #endregion

                    #region tipo3     TRASLADOS por sucursal
                    if (tipo == 3)
                    {
                        Dictionary<string, string> par = new Dictionary<string, string>();
                        par.Add("@SUC", cmb_suc.SelectedValue);
                        par.Add("@bod", cmb_bod.SelectedValue);
                        par.Add("@soc", cmb_soc.SelectedValue);

                        string CONSULTA = "select CODSUC, NOMSUC, DESCRIPCION, BODEGA from  INTRADE.dbo.Grl_vwSucursales where CODSUC=@SUC";

                        DataTable Tbcod = _Sql.ExecuteQuery(CONSULTA, par);

                        if (Tbcod.Rows.Count > 0)
                        {
                            par.Clear();
                            par.Add("@bod_des", Tbcod.Rows[0]["BODEGA"].ToString().Trim());
                        }

                        cargar_progress(Tbcod.Rows.Count);
                        Thread trd = new Thread(hilo);
                        trd.IsBackground = true;
                        trd.Start();

                        DataTable Tb = _Sql.ExecuteQuery("select SIGUIENTE_CONSEC from  ExactusERP6.intrade.CONSECUTIVO_CI where CONSECUTIVO= 'CNSCTRA'", par);

                        DataTable dt = _Sql.ExecuteQuery("select codsuc, nomsuc, conjunto, venta, sugerido from Paut_vwSucursal_conjunto where conjunto='" + cmb_suc.SelectedItem.ToolTipTitle + "' and codsuc='" + cmb_suc.SelectedValue + "'  ");
                        string column_sugerido = dt.Rows[0]["sugerido"].ToString().Trim();
                        string suc = dt.Rows[0]["codsuc"].ToString().Trim();
                   
                        if (Tb.Rows.Count > 0)
                        {
                            string query = "";
                            string query_deta = "";
                            Dictionary<string, string> parm = new Dictionary<string, string>();
                            parm.Add("@paquete", "SI");
                            parm.Add("@COD_SEC", Tb.Rows[0][0].ToString().Trim());                       
                            parm.Add("@consecutivo", "CNSCTRA");
                            parm.Add("@referencia", "Pedido Automatico General");
                            parm.Add("@seleccionado", "N");
                            parm.Add("@usuario", "APPUSER");
                            parm.Add("@noteExistsflag", "0");
                                                                       
                            query += " UPDATE ExactusERP6.intrade.CONSECUTIVO_CI " +
                                   "SET ULTIMO_USUARIO = @usuario, SIGUIENTE_CONSEC = RIGHT('000000'+ CAST(@COD_SEC+1 AS VARCHAR(6)),6), ULT_FECHA_HORA = GETDATE() " +
                                   "WHERE CONSECUTIVO = @consecutivo ";

                            query += " UPDATE ExactusERP6.intrade.PAQUETE_INVENTARIO SET ULTIMO_USUARIO = @usuario, FECHA_ULT_ACCESO = GETDATE() WHERE PAQUETE_INVENTARIO = @paquete ";

                            query += " INSERT INTO ExactusERP6.intrade.DOCUMENTO_INV" +
                                    "(PAQUETE_INVENTARIO, DOCUMENTO_INV, CONSECUTIVO, REFERENCIA, FECHA_HOR_CREACION, FECHA_DOCUMENTO, SELECCIONADO, USUARIO, NoteExistsFlag) " +
                                    "VALUES(@paquete, @COD_SEC, @consecutivo, @referencia, GETDATE(), GETDATE(), @seleccionado, @usuario, @noteExistsflag ) ";

                            int contador = 1;
                            int menor = 0;
                            foreach (DataGridViewRow gd in Dg.Rows)
                            {
                                if (Convert.ToBoolean(gd.Cells["Sel"].Value) == true)
                                {
                                    if (Convert.ToInt32(gd.Cells["existencia"].Value) >= Convert.ToInt32(gd.Cells[column_sugerido].Value))
                                    {
                                        string soc = (cmb_soc.SelectedValue).ToString();
                                        string art =  (gd.Cells["codigo"].Value).ToString().Trim();
                                        int sug = Convert.ToInt32(gd.Cells[column_sugerido].Value);
                                                                 
                                        query += (string.Format(" INSERT INTO ExactusERP6.intrade.LINEA_DOC_INV " +
                                        "(PAQUETE_INVENTARIO, DOCUMENTO_INV, LINEA_DOC_INV, AJUSTE_CONFIG, ARTICULO, BODEGA, TIPO, SUBTIPO, SUBSUBTIPO, CANTIDAD, " +
                                        " COSTO_TOTAL_LOCAL, COSTO_TOTAL_DOLAR, PRECIO_TOTAL_LOCAL, PRECIO_TOTAL_DOLAR, BODEGA_DESTINO, NoteExistsFlag)" +
                                        "values('SI', '" + Tb.Rows[0][0].ToString().Trim() + "', " + contador + ", '~TT~', '" + gd.Cells["codigo"].Value + "' , '" + cmb_bod.SelectedValue + "', 'T', 'D', '', '" +
                                        gd.Cells[column_sugerido].Value + "'  , 0 , 0 , 0, 0,'" + Tbcod.Rows[0]["BODEGA"].ToString().Trim() + "', '0' )"));

                                        query_deta += string.Format(" EXEC GLOBAL.dbo.Paut_spActualizarExistencia "+ 2 +", '"+ soc +"','"+ art + "','" + suc + "'," + sug);
                                   
                                        contador++;
                                    }
                                    if (Convert.ToInt32(gd.Cells["EXISTENCIA"].Value) < Convert.ToInt32(gd.Cells["CANT_ABASTECER"].Value) || Convert.ToInt32(gd.Cells["EXISTENCIA"].Value) < 1)
                                    {
                                        menor++;
                                    }
                                }
                                pgb++;
                                progressBar1.Value = pgb;                           
                            }
                                              
                            if (contador > 1)
                            {
                                DataTable dt1 = _Sql.ExecuteQuery(string.Format("select * from ExactusERP6.intrade.LOCKS where LLAVE='paquete_inventarioSI'"));
                                if (dt1.Rows.Count == 0)
                                {
                                    bool st = _Sql.ExecuteNoResult(query, parm);
                                    bool sp = _Sql.ExecuteNoResult(query_deta);
                                    if (st == true &&  sp == true)
                                    {
                                        visible_progress(2);
                                        removerfilas_actualizar(true);
                                        /* Comentareado 26/072016 RRE  */
                                        //DataTable dt_email = _Sql.ExecuteQuery(string.Format("EXEC GLOBAL.DBO.Paut_spExistenciasCero 2,'" + cmb_soc.SelectedValue.ToString() + "', '" + cmb_bod.SelectedValue.ToString() + "', '" + Clases.Statics.conjunto + "', '', 0, '', '', ''"));
                                        //mail.SendToList("164", "Advertencia de artículos con existencia 0 - PAUT", Clases.Statics.MakeHTML("Los siguientes artículos no pueden ser abastecidos de la bodega '" + cmb_bod.SelectedValue.ToString() + "'", dt_email));
                                        _Desing.SendMessage("Pedido Creado Correctamente", "2");
                                    }
                                    else
                                    {
                                        visible_progress(2);     
                                        _Desing.SendMessage("Error al Crear el Pedido", "1");
                                    }
                                }
                                else
                                {
                                    _Desing.SendMessage("El paquete de inventario SI esta siendo utilizado", "3");
                                    visible_progress(2);     
                                }
                            }
                            if (menor > 0)
                            {
                                _Desing.SendMessage("Verificar Existencia de algunos de los artículos", "3");
                            }                       
                        }
                        else
                        {
                            MessageBox.Show("Informacion de sucursal incompleta", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }

                    }
                    #endregion

                    #region tipo4   PEdidos Todas las suc
                    if (tipo == 4)
                    {
                        Dictionary<string, string> par = new Dictionary<string, string>();                   
                        par.Add("@SOC", cmb_soc.SelectedValue);

                        string CONSULTA = "SELECT CODSUC,NOMSUC,codcli_exactus FROM Paut_VwSucursal WHERE TIPO='VTA' and coddis=@SOC and conjunto in ("+ Clases.Statics.conjunto +") order by conjunto";

                        DataTable Tbcod = _Sql.ExecuteQuery(CONSULTA, par);
                        int i = 0;
                        int menor = 0;
                        int cont = 1;
                                    
                        string nomsuc = "";
                        cargar_progress(Tbcod.Rows.Count);
                        Thread trd = new Thread(hilo);
                        trd.IsBackground = true;
                        trd.Start();

                        foreach (DataRow dt_row in Tbcod.Rows)
                        {
                            string suc = Tbcod.Rows[i]["CODSUC"].ToString().Trim();
                            if (Tbcod.Rows.Count > 0)
                            {
                                par.Clear();
                                par.Add("@cliente", Tbcod.Rows[i]["codcli_exactus"].ToString().Trim());
                                par.Add("@soc", cmb_soc.SelectedItem.Text);
                                string cli = Tbcod.Rows[i]["codcli_exactus"].ToString().Trim();
                            }

                            DataTable Tb = _Sql.ExecuteQuery("SELECT  cliente,condicion_pago,clase_documento FROM  Paut_VwClientes  " +
                            " WHERE cliente =@cliente and sociedad = @soc", par);

                            DataTable dt = _Sql.ExecuteQuery("select nomsuc, conjunto, sugerido, Venta from Paut_vwSucursal_conjunto where conjunto in ("+ Clases.Statics.conjunto +") and codsuc='" + suc + "'  ");
                            string column_sugerido = dt.Rows[0]["sugerido"].ToString().Trim();
                            string conj = dt.Rows[0]["conjunto"].ToString().Trim();                                                       

                            if (Tb.Rows.Count > 0)
                            {
                                string query = "";
                                string query_deta="";

                                Dictionary<string, string> parm = new Dictionary<string, string>();
                                parm.Add("@Cliente", Tbcod.Rows[i]["codcli_exactus"].ToString().Trim());
                                parm.Add("@Vendedor", "9999");
                                parm.Add("@Condicion_Pago", Tb.Rows[0]["condicion_pago"].ToString());
                                parm.Add("@Clase_Documento", Tb.Rows[0]["clase_documento"].ToString());
                                parm.Add("@UsuCrea", Clases.Statics.usuario);
                                parm.Add("@CargadoERP", "N");
                                parm.Add("@codsoc", cmb_soc.SelectedValue);
                                parm.Add("@Nota", "Pedido Automatico General");
                                parm.Add("@Estado", "11"); // aprobado credito
                                parm.Add("@Origen", "PAUG");
                                parm.Add("@Total", "0");
                                parm.Add("@ImprimeCh", "1");
                            
                                query += " INSERT INTO " + cmb_soc.SelectedItem.Tag + ".[dbo].[Mvp_Solicitudes] " +
                                " (Cliente ,Vendedor ,Condicion_Pago ,Clase_Documento ,UsuCrea ,FechaCrea ,CargadoERP ,codsoc " +
                                " ,Nota ,Estado ,Origen ,Total ,ImprimeCh, GrpVta)  VALUES " +
                                " (@Cliente ,@Vendedor ,@Condicion_Pago ,@Clase_Documento ,@UsuCrea ,GETDATE() ,@CargadoERP ,@codsoc " +
                                " ,@Nota ,@Estado ,@Origen ,@Total ,@ImprimeCh, 3) ";

                                query += " DECLARE @CODP AS INT = @@IDENTITY ";
                                int contador = 1;
                           
                                foreach (DataGridViewRow gd in Dg.Rows)
                                {
                                    if (Convert.ToBoolean(gd.Cells["Sel"].Value) == true)
                                    {
                                        if (Convert.ToInt32(gd.Cells[column_sugerido].Value) > 0 && Convert.ToInt32(gd.Cells["EXISTENCIA"].Value) >= Convert.ToInt32(gd.Cells["CANT_ABASTECER"].Value))
                                        {
                                            DataTable dt_top = _Sql.ExecuteQuery("SELECT VALOR from  GLOBAL.dbo.Paut_parametros where TIPO = 'Existencia' and CONJUNTO in (" + Clases.Statics.conjunto + ")");

                                            int sug = Convert.ToInt32(gd.Cells[column_sugerido].Value);
                                            if (sug > Convert.ToInt32(dt_top.Rows[0][0]))
                                            {
                                                sug = Convert.ToInt32(dt_top.Rows[0][0]);
                                            }
                                            string soc = cmb_soc.SelectedValue.ToString();
                                            DataTable Tb2 = _Sql.ExecuteQuery(" select CTRL.articulo, PC.ARTICULO	FROM  GLOBAL.dbo.Paut_TabControl AS CTRL " +
                                            "LEFT JOIN(select articulo,  COD_PROVEEDOR FROM Paut_VwCatalogoArticulos where (sociedad = '" + cmb_soc.SelectedValue + "')" +
                                            "and (CONJUNTO =  " + conj + " )) AS PC ON CTRL.articulo = PC.ARTICULO	" +
                                            "where PC.COD_PROVEEDOR = '" + (gd.Cells["codigo"].Value).ToString().Trim() + "' ");
                                            string art = Tb2.Rows[0][0].ToString().Trim();
                                           
                                            query += string.Format(" INSERT INTO " + cmb_soc.SelectedItem.Tag + ".[dbo].[Mvp_SolicitudDetalle] " +
                                                "(IdSolicitud ,Oferta ,Articulo ,Cantidad ,Descuento ,ItemId  ,Selectivo ) " +
                                                " VALUES (@CODP,'','{0}','{1}',0,'{2}',0) ", gd.Cells["codigo"].Value, sug, contador);

                                            query_deta += string.Format(" EXEC GLOBAL.dbo.Paut_spActualizarExistencia " + 2 + ", '" + soc + "','" + art + "','" + suc + "'," + sug);
                                            cont++;
                                            contador++;      
                                                                              
                                        }
                                        else if (Convert.ToInt32(gd.Cells["EXISTENCIA"].Value) < Convert.ToInt32(gd.Cells["CANT_ABASTECER"].Value) || Convert.ToInt32(gd.Cells["EXISTENCIA"].Value) < 1)
                                        {
                                            menor++;
                                        }
                                    }
                                    pgb++;
                                    progressBar1.Value = pgb;  
                                }
                                if (contador > 1)
                                { 
                                    bool st = _Sql.ExecuteNoResult(query, parm);
                                    bool sp = _Sql.ExecuteNoResult(query_deta);
                                    if (st == true && sp == true)
                                    {  
                                        nomsuc += dt.Rows[0]["nomsuc"].ToString().Trim() + Environment.NewLine;
                                    }
                                    else
                                    {
                                        _Desing.SendMessage("Error al Crear el Pedido", "1");
                                    }
                                }                           
                            }
                            else
                            {
                                MessageBox.Show("Informacion de sucursal incompleta", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }

                            i++;
                        }
                        visible_progress(2);
                        if (cont > 1)
                        {
                            removerfilas_actualizar(true);
                            /* Comentareado 26/072016 RRE  */
                            //DataTable dt_email = _Sql.ExecuteQuery(string.Format("EXEC GLOBAL.DBO.Paut_spExistenciasCero 1,'" + cmb_soc.SelectedValue.ToString() + "', '" + cmb_bod.SelectedValue.ToString() + "', '" + Clases.Statics.conjunto + "', '', 0, '', '', ''"));
                            //mail.SendToList("164", "Advertencia de artículos con existencia 0 - PAUT", Clases.Statics.MakeHTML("Los siguientes artículos no pueden ser abastecidos de la bodega '" + cmb_bod.SelectedValue.ToString() + "'", dt_email));
                            _Desing.SendMessage("Pedido Creado Correctamente para las Sucursales: " + "\n" + "\n" + nomsuc, "2");     
                        }
                        if (menor > 0)
                        {
                            _Desing.SendMessage("Verificar Existencia de algunos de los artículos", "3");
                        }
                   
                    }
                    #endregion            
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }           
            }

        #endregion
        
            private void FrmPaut_Load(object sender, EventArgs e)
            {
                try
                {
                    limpiar_var();
                    cargarSoc();
                    cargarBodega();
                    cargarSuc();
                    LoadPendientes(false);
                    cargar_lbl_actualizacion();
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }

            private void cmb_soc_DropDownItemClicked(object sender, RibbonItemEventArgs e)
            {
                try
                {
                    if (cmb_soc.DropDownItems.Count > 0 && cmb_suc.DropDownItems.Count > 0 && cmb_bod.DropDownItems.Count > 0)
                    {
                        cargarBodega();
                        LoadPendientes(false);
                    }
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }

            private void cmb_suc_DropDownItemClicked(object sender, RibbonItemEventArgs e)
            {
                try
                {
                    if (cmb_soc.DropDownItems.Count > 0 && cmb_suc.DropDownItems.Count > 0 && cmb_bod.DropDownItems.Count > 0)
                    {
                       LoadPendientes(false);
                    }        
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }

            private void btn_limpiar_Click(object sender, EventArgs e)
            {
                try
                {
                    removerfilas(false);
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }

            private void btn_eliminar_Click(object sender, EventArgs e)
            {
                try
                {
                    if (validaArticulos(Dg, "Sel") == false)
                    {
                        MessageBox.Show("Por favor, Seleccione al menos un Articulo", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        if (MessageBox.Show("¿Desea Eliminar Los artículos seleccionados?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            removerfilas(true);
                        }
                    }
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }
                   
            private void btn_incializar_Click(object sender, EventArgs e)
            {
                try
                {
                    if (MessageBox.Show("¿Desea Inicializar el tablero?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        Reinicializar();
                        LoadPendientes(false);
                        _Desing.SendMessage("Proceso realizado con exito", "2");
                    }
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }          
                 
            private void cmb_bod_DropDownItemClicked(object sender, RibbonItemEventArgs e)
            {
                try
                {
                    if (cmb_soc.DropDownItems.Count > 0 && cmb_suc.DropDownItems.Count > 0 && cmb_bod.DropDownItems.Count > 0)
                    {
                        LoadPendientes(false);
                    }                         
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }

            private void btn_pedido_Click(object sender, EventArgs e)
            {
                try 
	            {
                    if (validaArticulos(Dg, "Sel") == false)
                    {
                        MessageBox.Show("Por favor, Seleccione al menos un Artículo", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                    //if (Clases.Statics.sociedad == "I")
                    if (Clases.Statics.sociedad == "T")
                    {
                        if (cmb_soc.SelectedValue == "7")
                            {
                                if (cmb_suc.SelectedValue != "")
                                {
                                    Generar_Pedido_intrade(1);
                                }
                                if (cmb_suc.SelectedValue == "")
                                {
                                  //  Generar_Pedido_intrade(4);  NO SE OCUPA ESTA OPCION, HACIA LA FUNCION DE GENERAR PEDIDO PARA TODAS LAS SUCURSALES
                                }
                            }
                            else if (cmb_soc.SelectedValue == "9")
                            {
                                if (cmb_suc.SelectedValue != "")
                                {
                                // Generar_Pedido_intrade(3); ORIGINAL, CUANDO TBS ERA INTRADE SE HACIAN TRASLADOS
                                Generar_Pedido_intrade(1);
                            }
                                else if (cmb_suc.SelectedValue == "")
                                {
                                   // Generar_Pedido_intrade(2);
                                }
                            }                            
                        }

                        else if (Clases.Statics.sociedad == "VS")
                        {
                            if (cmb_suc.SelectedValue != "")
                            {
                                Generar_Pedido_intrade(1);
                            }
                            else if (cmb_suc.SelectedValue == "")
                            {
                                Generar_Pedido_intrade(4);
                            }
                        }


                    }

                    visible_progress(2);
	            }              
                catch (Exception Ex)
                {
                    visible_progress(2);
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }         

            private void chtodos_CheckedChanged(object sender, EventArgs e)
            {
                CheckBox ch = sender as CheckBox;
                int fila = 0;
              
                if (chtodos.Checked == true)
                {                    
                    foreach (DataGridViewRow row in Dg.Rows) //para despacho
                    {

                        if (Convert.ToInt32(row.Cells["EXISTENCIA"].Value) >= Convert.ToInt32(row.Cells["CANT_ABASTECER"].Value))
                        {
                            row.Cells["Sel"].Value = ch.Checked;
                        }
                        else if (Convert.ToInt32(row.Cells["EXISTENCIA"].Value) > 0 && Convert.ToInt32(row.Cells["CANT_ABASTECER"].Value) > Convert.ToInt32(row.Cells["EXISTENCIA"].Value))
	                    {
                            Dg.Rows[fila].DefaultCellStyle.BackColor = System.Drawing.Color.Yellow;		                  
	                    }
                        else if (Convert.ToInt32(row.Cells["EXISTENCIA"].Value) == 0)
                        {
                            Dg.Rows[fila].DefaultCellStyle.BackColor = System.Drawing.Color.OrangeRed;
                        }
                        fila++;
                    } 
                }
                else
                {
                    foreach (DataGridViewRow row in Dg.Rows) //para despacho
                    {
                        row.Cells["Sel"].Value = false;    
                    } 
                }
              
            }                   
                 
            private void Dg_CurrentCellDirtyStateChanged(object sender, EventArgs e)
            {
                if (Dg.CurrentCell != null)
                {
                    if (Dg.Columns[Dg.CurrentCell.ColumnIndex].Name == "Sel")
                    {
                        if (Dg.IsCurrentCellDirty)
                        {
                            Dg.CommitEdit(DataGridViewDataErrorContexts.CurrentCellChange);
                        }
                    }
                }
            }

            private void Dg_CellClick(object sender, DataGridViewCellEventArgs e)
            {
                try
                {
                    int a = Dg.Columns["Sel"].Index;

                    if (e.ColumnIndex == a)
                    {
                        if (Convert.ToDecimal(Dg.CurrentRow.Cells["Existencia"].Value) >= Convert.ToDecimal(Dg.CurrentRow.Cells["CANT_ABASTECER"].Value))
                        {
                            Dg.CurrentRow.Cells["Sel"].Value = true;
                        }
                        else
                        {
                            MessageBox.Show("Las Existencias son menores a la Venta a Reabastecer", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            Dg.CurrentRow.Cells["Sel"].Value = false;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }

            private void FrmPaut_FormClosed(object sender, FormClosedEventArgs e)
            {
                control_pantalla(Clases.Statics.pantalla_paut);
            }

            private void chb_existencias_CheckBoxCheckChanged(object sender, EventArgs e)
            {
                LoadPendientes(false); 
            }

            private void btn_actualizar__Click(object sender, EventArgs e)
            {
                actualiza_existencia();
                cambiar_color_fila();  
            }

            private void txt_articulo_TextBoxTextChanged(object sender, EventArgs e)
            {
                LoadPendientes(false); 
            }

            private void FrmPaut_Resize(object sender, EventArgs e)
            {                
                progressBar1.Left = (this.Width - progressBar1.Width) / 2;
                progressBar1.Top = (this.Height - progressBar1.Height) / 2;
                lbl_procesando.Left = (this.Width -  lbl_procesando.Width) / 2;
                lbl_procesando.Top = ((this.Height - (lbl_procesando.Height * (-1) - progressBar1.Height)) / 2);
                lblconteo.Left = (this.Width - (lblconteo.Width * 3/2 ));
                Dg.Width = this.Width - 50;
                Dg.Height = this.Height - 220;      
            }

            private void Dg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
            {
                try
                {
                    Clases.Statics.cod_proveedor = Dg.CurrentRow.Cells["codigo"].Value.ToString().Trim();
                    Clases.Statics.sucursal = cmb_suc.SelectedValue;
                    Clases.Statics.cod_dis = cmb_soc.SelectedValue.ToString();
                    Clases.Statics.bodega = cmb_bod.SelectedValue.ToString();

                    AP.ShowDialog();

                    if (Clases.Statics.i == 1)
                    {
                        Clases.Statics.i = 0;
                    }
                    LoadPendientes(false);
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }

            private void Dg_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
            {
                cambiar_color_fila();  
            }

            private void btn_filtrar_Click(object sender, EventArgs e)
            {             
                try
                {                   
                    f.ShowDialog();
                      
                    LoadPendientes(false);
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }

            private void btn_QuitarF_Click(object sender, EventArgs e)
            {
                try
                {
                    limpiar_var();
                    LoadPendientes(false);
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }

            private void rbbReporte_Click(object sender, EventArgs e)
            {
                try
                {
                    Reporte_filtrar();

                    if (TbPendientes.Rows.Count > 0)
	                {
		                FrmReporte frm = new FrmReporte(TbPendientes);
                        frm.cod_sucursal = cmb_suc.SelectedValue.ToString();
                        frm.ShowDialog();
                        frm.Dispose();       
	                }
                    else
	                {
                        _Desing.SendMessage("No existen datos que mostrar", "3");
	                }
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }
                 
         
           

           
    }
}
