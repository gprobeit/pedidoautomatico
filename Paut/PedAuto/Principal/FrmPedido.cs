﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

namespace PedAuto.Formularios
{
    public partial class FrmPedido : PedAuto.Formularios.FrmBase
    {
        public FrmPedido()
        {
            InitializeComponent();
        }

        DataTable TbCatalogo;
        string GRP = "";
        string bodega = "";
    


        private void FrmPedido_Load(object sender, EventArgs e)
        {

            try
            {

                Cantidad.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                Existencia.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;


                string query = " SELECT codsuc,nomsuc from Pa_VwSucursal ";
                DataTable TbSuc = _Sql.ExecuteQuery(query);

                foreach (DataRow item in TbSuc.Rows)
                {
                    RibbonButton It = new RibbonButton();
                    It.Value = item["codsuc"].ToString();
                    It.Text = item["nomsuc"].ToString();
                    cmbsucursal.DropDownItems.Add(It);
                }

                cmbsucursal.SelectedValue = Clases.Statics.sucursal;
                ribbon1.Refresh();


                Dg.Width = Screen.PrimaryScreen.WorkingArea.Width - 25;
                Dg.Height = Screen.PrimaryScreen.WorkingArea.Height - 295;

                Articulo.Width = Convert.ToInt32(Dg.Width * 0.20);
                Descripcion.Width = Convert.ToInt32(Dg.Width * 0.40);
                Cantidad.Width = Convert.ToInt32(Dg.Width * 0.15);
                Existencia.Width = Convert.ToInt32(Dg.Width * 0.15);
                El.Width = Convert.ToInt32(Dg.Width * 0.09);

            }
            catch (Exception Ex)
            {

                _Desing.SendMessage(Ex.Message, "1");
            }

        }


        private void cargaArticulos()
        {
            Dictionary<string, string> paramts = new Dictionary<string, string>();
            paramts["@EMPRESA"] = Clases.Statics.owner;
            TbCatalogo = _Sql.ExecuteQuery(string.Format(" SELECT ARTICULO,DESCRIPCION,EXISTENCIA FROM Pa_VwArticulo WHERE {0}='S' AND EMPRESA=@EMPRESA ", GRP), paramts);

        }


        private void btnBusqueda_Click(object sender, EventArgs e)
        {
            try
            {
                if (GRP != "")
                {
                    cargaArticulos();
                    FrmBusqueda FrmB = new FrmBusqueda(1, TbCatalogo);
                    FrmB.ShowDialog();
                    TxtArticulo.Text = FrmB.Codigo;
                    TxtArticulo_Validated(null, null);
                }
                else { _Desing.SendMessage("Por favor, Elija un Tipo de Pedido", "3"); }
            }
            catch (Exception Ex)
            {

                _Desing.SendMessage(Ex.Message, "1");
            }

        }

        private void TxtArticulo_Validated(object sender, EventArgs e)
        {
            try
            {

                if (TxtArticulo.Text.Trim() != "")
                {
                    DataRow[] Rw = TbCatalogo.Select(string.Format("ARTICULO='{0}'", TxtArticulo.Text.Trim()));
                    if (Rw.Length > 0)
                    {
                        if (Convert.ToInt32(Rw[0]["EXISTENCIA"].ToString().Trim()) > 0)
                        {
                            TxtArticulo.Text = Rw[0]["ARTICULO"].ToString().Trim();
                            TxtDescrip.Text = Rw[0]["DESCRIPCION"].ToString().Trim();
                            TxtCantidad.Tag = Rw[0]["EXISTENCIA"].ToString().Trim();
                            TxtCantidad.Focus();
                        }
                        else
                        {

                            _Desing.SendMessage("La Existencia de este Articulo es Cero", "3");
                            limpiarDetalle();
                        }
                    }
                    else
                    {
                        limpiarDetalle();
                    }
                }
                else
                {
                    limpiarDetalle();
                }

            }
            catch (Exception Ex)
            {

                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void limpiarDetalle()
        {
            TxtArticulo.Clear();
            TxtDescrip.Clear();
            TxtCantidad.Clear();
            TxtCantidad.Tag = "";

        }

        private void TxtCantidad_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (Dg.Rows.Count < 14)
                    {
                        if (TxtArticulo.Text.Trim() != "" && TxtDescrip.Text.Trim() != "")
                        {
                            int i = 0;
                            bool result = int.TryParse(TxtCantidad.Text.Trim(), out i);
                            if (result == true && i > 0)
                            {
                                Clases.Statics st = new Clases.Statics();
                                bool entontrado = st.BuscarLINQ(TxtArticulo.Text.Trim(), "Articulo", Dg);

                                if (entontrado == false)
                                {
                                    int row = Dg.Rows.Add(TxtArticulo.Text.Trim(), TxtDescrip.Text.Trim(), TxtCantidad.Text.Trim(), TxtCantidad.Tag);
                                    if (Convert.ToInt32(TxtCantidad.Tag) >= Convert.ToInt32(TxtCantidad.Text.Trim()))
                                    {
                                        Dg.Rows[row].DefaultCellStyle.ForeColor = Color.Black;
                                    }
                                    else
                                    {
                                        Dg.Rows[row].DefaultCellStyle.ForeColor = Color.Red;
                                    }

                                    limpiarDetalle();
                                }
                                else
                                {
                                    _Desing.SendMessage("Articulo ya fue Seleccionado", "3");
                                    limpiarDetalle();

                                }
                            }
                            else
                            {
                                _Desing.SendMessage("Por favor, ingrese una Cantidad Correcta", "3");
                            }
                        }
                        else
                        {
                            _Desing.SendMessage("Por favor, elija un Articulo", "3");
                        }
                    }
                    else
                    {

                        _Desing.SendMessage("Se ha llegado al límite de las líneas en el detalle de pedido", "3");
                    }



                }// si presiono enter

            }
            catch (Exception Ex)
            {

                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void Dg_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                if (e.RowIndex >= 0)
                {
                    if (Convert.ToInt32(Dg.Rows[e.RowIndex].Cells["Existencia"].Value) >= Convert.ToInt32(Dg.Rows[e.RowIndex].Cells["Cantidad"].Value))
                    {
                        Dg.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Black;
                    }
                    else
                    {
                        Dg.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;

                    }
                }
            }
            catch (Exception Ex)
            {

                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void Dg_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && Dg.Columns[e.ColumnIndex].Name.ToString() == "El")
            {
                Dg.Rows.RemoveAt(e.RowIndex);
            }
        }

        private void BtnActualiza_Click(object sender, EventArgs e)
        {
            try
            {
                cargaArticulos();

                foreach (DataGridViewRow Rw in Dg.Rows)
                {
                    DataRow[] Ra = TbCatalogo.Select(string.Format("Articulo='{0}'", Rw.Cells["Articulo"].Value.ToString().Trim()));
                    if (Ra.Length > 0)
                    {
                        Rw.Cells["Existencia"].Value = Ra[0]["Existencia"];
                    }
                }
            }
            catch (Exception Ex)
            {

                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void rb_CheckBoxCheckChanged(object sender, EventArgs e)
        {
            RibbonCheckBox Ch = sender as RibbonCheckBox;      
           
            
            if (Ch.Checked)
            {
                GRP = Ch.Value;
                Dictionary<string, string> par = new Dictionary<string, string>();
                par.Add("@sucursal", Clases.Statics.sucursal);
                par.Add("@tipo", GRP);
                par.Add("@empresa", Clases.Statics.empresa);
                DataTable Tb = _Sql.ExecuteQuery(" select codcli_exactus  FROM Pa_SucClienteERP where codsuc=@sucursal and tipo=@tipo and empresa=@empresa ", par);

                if (Tb.Rows.Count>0)
                {
                    Clases.Statics.cliente = Tb.Rows[0]["codcli_exactus"].ToString().Trim();
                }

                par.Remove("@sucursal");
                DataTable TbBod = _Sql.ExecuteQuery(" SELECT bodega,principal  FROM Pa_BodegaTipo where empresa=@empresa and tipo=@tipo ", par);
                if (  TbBod.Rows.Count == 1)
                {
                    bodega = TbBod.Rows[0]["bodega"].ToString().Trim();
                }
                else {
                    if (TbBod.Rows.Count > 0)
                    {
                        DataRow[] rw = TbBod.Select("principal=1");
                        if (rw.Length >0)
                        {
                           bodega = rw[0]["bodega"].ToString().Trim();
                        }
                    }                
                }

               
                lbltipo.Text = "Pedido de " + Ch.Text;
            }
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            groupBox1.Enabled = true;
            TxtPedido.Text = "18585858585858585";

            ribbon1.Refresh();
            Dg.Enabled = true;
        }

        private void rbprd_CheckBoxCheckChanging(object sender, CancelEventArgs e)
        {
            if (Dg.Rows.Count > 0)
            {
                RibbonCheckBox Ch = sender as RibbonCheckBox;

                if ((Ch.Checked) && (Ch.Value != GRP))
                {
                    DialogResult rs = MessageBox.Show("Si cambia perdera los registros", "Pregunta", MessageBoxButtons.YesNo);
                    if (rs == System.Windows.Forms.DialogResult.Yes)
                        Dg.Rows.Clear();
                    else
                        e.Cancel = true;
                }

            }
         }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
            if (Dg.Rows.Count > 0)
            {        
                   string sql = " DECLARE @Pedido int ";

                   if (TxtPedido.Text == "#")
                   {

                       string num = "";
                       DataTable Tb = _Sql.ExecuteQuery("SELECT ISNULL(MAX(PEGRAL),0) + 1 num FROM [INTRADE].[dbo].[Dva_Pedido]");

                       if (Tb.Rows.Count > 0)
                       {
                           num = Tb.Rows[0][0].ToString().Trim();
                       }


                       Dictionary<string, string> param = new Dictionary<string, string>();

                       param.Add("@USUCREA", Clases.Statics.usuario);
                       param.Add("@SUCURSAL", Clases.Statics.sucursal);
                       param.Add("@CODCLIENTE", Clases.Statics.cliente);
                       param.Add("@TIPOINV", GRP);
                       param.Add("@TOTAL", "0");
                       param.Add("@BODEGA", bodega);
                       param.Add("@ESTADO", "0");
                       param.Add("@PEGRAL", num);
                       param.Add("@EMPRESA", Clases.Statics.empresa);


                       sql += " INSERT INTO [INTRADE].[dbo].[Dva_Pedido] " +
                       " (FECHACREA,USUCREA,SUCURSAL,CODCLIENTE,TIPOINV,TOTAL,BODEGA,ESTADO,PEGRAL,EMPRESA) " +
                       " VALUES (GETDATE(),@USUCREA,@SUCURSAL,@CODCLIENTE,@TIPOINV,@TOTAL,@BODEGA,@ESTADO,@PEGRAL,@EMPRESA) ";

                       sql += " SET @Pedido = @@IDENTITY ";


                   }
                   else
                   {

                       sql += " SET @Pedido =  " + TxtPedido .Text .Trim ();
                       sql += " DELETE FROM [INTRADE].[dbo].[Dva_Pedido_Detalle] WHERE PEDIDO= @Pedido ";
                   
                   }
                
                
                   foreach (DataGridViewRow Fg in Dg.Rows)
                   {
                       sql += string.Format(" INSERT INTO [INTRADE].[dbo].[Dva_Pedido_Detalle] (ARTICULO ,CANTIDAD,PEDIDO) " +
                              " VALUES ('{0}','{1}',@Pedido) ", Fg.Cells["Articulo"].Value.ToString().Trim(), Fg.Cells["Cantidad"].Value.ToString().Trim());
                   }

               //} 

            }
            else 
            { _Desing.SendMessage("Por favor, Ingrese detalle de Articulos","2"); }
            
            }
            catch (Exception Ex)
            {

                _Desing.SendMessage(Ex.Message,"1");
            }
        }





    }//FIN CLASE
}
