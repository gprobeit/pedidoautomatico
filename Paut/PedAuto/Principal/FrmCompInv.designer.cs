﻿namespace PedAuto.Formularios
{
    partial class FrmCompInv
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCompInv));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            this.rbb_opciones = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel1 = new System.Windows.Forms.RibbonPanel();
            this.ButMostrar = new System.Windows.Forms.RibbonButton();
            this.ButQuitar = new System.Windows.Forms.RibbonButton();
            this.chExistencia = new System.Windows.Forms.RibbonCheckBox();
            this.ribbonPanel2 = new System.Windows.Forms.RibbonPanel();
            this.TxtDesde = new System.Windows.Forms.RibbonTextBox();
            this.TxtHasta = new System.Windows.Forms.RibbonTextBox();
            this.panelTipoInv = new System.Windows.Forms.RibbonPanel();
            this.RbPro = new System.Windows.Forms.RibbonCheckBox();
            this.RbVe = new System.Windows.Forms.RibbonCheckBox();
            this.ChMen = new System.Windows.Forms.RibbonCheckBox();
            this.ChSem = new System.Windows.Forms.RibbonCheckBox();
            this.ribbonPanel4 = new System.Windows.Forms.RibbonPanel();
            this.ribbonLabel2 = new System.Windows.Forms.RibbonLabel();
            this.TxtCodArt = new System.Windows.Forms.RibbonTextBox();
            this.ribbonLabel3 = new System.Windows.Forms.RibbonLabel();
            this.TxtDesc = new System.Windows.Forms.RibbonTextBox();
            this.ribbonPanel5 = new System.Windows.Forms.RibbonPanel();
            this.TxtCla1 = new System.Windows.Forms.RibbonTextBox();
            this.TxtCla2 = new System.Windows.Forms.RibbonTextBox();
            this.TxtCla3 = new System.Windows.Forms.RibbonTextBox();
            this.TxtCla4 = new System.Windows.Forms.RibbonTextBox();
            this.TxtCla5 = new System.Windows.Forms.RibbonTextBox();
            this.TxtCla6 = new System.Windows.Forms.RibbonTextBox();
            this.lblaviso = new System.Windows.Forms.Label();
            this.ribbon1 = new System.Windows.Forms.Ribbon();
            this.lblArticulo = new System.Windows.Forms.Label();
            this.Dgcomp = new System.Windows.Forms.DataGridView();
            this.DgArt = new System.Windows.Forms.DataGridView();
            this.ARTICULO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VTA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmb_distribuidora = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_total = new System.Windows.Forms.Label();
            this.lbl_detalle = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Dgcomp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgArt)).BeginInit();
            this.SuspendLayout();
            // 
            // rbb_opciones
            // 
            this.rbb_opciones.Panels.Add(this.ribbonPanel1);
            this.rbb_opciones.Panels.Add(this.ribbonPanel2);
            this.rbb_opciones.Panels.Add(this.panelTipoInv);
            this.rbb_opciones.Panels.Add(this.ribbonPanel4);
            this.rbb_opciones.Panels.Add(this.ribbonPanel5);
            this.rbb_opciones.Text = "Opciones";
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.Items.Add(this.ButMostrar);
            this.ribbonPanel1.Items.Add(this.ButQuitar);
            this.ribbonPanel1.Items.Add(this.chExistencia);
            this.ribbonPanel1.Text = "Acciones";
            // 
            // ButMostrar
            // 
            this.ButMostrar.Image = global::PedAuto.Properties.Resources.search;
            this.ButMostrar.SmallImage = ((System.Drawing.Image)(resources.GetObject("ButMostrar.SmallImage")));
            this.ButMostrar.Text = "Mostrar Datos";
            this.ButMostrar.Click += new System.EventHandler(this.ButMostrar_Click);
            // 
            // ButQuitar
            // 
            this.ButQuitar.Image = global::PedAuto.Properties.Resources.lente_de_zoom_de_busqueda_icono_7035_32;
            this.ButQuitar.SmallImage = ((System.Drawing.Image)(resources.GetObject("ButQuitar.SmallImage")));
            this.ButQuitar.Text = "Quitar Filtro";
            this.ButQuitar.Click += new System.EventHandler(this.ButQuitar_Click);
            // 
            // chExistencia
            // 
            this.chExistencia.CheckBoxOrientation = System.Windows.Forms.RibbonCheckBox.CheckBoxOrientationEnum.Right;
            this.chExistencia.Text = "Existencia";
            this.chExistencia.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.Items.Add(this.TxtDesde);
            this.ribbonPanel2.Items.Add(this.TxtHasta);
            this.ribbonPanel2.Text = "Periodo";
            // 
            // TxtDesde
            // 
            this.TxtDesde.Text = "";
            this.TxtDesde.TextBoxText = "";
            this.TxtDesde.Value = "";
            this.TxtDesde.TextBoxKeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtDesde_TextBoxKeyPress);
            this.TxtDesde.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TxtDesde_Click);
            // 
            // TxtHasta
            // 
            this.TxtHasta.TextBoxText = "";
            this.TxtHasta.TextBoxKeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtHasta_TextBoxKeyPress);
            this.TxtHasta.Click += new System.EventHandler(this.TxtHasta_Click);
            this.TxtHasta.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TxtHasta_MouseUp);
            // 
            // panelTipoInv
            // 
            this.panelTipoInv.Items.Add(this.RbPro);
            this.panelTipoInv.Items.Add(this.RbVe);
            this.panelTipoInv.Items.Add(this.ChMen);
            this.panelTipoInv.Items.Add(this.ChSem);
            this.panelTipoInv.Text = "Tipo Inventario";
            // 
            // RbPro
            // 
            this.RbPro.Checked = true;
            this.RbPro.Style = System.Windows.Forms.RibbonCheckBox.CheckBoxStyle.RadioButton;
            this.RbPro.Text = "Produccion";
            this.RbPro.CheckBoxCheckChanged += new System.EventHandler(this.RbPro_CheckBoxCheckChanged);
            // 
            // RbVe
            // 
            this.RbVe.Style = System.Windows.Forms.RibbonCheckBox.CheckBoxStyle.RadioButton;
            this.RbVe.Text = "Venta";
            this.RbVe.CheckBoxCheckChanged += new System.EventHandler(this.RbVe_CheckBoxCheckChanged);
            // 
            // ChMen
            // 
            this.ChMen.Checked = true;
            this.ChMen.Text = "Mensual";
            this.ChMen.Visible = false;
            this.ChMen.CheckBoxCheckChanged += new System.EventHandler(this.ChMen_CheckBoxCheckChanged);
            // 
            // ChSem
            // 
            this.ChSem.Text = "Semanal";
            this.ChSem.Visible = false;
            this.ChSem.CheckBoxCheckChanged += new System.EventHandler(this.ChSem_CheckBoxCheckChanged);
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.Items.Add(this.ribbonLabel2);
            this.ribbonPanel4.Items.Add(this.TxtCodArt);
            this.ribbonPanel4.Items.Add(this.ribbonLabel3);
            this.ribbonPanel4.Items.Add(this.TxtDesc);
            this.ribbonPanel4.Text = "Articulo";
            // 
            // ribbonLabel2
            // 
            this.ribbonLabel2.Text = "Codigo";
            // 
            // TxtCodArt
            // 
            this.TxtCodArt.TextBoxText = "";
            // 
            // ribbonLabel3
            // 
            this.ribbonLabel3.Text = "Descripcion";
            // 
            // TxtDesc
            // 
            this.TxtDesc.TextBoxText = "";
            this.TxtDesc.TextBoxWidth = 200;
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.Items.Add(this.TxtCla1);
            this.ribbonPanel5.Items.Add(this.TxtCla2);
            this.ribbonPanel5.Items.Add(this.TxtCla3);
            this.ribbonPanel5.Items.Add(this.TxtCla4);
            this.ribbonPanel5.Items.Add(this.TxtCla5);
            this.ribbonPanel5.Items.Add(this.TxtCla6);
            this.ribbonPanel5.Text = "Clasificacion";
            // 
            // TxtCla1
            // 
            this.TxtCla1.Text = "Clasi 1";
            this.TxtCla1.TextBoxText = "";
            this.TxtCla1.DoubleClick += new System.EventHandler(this.TxtCla1_DoubleClick);
            // 
            // TxtCla2
            // 
            this.TxtCla2.Text = "Clasi 2";
            this.TxtCla2.TextBoxText = "";
            this.TxtCla2.DoubleClick += new System.EventHandler(this.TxtCla2_DoubleClick);
            // 
            // TxtCla3
            // 
            this.TxtCla3.Text = "Clasi 3";
            this.TxtCla3.TextBoxText = "";
            this.TxtCla3.DoubleClick += new System.EventHandler(this.TxtCla3_DoubleClick);
            // 
            // TxtCla4
            // 
            this.TxtCla4.Text = "Clasi 4";
            this.TxtCla4.TextBoxText = "";
            this.TxtCla4.DoubleClick += new System.EventHandler(this.TxtCla4_DoubleClick);
            // 
            // TxtCla5
            // 
            this.TxtCla5.Text = "Clasi 5";
            this.TxtCla5.TextBoxText = "";
            this.TxtCla5.DoubleClick += new System.EventHandler(this.TxtCla5_DoubleClick);
            // 
            // TxtCla6
            // 
            this.TxtCla6.Text = "Clasi 6";
            this.TxtCla6.TextBoxText = "";
            this.TxtCla6.DoubleClick += new System.EventHandler(this.TxtCla6_DoubleClick);
            // 
            // lblaviso
            // 
            this.lblaviso.AutoSize = true;
            this.lblaviso.BackColor = System.Drawing.Color.LightSkyBlue;
            this.lblaviso.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblaviso.ForeColor = System.Drawing.Color.Black;
            this.lblaviso.Location = new System.Drawing.Point(185, 5);
            this.lblaviso.Name = "lblaviso";
            this.lblaviso.Size = new System.Drawing.Size(51, 20);
            this.lblaviso.TabIndex = 24;
            this.lblaviso.Text = "label1";
            this.lblaviso.Visible = false;
            // 
            // ribbon1
            // 
            this.ribbon1.BackColor = System.Drawing.Color.Azure;
            this.ribbon1.CaptionBarVisible = false;
            this.ribbon1.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold);
            this.ribbon1.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.Minimized = false;
            this.ribbon1.Name = "ribbon1";
            // 
            // 
            // 
            this.ribbon1.OrbDropDown.BorderRoundness = 8;
            this.ribbon1.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.OrbDropDown.Name = "";
            this.ribbon1.OrbDropDown.Size = new System.Drawing.Size(527, 72);
            this.ribbon1.OrbDropDown.TabIndex = 0;
            this.ribbon1.OrbImage = null;
            this.ribbon1.OrbVisible = false;
            // 
            // 
            // 
            this.ribbon1.QuickAcessToolbar.Visible = false;
            this.ribbon1.Size = new System.Drawing.Size(1119, 155);
            this.ribbon1.TabIndex = 22;
            this.ribbon1.Tabs.Add(this.rbb_opciones);
            this.ribbon1.TabsMargin = new System.Windows.Forms.Padding(12, 2, 25, 0);
            this.ribbon1.TabSpacing = 10;
            // 
            // lblArticulo
            // 
            this.lblArticulo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblArticulo.AutoSize = true;
            this.lblArticulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArticulo.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblArticulo.Location = new System.Drawing.Point(616, 164);
            this.lblArticulo.Name = "lblArticulo";
            this.lblArticulo.Size = new System.Drawing.Size(135, 13);
            this.lblArticulo.TabIndex = 20;
            this.lblArticulo.Text = "Articulo Seleccionado:";
            // 
            // Dgcomp
            // 
            this.Dgcomp.AllowUserToAddRows = false;
            this.Dgcomp.AllowUserToDeleteRows = false;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
            this.Dgcomp.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle19;
            this.Dgcomp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Dgcomp.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Dgcomp.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.Lavender;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Dgcomp.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.Dgcomp.ColumnHeadersHeight = 25;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Dgcomp.DefaultCellStyle = dataGridViewCellStyle21;
            this.Dgcomp.EnableHeadersVisualStyles = false;
            this.Dgcomp.Location = new System.Drawing.Point(503, 188);
            this.Dgcomp.Name = "Dgcomp";
            this.Dgcomp.ReadOnly = true;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Dgcomp.RowHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.Dgcomp.RowHeadersVisible = false;
            this.Dgcomp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dgcomp.Size = new System.Drawing.Size(604, 265);
            this.Dgcomp.TabIndex = 6;
            // 
            // DgArt
            // 
            this.DgArt.AllowUserToAddRows = false;
            this.DgArt.AllowUserToDeleteRows = false;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
            this.DgArt.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle23;
            this.DgArt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.DgArt.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.Lavender;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgArt.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.DgArt.ColumnHeadersHeight = 25;
            this.DgArt.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ARTICULO,
            this.Column2,
            this.Column3,
            this.PRD,
            this.VTA});
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgArt.DefaultCellStyle = dataGridViewCellStyle26;
            this.DgArt.EnableHeadersVisualStyles = false;
            this.DgArt.Location = new System.Drawing.Point(12, 188);
            this.DgArt.Name = "DgArt";
            this.DgArt.ReadOnly = true;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgArt.RowHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.DgArt.RowHeadersVisible = false;
            this.DgArt.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgArt.Size = new System.Drawing.Size(468, 265);
            this.DgArt.TabIndex = 0;
            this.DgArt.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgArt_CellClick);
            this.DgArt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DgArt_KeyDown);
            // 
            // ARTICULO
            // 
            this.ARTICULO.DataPropertyName = "ARTICULO";
            this.ARTICULO.FillWeight = 9.87366F;
            this.ARTICULO.HeaderText = "ARTICULO";
            this.ARTICULO.Name = "ARTICULO";
            this.ARTICULO.ReadOnly = true;
            this.ARTICULO.Width = 125;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "DESCRIPCION";
            this.Column2.FillWeight = 284.7716F;
            this.Column2.HeaderText = "DESCRIPCION";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 250;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "INVENTARIO";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle25.Format = "N0";
            dataGridViewCellStyle25.NullValue = null;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle25;
            this.Column3.FillWeight = 5.354764F;
            this.Column3.HeaderText = "INVENTARIO";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 50;
            // 
            // PRD
            // 
            this.PRD.DataPropertyName = "PRD";
            this.PRD.HeaderText = "PRD";
            this.PRD.Name = "PRD";
            this.PRD.ReadOnly = true;
            this.PRD.Visible = false;
            // 
            // VTA
            // 
            this.VTA.DataPropertyName = "VTA";
            this.VTA.HeaderText = "VTA";
            this.VTA.Name = "VTA";
            this.VTA.ReadOnly = true;
            this.VTA.Visible = false;
            // 
            // cmb_distribuidora
            // 
            this.cmb_distribuidora.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_distribuidora.Enabled = false;
            this.cmb_distribuidora.FormattingEnabled = true;
            this.cmb_distribuidora.Items.AddRange(new object[] {
            "Disprobe",
            "Intrade"});
            this.cmb_distribuidora.Location = new System.Drawing.Point(281, 161);
            this.cmb_distribuidora.Name = "cmb_distribuidora";
            this.cmb_distribuidora.Size = new System.Drawing.Size(170, 21);
            this.cmb_distribuidora.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(213, 164);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "Distribuidora:";
            // 
            // lbl_total
            // 
            this.lbl_total.AutoSize = true;
            this.lbl_total.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total.Location = new System.Drawing.Point(12, 164);
            this.lbl_total.Name = "lbl_total";
            this.lbl_total.Size = new System.Drawing.Size(145, 13);
            this.lbl_total.TabIndex = 27;
            this.lbl_total.Text = "Cantidad de Artículos: 0";
            // 
            // lbl_detalle
            // 
            this.lbl_detalle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_detalle.AutoSize = true;
            this.lbl_detalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_detalle.Location = new System.Drawing.Point(958, 164);
            this.lbl_detalle.Name = "lbl_detalle";
            this.lbl_detalle.Size = new System.Drawing.Size(125, 13);
            this.lbl_detalle.TabIndex = 28;
            this.lbl_detalle.Text = "Cantidad de Salas: 0";
            // 
            // FrmCompInv
            // 
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1119, 468);
            this.Controls.Add(this.lbl_detalle);
            this.Controls.Add(this.lbl_total);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmb_distribuidora);
            this.Controls.Add(this.lblaviso);
            this.Controls.Add(this.ribbon1);
            this.Controls.Add(this.lblArticulo);
            this.Controls.Add(this.Dgcomp);
            this.Controls.Add(this.DgArt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCompInv";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Inventario";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmCompInv_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Dgcomp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgArt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DgArt;
        private System.Windows.Forms.DataGridView Dgcomp;
        private System.Windows.Forms.Label lblArticulo;
        private System.Windows.Forms.RibbonTab rbb_opciones;
        private System.Windows.Forms.RibbonPanel ribbonPanel1;
        private System.Windows.Forms.RibbonPanel ribbonPanel2;
        private System.Windows.Forms.RibbonTextBox TxtDesde;
        private System.Windows.Forms.RibbonTextBox TxtHasta;
        private System.Windows.Forms.RibbonPanel panelTipoInv;
        private System.Windows.Forms.RibbonCheckBox RbPro;
        private System.Windows.Forms.RibbonCheckBox RbVe;
        private System.Windows.Forms.RibbonCheckBox ChMen;
        private System.Windows.Forms.RibbonCheckBox ChSem;
        private System.Windows.Forms.RibbonPanel ribbonPanel4;
        private System.Windows.Forms.RibbonLabel ribbonLabel2;
        private System.Windows.Forms.RibbonTextBox TxtCodArt;
        private System.Windows.Forms.RibbonLabel ribbonLabel3;
        private System.Windows.Forms.RibbonTextBox TxtDesc;
        private System.Windows.Forms.RibbonPanel ribbonPanel5;
        private System.Windows.Forms.RibbonButton ButMostrar;
        private System.Windows.Forms.RibbonButton ButQuitar;
        private System.Windows.Forms.RibbonCheckBox chExistencia;
        private System.Windows.Forms.Ribbon ribbon1;
        private System.Windows.Forms.Label lblaviso;
        private System.Windows.Forms.RibbonTextBox TxtCla1;
        private System.Windows.Forms.RibbonTextBox TxtCla2;
        private System.Windows.Forms.RibbonTextBox TxtCla3;
        private System.Windows.Forms.RibbonTextBox TxtCla4;
        private System.Windows.Forms.RibbonTextBox TxtCla5;
        private System.Windows.Forms.RibbonTextBox TxtCla6;
        private System.Windows.Forms.DataGridViewTextBoxColumn ARTICULO;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRD;
        private System.Windows.Forms.DataGridViewTextBoxColumn VTA;
        private System.Windows.Forms.ComboBox cmb_distribuidora;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_total;
        private System.Windows.Forms.Label lbl_detalle;
    }
}
