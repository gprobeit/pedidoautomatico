﻿namespace PedAuto.Formularios
{
    partial class FrmPedido
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPedido));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ribbonTab2 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel1 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel2 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel3 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel4 = new System.Windows.Forms.RibbonPanel();
            this.ribbonTab1 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel5 = new System.Windows.Forms.RibbonPanel();
            this.BtnNuevo = new System.Windows.Forms.RibbonButton();
            this.BtnBuscar = new System.Windows.Forms.RibbonButton();
            this.BtnGuardar = new System.Windows.Forms.RibbonButton();
            this.BtnImprimir = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel9 = new System.Windows.Forms.RibbonPanel();
            this.TxtPedido = new System.Windows.Forms.RibbonLabel();
            this.ribbonPanel6 = new System.Windows.Forms.RibbonPanel();
            this.cmbestado = new System.Windows.Forms.RibbonComboBox();
            this.ribbonButton5 = new System.Windows.Forms.RibbonButton();
            this.ribbonButton6 = new System.Windows.Forms.RibbonButton();
            this.panelTipo = new System.Windows.Forms.RibbonPanel();
            this.rbprd = new System.Windows.Forms.RibbonCheckBox();
            this.rbventa = new System.Windows.Forms.RibbonCheckBox();
            this.rbinsumo = new System.Windows.Forms.RibbonCheckBox();
            this.rbter = new System.Windows.Forms.RibbonCheckBox();
            this.ribbonPanel8 = new System.Windows.Forms.RibbonPanel();
            this.cmbsucursal = new System.Windows.Forms.RibbonComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lbltipo = new System.Windows.Forms.Label();
            this.ribbon1 = new System.Windows.Forms.Ribbon();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnBusqueda = new System.Windows.Forms.Button();
            this.BtnActualiza = new System.Windows.Forms.Button();
            this.TxtCantidad = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtDescrip = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtArticulo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Dg = new System.Windows.Forms.DataGridView();
            this.Articulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Existencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.El = new System.Windows.Forms.DataGridViewButtonColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dg)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonTab2
            // 
            this.ribbonTab2.Panels.Add(this.ribbonPanel1);
            this.ribbonTab2.Panels.Add(this.ribbonPanel2);
            this.ribbonTab2.Panels.Add(this.ribbonPanel3);
            this.ribbonTab2.Panels.Add(this.ribbonPanel4);
            this.ribbonTab2.Text = "ribbonTab2";
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.Text = "ribbonPanel1";
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.Text = "ribbonPanel2";
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.Text = "ribbonPanel3";
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.Text = "ribbonPanel4";
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Panels.Add(this.ribbonPanel5);
            this.ribbonTab1.Panels.Add(this.ribbonPanel9);
            this.ribbonTab1.Panels.Add(this.ribbonPanel6);
            this.ribbonTab1.Panels.Add(this.panelTipo);
            this.ribbonTab1.Panels.Add(this.ribbonPanel8);
            this.ribbonTab1.Text = "Sistema";
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.Items.Add(this.BtnNuevo);
            this.ribbonPanel5.Items.Add(this.BtnBuscar);
            this.ribbonPanel5.Items.Add(this.BtnGuardar);
            this.ribbonPanel5.Items.Add(this.BtnImprimir);
            this.ribbonPanel5.Text = "Acciones";
            // 
            // BtnNuevo
            // 
            this.BtnNuevo.Image = global::PedAuto.Properties.Resources.nuevo;
            this.BtnNuevo.SmallImage = global::PedAuto.Properties.Resources.nuevo;
            this.BtnNuevo.Text = "";
            this.BtnNuevo.ToolTip = "Nuevo";
            this.BtnNuevo.Click += new System.EventHandler(this.BtnNuevo_Click);
            // 
            // BtnBuscar
            // 
            this.BtnBuscar.Image = global::PedAuto.Properties.Resources.search;
            this.BtnBuscar.SmallImage = ((System.Drawing.Image)(resources.GetObject("BtnBuscar.SmallImage")));
            this.BtnBuscar.Text = "";
            this.BtnBuscar.ToolTip = "Buscar";
            // 
            // BtnGuardar
            // 
            this.BtnGuardar.Image = global::PedAuto.Properties.Resources.save;
            this.BtnGuardar.SmallImage = ((System.Drawing.Image)(resources.GetObject("BtnGuardar.SmallImage")));
            this.BtnGuardar.Text = "";
            this.BtnGuardar.ToolTip = "Guardar";
            this.BtnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // BtnImprimir
            // 
            this.BtnImprimir.Image = global::PedAuto.Properties.Resources.print;
            this.BtnImprimir.SmallImage = ((System.Drawing.Image)(resources.GetObject("BtnImprimir.SmallImage")));
            this.BtnImprimir.Text = "";
            this.BtnImprimir.ToolTip = "Consultar";
            // 
            // ribbonPanel9
            // 
            this.ribbonPanel9.Items.Add(this.TxtPedido);
            this.ribbonPanel9.Text = "Pedido #";
            // 
            // TxtPedido
            // 
            this.TxtPedido.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.TxtPedido.Text = "#";
            this.TxtPedido.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            // 
            // ribbonPanel6
            // 
            this.ribbonPanel6.Items.Add(this.cmbestado);
            this.ribbonPanel6.Text = "Estado";
            // 
            // cmbestado
            // 
            this.cmbestado.DropDownItems.Add(this.ribbonButton5);
            this.cmbestado.DropDownItems.Add(this.ribbonButton6);
            this.cmbestado.Text = "";
            this.cmbestado.TextBoxText = "DIGITADO";
            // 
            // ribbonButton5
            // 
            this.ribbonButton5.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton5.Image")));
            this.ribbonButton5.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton5.SmallImage")));
            this.ribbonButton5.Text = "DIGITADO";
            this.ribbonButton5.Value = "1";
            // 
            // ribbonButton6
            // 
            this.ribbonButton6.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton6.Image")));
            this.ribbonButton6.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton6.SmallImage")));
            this.ribbonButton6.Text = "APROBADO";
            this.ribbonButton6.Value = "2";
            // 
            // panelTipo
            // 
            this.panelTipo.Items.Add(this.rbprd);
            this.panelTipo.Items.Add(this.rbventa);
            this.panelTipo.Items.Add(this.rbinsumo);
            this.panelTipo.Items.Add(this.rbter);
            this.panelTipo.Text = "Tipo";
            // 
            // rbprd
            // 
            this.rbprd.CheckedGroup = "1";
            this.rbprd.Style = System.Windows.Forms.RibbonCheckBox.CheckBoxStyle.RadioButton;
            this.rbprd.Text = "Produccion";
            this.rbprd.Value = "PRD";
            this.rbprd.CheckBoxCheckChanged += new System.EventHandler(this.rb_CheckBoxCheckChanged);
            // 
            // rbventa
            // 
            this.rbventa.CheckedGroup = "1";
            this.rbventa.Style = System.Windows.Forms.RibbonCheckBox.CheckBoxStyle.RadioButton;
            this.rbventa.Text = "Venta";
            this.rbventa.Value = "VTA";
            this.rbventa.CheckBoxCheckChanged += new System.EventHandler(this.rb_CheckBoxCheckChanged);
            // 
            // rbinsumo
            // 
            this.rbinsumo.CheckedGroup = "1";
            this.rbinsumo.Style = System.Windows.Forms.RibbonCheckBox.CheckBoxStyle.RadioButton;
            this.rbinsumo.Text = "Insumo";
            this.rbinsumo.Value = "INS";
            this.rbinsumo.CheckBoxCheckChanged += new System.EventHandler(this.rb_CheckBoxCheckChanged);
            // 
            // rbter
            // 
            this.rbter.CheckedGroup = "1";
            this.rbter.Style = System.Windows.Forms.RibbonCheckBox.CheckBoxStyle.RadioButton;
            this.rbter.Text = "Terceros";
            this.rbter.Value = "TER";
            this.rbter.CheckBoxCheckChanged += new System.EventHandler(this.rb_CheckBoxCheckChanged);
            // 
            // ribbonPanel8
            // 
            this.ribbonPanel8.Items.Add(this.cmbsucursal);
            this.ribbonPanel8.Text = "Sucursal";
            // 
            // cmbsucursal
            // 
            this.cmbsucursal.Enabled = false;
            this.cmbsucursal.Text = "";
            this.cmbsucursal.TextBoxText = "";
            this.cmbsucursal.TextBoxWidth = 150;
            // 
            // lbltipo
            // 
            this.lbltipo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbltipo.AutoSize = true;
            this.lbltipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltipo.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbltipo.Location = new System.Drawing.Point(332, 3);
            this.lbltipo.Name = "lbltipo";
            this.lbltipo.Size = new System.Drawing.Size(163, 20);
            this.lbltipo.TabIndex = 9;
            this.lbltipo.Text = "Pedido de Produccion";
            this.lbltipo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ribbon1
            // 
            this.ribbon1.CaptionBarVisible = false;
            this.ribbon1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ribbon1.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.Minimized = false;
            this.ribbon1.Name = "ribbon1";
            // 
            // 
            // 
            this.ribbon1.OrbDropDown.BorderRoundness = 8;
            this.ribbon1.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.OrbDropDown.Name = "";
            this.ribbon1.OrbDropDown.Size = new System.Drawing.Size(527, 447);
            this.ribbon1.OrbDropDown.TabIndex = 0;
            this.ribbon1.OrbImage = null;
            this.ribbon1.OrbVisible = false;
            this.ribbon1.Size = new System.Drawing.Size(806, 106);
            this.ribbon1.TabIndex = 7;
            this.ribbon1.Tabs.Add(this.ribbonTab1);
            this.ribbon1.TabsMargin = new System.Windows.Forms.Padding(12, 2, 20, 0);
            this.ribbon1.Text = "ribbon1";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox1.Controls.Add(this.btnBusqueda);
            this.groupBox1.Controls.Add(this.BtnActualiza);
            this.groupBox1.Controls.Add(this.TxtCantidad);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.TxtDescrip);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TxtArticulo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(63, 124);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(680, 62);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nuevo Articulo";
            // 
            // btnBusqueda
            // 
            this.btnBusqueda.Image = global::PedAuto.Properties.Resources.searchSmall;
            this.btnBusqueda.Location = new System.Drawing.Point(24, 20);
            this.btnBusqueda.Name = "btnBusqueda";
            this.btnBusqueda.Size = new System.Drawing.Size(31, 31);
            this.btnBusqueda.TabIndex = 8;
            this.btnBusqueda.UseVisualStyleBackColor = true;
            this.btnBusqueda.Click += new System.EventHandler(this.btnBusqueda_Click);
            // 
            // BtnActualiza
            // 
            this.BtnActualiza.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnActualiza.Image = global::PedAuto.Properties.Resources.refresch;
            this.BtnActualiza.Location = new System.Drawing.Point(636, 20);
            this.BtnActualiza.Name = "BtnActualiza";
            this.BtnActualiza.Size = new System.Drawing.Size(31, 31);
            this.BtnActualiza.TabIndex = 8;
            this.toolTip1.SetToolTip(this.BtnActualiza, "Actualizar Existencias");
            this.BtnActualiza.UseVisualStyleBackColor = true;
            this.BtnActualiza.Click += new System.EventHandler(this.BtnActualiza_Click);
            // 
            // TxtCantidad
            // 
            this.TxtCantidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtCantidad.Location = new System.Drawing.Point(514, 30);
            this.TxtCantidad.Name = "TxtCantidad";
            this.TxtCantidad.Size = new System.Drawing.Size(100, 20);
            this.TxtCantidad.TabIndex = 7;
            this.TxtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtCantidad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtCantidad_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(517, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Cantidad";
            // 
            // TxtDescrip
            // 
            this.TxtDescrip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtDescrip.Enabled = false;
            this.TxtDescrip.Location = new System.Drawing.Point(247, 30);
            this.TxtDescrip.Name = "TxtDescrip";
            this.TxtDescrip.Size = new System.Drawing.Size(237, 20);
            this.TxtDescrip.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(250, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Descripcion";
            // 
            // TxtArticulo
            // 
            this.TxtArticulo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtArticulo.Location = new System.Drawing.Point(83, 30);
            this.TxtArticulo.Name = "TxtArticulo";
            this.TxtArticulo.Size = new System.Drawing.Size(134, 20);
            this.TxtArticulo.TabIndex = 1;
            this.TxtArticulo.Validated += new System.EventHandler(this.TxtArticulo_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Articulo";
            // 
            // Dg
            // 
            this.Dg.AllowUserToAddRows = false;
            this.Dg.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
            this.Dg.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Dg.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Dg.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.Dg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(230)))), ((int)(((byte)(246)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Dg.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Dg.ColumnHeadersHeight = 26;
            this.Dg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Articulo,
            this.Descripcion,
            this.Cantidad,
            this.Existencia,
            this.El});
            this.Dg.Enabled = false;
            this.Dg.EnableHeadersVisualStyles = false;
            this.Dg.Location = new System.Drawing.Point(7, 195);
            this.Dg.Name = "Dg";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Dg.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.Dg.RowHeadersVisible = false;
            this.Dg.Size = new System.Drawing.Size(792, 269);
            this.Dg.TabIndex = 0;
            this.Dg.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dg_CellClick);
            this.Dg.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dg_CellValueChanged);
            // 
            // Articulo
            // 
            this.Articulo.HeaderText = "ARTICULO";
            this.Articulo.Name = "Articulo";
            this.Articulo.ReadOnly = true;
            this.Articulo.Width = 150;
            // 
            // Descripcion
            // 
            this.Descripcion.HeaderText = "DESCRIPCION";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.ReadOnly = true;
            this.Descripcion.Width = 250;
            // 
            // Cantidad
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Cantidad.DefaultCellStyle = dataGridViewCellStyle3;
            this.Cantidad.HeaderText = "CANTIDAD";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.Width = 125;
            // 
            // Existencia
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Existencia.DefaultCellStyle = dataGridViewCellStyle4;
            this.Existencia.HeaderText = "EXISTENCIA";
            this.Existencia.Name = "Existencia";
            this.Existencia.ReadOnly = true;
            this.Existencia.Width = 125;
            // 
            // El
            // 
            this.El.HeaderText = "ELIMINAR";
            this.El.Name = "El";
            this.El.Text = "Eliminar";
            this.El.UseColumnTextForButtonValue = true;
            // 
            // FrmPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.ClientSize = new System.Drawing.Size(806, 473);
            this.Controls.Add(this.lbltipo);
            this.Controls.Add(this.ribbon1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Dg);
            this.Name = "FrmPedido";
            this.Load += new System.EventHandler(this.FrmPedido_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Dg;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TxtCantidad;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtDescrip;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtArticulo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBusqueda;
        private System.Windows.Forms.RibbonTab ribbonTab2;
        private System.Windows.Forms.RibbonPanel ribbonPanel1;
        private System.Windows.Forms.RibbonPanel ribbonPanel2;
        private System.Windows.Forms.RibbonPanel ribbonPanel3;
        private System.Windows.Forms.RibbonPanel ribbonPanel4;
        private System.Windows.Forms.Ribbon ribbon1;
        private System.Windows.Forms.RibbonTab ribbonTab1;
        private System.Windows.Forms.RibbonPanel ribbonPanel5;
        private System.Windows.Forms.RibbonButton BtnNuevo;
        private System.Windows.Forms.RibbonButton BtnBuscar;
        private System.Windows.Forms.RibbonButton BtnGuardar;
        private System.Windows.Forms.RibbonButton BtnImprimir;
        private System.Windows.Forms.RibbonPanel ribbonPanel6;
        private System.Windows.Forms.RibbonComboBox cmbestado;
        private System.Windows.Forms.RibbonPanel panelTipo;
        private System.Windows.Forms.RibbonCheckBox rbprd;
        private System.Windows.Forms.RibbonCheckBox rbinsumo;
        private System.Windows.Forms.RibbonCheckBox rbventa;
        private System.Windows.Forms.RibbonCheckBox rbter;
        private System.Windows.Forms.RibbonPanel ribbonPanel8;
        private System.Windows.Forms.RibbonComboBox cmbsucursal;
        private System.Windows.Forms.RibbonButton ribbonButton5;
        private System.Windows.Forms.RibbonButton ribbonButton6;
        private System.Windows.Forms.Button BtnActualiza;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label lbltipo;
        private System.Windows.Forms.RibbonLabel TxtPedido;
        private System.Windows.Forms.RibbonPanel ribbonPanel9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Articulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Existencia;
        private System.Windows.Forms.DataGridViewButtonColumn El;
    }
}
