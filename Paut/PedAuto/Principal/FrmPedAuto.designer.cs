﻿namespace PedAuto.Formularios
{
    partial class FrmPedAuto
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPedAuto));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ribbonTab1 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel1 = new System.Windows.Forms.RibbonPanel();
            this.lbl_suc = new System.Windows.Forms.RibbonLabel();
            this.cmbSuc = new System.Windows.Forms.RibbonComboBox();
            this.ribbonButton1 = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel2 = new System.Windows.Forms.RibbonPanel();
            this.cmbSociedad = new System.Windows.Forms.RibbonComboBox();
            this.cmbBodega = new System.Windows.Forms.RibbonComboBox();
            this.BtnActualiza = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel3 = new System.Windows.Forms.RibbonPanel();
            this.BtnDel = new System.Windows.Forms.RibbonButton();
            this.BtnPend = new System.Windows.Forms.RibbonButton();
            this.BtnPedD = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel4 = new System.Windows.Forms.RibbonPanel();
            this.btn_Reset = new System.Windows.Forms.RibbonButton();
            this.ribbon1 = new System.Windows.Forms.Ribbon();
            this.Dg = new System.Windows.Forms.DataGridView();
            this.Sel = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Existencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Facturar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Proveedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chtodos = new System.Windows.Forms.CheckBox();
            this.lblfecha = new System.Windows.Forms.RibbonLabel();
            ((System.ComponentModel.ISupportInitialize)(this.Dg)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Panels.Add(this.ribbonPanel1);
            this.ribbonTab1.Panels.Add(this.ribbonPanel2);
            this.ribbonTab1.Panels.Add(this.ribbonPanel3);
            this.ribbonTab1.Panels.Add(this.ribbonPanel4);
            this.ribbonTab1.Text = "Procesar Ventas";
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.Items.Add(this.lbl_suc);
            this.ribbonPanel1.Items.Add(this.cmbSuc);
            this.ribbonPanel1.Text = "";
            // 
            // lbl_suc
            // 
            this.lbl_suc.Text = "Sucursal Envio";
            this.lbl_suc.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Right;
            // 
            // cmbSuc
            // 
            this.cmbSuc.DropDownItems.Add(this.ribbonButton1);
            this.cmbSuc.Text = "";
            this.cmbSuc.TextBoxText = "";
            this.cmbSuc.TextBoxWidth = 200;
            this.cmbSuc.DropDownItemClicked += new System.Windows.Forms.RibbonComboBox.RibbonItemEventHandler(this.cmbSuc_DropDownItemClicked);
            // 
            // ribbonButton1
            // 
            this.ribbonButton1.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.Image")));
            this.ribbonButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.SmallImage")));
            this.ribbonButton1.Text = "Sucursal Envio";
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.Items.Add(this.cmbSociedad);
            this.ribbonPanel2.Items.Add(this.cmbBodega);
            this.ribbonPanel2.Items.Add(this.BtnActualiza);
            this.ribbonPanel2.Text = "";
            // 
            // cmbSociedad
            // 
            this.cmbSociedad.Text = "Sociedad:";
            this.cmbSociedad.TextBoxText = "";
            this.cmbSociedad.TextBoxWidth = 142;
            this.cmbSociedad.DropDownItemClicked += new System.Windows.Forms.RibbonComboBox.RibbonItemEventHandler(this.cmbSociedad_DropDownItemClicked);
            // 
            // cmbBodega
            // 
            this.cmbBodega.Text = "Bodega:  ";
            this.cmbBodega.TextBoxText = "";
            this.cmbBodega.TextBoxWidth = 150;
            this.cmbBodega.DropDownItemClicked += new System.Windows.Forms.RibbonComboBox.RibbonItemEventHandler(this.cmbBodega_DropDownItemClicked);
            // 
            // BtnActualiza
            // 
            this.BtnActualiza.Image = global::PedAuto.Properties.Resources.refresch;
            this.BtnActualiza.SmallImage = ((System.Drawing.Image)(resources.GetObject("BtnActualiza.SmallImage")));
            this.BtnActualiza.Text = "Actualizar Existencias";
            this.BtnActualiza.Click += new System.EventHandler(this.BtnActualiza_Click);
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.Items.Add(this.BtnDel);
            this.ribbonPanel3.Items.Add(this.BtnPend);
            this.ribbonPanel3.Items.Add(this.BtnPedD);
            this.ribbonPanel3.Text = "";
            // 
            // BtnDel
            // 
            this.BtnDel.Image = global::PedAuto.Properties.Resources.borrar;
            this.BtnDel.SmallImage = ((System.Drawing.Image)(resources.GetObject("BtnDel.SmallImage")));
            this.BtnDel.Text = "Eliminar Articulos";
            this.BtnDel.ToolTip = "Eliminar Articulos";
            this.BtnDel.Click += new System.EventHandler(this.BtnDel_Click);
            // 
            // BtnPend
            // 
            this.BtnPend.Image = global::PedAuto.Properties.Resources.clear;
            this.BtnPend.SmallImage = ((System.Drawing.Image)(resources.GetObject("BtnPend.SmallImage")));
            this.BtnPend.Text = "Articulos Pendientes";
            this.BtnPend.ToolTip = "Articulos Pendientes";
            this.BtnPend.Click += new System.EventHandler(this.BtnPend_Click);
            // 
            // BtnPedD
            // 
            this.BtnPedD.Image = global::PedAuto.Properties.Resources.nuevo;
            this.BtnPedD.SmallImage = ((System.Drawing.Image)(resources.GetObject("BtnPedD.SmallImage")));
            this.BtnPedD.Text = "Generar Pedido";
            this.BtnPedD.ToolTip = "Pedido";
            this.BtnPedD.Click += new System.EventHandler(this.BtnPed_Click);
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.Items.Add(this.btn_Reset);
            this.ribbonPanel4.Text = null;
            // 
            // btn_Reset
            // 
            this.btn_Reset.Image = global::PedAuto.Properties.Resources.reset;
            this.btn_Reset.SmallImage = ((System.Drawing.Image)(resources.GetObject("btn_Reset.SmallImage")));
            this.btn_Reset.Text = "Inicializar Tablero";
            this.btn_Reset.ToolTip = "Inicializar Tablero";
            this.btn_Reset.Click += new System.EventHandler(this.btn_Reset_Click);
            // 
            // ribbon1
            // 
            this.ribbon1.CaptionBarVisible = false;
            this.ribbon1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ribbon1.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.Minimized = false;
            this.ribbon1.Name = "ribbon1";
            // 
            // 
            // 
            this.ribbon1.OrbDropDown.BorderRoundness = 8;
            this.ribbon1.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.OrbDropDown.Name = "";
            this.ribbon1.OrbDropDown.Size = new System.Drawing.Size(527, 447);
            this.ribbon1.OrbDropDown.TabIndex = 0;
            this.ribbon1.OrbImage = null;
            this.ribbon1.OrbVisible = false;
            this.ribbon1.Size = new System.Drawing.Size(876, 117);
            this.ribbon1.TabIndex = 8;
            this.ribbon1.Tabs.Add(this.ribbonTab1);
            this.ribbon1.TabsMargin = new System.Windows.Forms.Padding(12, 2, 20, 0);
            this.ribbon1.Text = "ribbon1";
            // 
            // Dg
            // 
            this.Dg.AllowUserToAddRows = false;
            this.Dg.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
            this.Dg.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Dg.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(230)))), ((int)(((byte)(246)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Dg.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Dg.ColumnHeadersHeight = 25;
            this.Dg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Sel,
            this.Codigo,
            this.Descripcion,
            this.Cantidad,
            this.Existencia,
            this.Facturar,
            this.Proveedor,
            this.Fecha});
            this.Dg.EnableHeadersVisualStyles = false;
            this.Dg.Location = new System.Drawing.Point(23, 142);
            this.Dg.Name = "Dg";
            this.Dg.RowHeadersVisible = false;
            this.Dg.Size = new System.Drawing.Size(830, 302);
            this.Dg.TabIndex = 9;
            this.Dg.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dg_CellContentClick);
            this.Dg.CurrentCellDirtyStateChanged += new System.EventHandler(this.Dg_CurrentCellDirtyStateChanged);
            // 
            // Sel
            // 
            this.Sel.HeaderText = "Sel";
            this.Sel.Name = "Sel";
            // 
            // Codigo
            // 
            this.Codigo.DataPropertyName = "Codigo";
            this.Codigo.HeaderText = "Codigo";
            this.Codigo.Name = "Codigo";
            this.Codigo.ReadOnly = true;
            // 
            // Descripcion
            // 
            this.Descripcion.DataPropertyName = "Descripcion";
            this.Descripcion.HeaderText = "Descripcion";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.ReadOnly = true;
            // 
            // Cantidad
            // 
            this.Cantidad.DataPropertyName = "Cantidad";
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.ReadOnly = true;
            // 
            // Existencia
            // 
            this.Existencia.DataPropertyName = "Existencia";
            this.Existencia.HeaderText = "Existencia";
            this.Existencia.Name = "Existencia";
            this.Existencia.ReadOnly = true;
            // 
            // Facturar
            // 
            this.Facturar.DataPropertyName = "Facturar";
            this.Facturar.HeaderText = "Pedido";
            this.Facturar.Name = "Facturar";
            // 
            // Proveedor
            // 
            this.Proveedor.DataPropertyName = "Proveedor";
            this.Proveedor.HeaderText = "Codigo Soc";
            this.Proveedor.Name = "Proveedor";
            this.Proveedor.ReadOnly = true;
            // 
            // Fecha
            // 
            this.Fecha.DataPropertyName = "Fecha";
            this.Fecha.HeaderText = "Fecha";
            this.Fecha.Name = "Fecha";
            this.Fecha.ReadOnly = true;
            // 
            // chtodos
            // 
            this.chtodos.AutoSize = true;
            this.chtodos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chtodos.Location = new System.Drawing.Point(25, 119);
            this.chtodos.Name = "chtodos";
            this.chtodos.Size = new System.Drawing.Size(159, 20);
            this.chtodos.TabIndex = 10;
            this.chtodos.Text = "Seleccionar Todos";
            this.chtodos.UseVisualStyleBackColor = true;
            this.chtodos.CheckedChanged += new System.EventHandler(this.chtodos_CheckedChanged);
            // 
            // lblfecha
            // 
            this.lblfecha.Text = "Actualizado al:";
            // 
            // FrmPedAuto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(876, 456);
            this.Controls.Add(this.chtodos);
            this.Controls.Add(this.Dg);
            this.Controls.Add(this.ribbon1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmPedAuto";
            this.Load += new System.EventHandler(this.FrmPedAuto_Load);
            this.Resize += new System.EventHandler(this.FrmPedAuto_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.Dg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Ribbon ribbon1;
        private System.Windows.Forms.RibbonTab ribbonTab1;
        private System.Windows.Forms.RibbonPanel ribbonPanel1;
        private System.Windows.Forms.RibbonComboBox cmbSuc;
        private System.Windows.Forms.RibbonPanel ribbonPanel2;
        private System.Windows.Forms.RibbonComboBox cmbBodega;
        private System.Windows.Forms.RibbonButton BtnActualiza;
        private System.Windows.Forms.RibbonPanel ribbonPanel3;
        private System.Windows.Forms.RibbonButton BtnDel;
        private System.Windows.Forms.RibbonButton BtnPedD;
        private System.Windows.Forms.RibbonButton BtnPend;
        private System.Windows.Forms.DataGridView Dg;
        private System.Windows.Forms.CheckBox chtodos;
        private System.Windows.Forms.RibbonComboBox cmbSociedad;
        private System.Windows.Forms.RibbonLabel lblfecha;
        private System.Windows.Forms.RibbonLabel lbl_suc;
        private System.Windows.Forms.RibbonButton ribbonButton1;
        private System.Windows.Forms.RibbonPanel ribbonPanel4;
        private System.Windows.Forms.RibbonButton btn_Reset;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Sel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Existencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn Facturar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Proveedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha;
    }
}
