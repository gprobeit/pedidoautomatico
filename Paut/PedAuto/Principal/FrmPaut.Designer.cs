﻿namespace PedAuto.Formularios
{
    partial class FrmPaut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPaut));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ribbon1 = new System.Windows.Forms.Ribbon();
            this.ribbonTab1 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel1 = new System.Windows.Forms.RibbonPanel();
            this.ribbonLabel2 = new System.Windows.Forms.RibbonLabel();
            this.cmb_suc = new System.Windows.Forms.RibbonComboBox();
            this.lblfecha = new System.Windows.Forms.RibbonLabel();
            this.ribbonPanel2 = new System.Windows.Forms.RibbonPanel();
            this.cmb_soc = new System.Windows.Forms.RibbonComboBox();
            this.cmb_bod = new System.Windows.Forms.RibbonComboBox();
            this.ribbonPanel3 = new System.Windows.Forms.RibbonPanel();
            this.btn_actualizar_ = new System.Windows.Forms.RibbonButton();
            this.btn_eliminar = new System.Windows.Forms.RibbonButton();
            this.btn_limpiar = new System.Windows.Forms.RibbonButton();
            this.btn_pedido = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel5 = new System.Windows.Forms.RibbonPanel();
            this.chb_existencias = new System.Windows.Forms.RibbonCheckBox();
            this.btn_filtrar = new System.Windows.Forms.RibbonButton();
            this.btn_QuitarF = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel4 = new System.Windows.Forms.RibbonPanel();
            this.btn_incializar = new System.Windows.Forms.RibbonButton();
            this.ribbonTab2 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel6 = new System.Windows.Forms.RibbonPanel();
            this.rbbReporte = new System.Windows.Forms.RibbonButton();
            this.chtodos = new System.Windows.Forms.CheckBox();
            this.Dg = new System.Windows.Forms.DataGridView();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lbl_procesando = new System.Windows.Forms.Label();
            this.lblconteo = new System.Windows.Forms.Label();
            this.ep1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Dg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ep1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon1
            // 
            this.ribbon1.CaptionBarVisible = false;
            this.ribbon1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ribbon1.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.Minimized = false;
            this.ribbon1.Name = "ribbon1";
            // 
            // 
            // 
            this.ribbon1.OrbDropDown.BorderRoundness = 8;
            this.ribbon1.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.OrbDropDown.Name = "";
            this.ribbon1.OrbDropDown.Size = new System.Drawing.Size(527, 447);
            this.ribbon1.OrbDropDown.TabIndex = 0;
            this.ribbon1.OrbImage = null;
            this.ribbon1.OrbVisible = false;
            this.ribbon1.Size = new System.Drawing.Size(1009, 117);
            this.ribbon1.TabIndex = 9;
            this.ribbon1.Tabs.Add(this.ribbonTab1);
            this.ribbon1.Tabs.Add(this.ribbonTab2);
            this.ribbon1.TabsMargin = new System.Windows.Forms.Padding(12, 2, 20, 0);
            this.ribbon1.Text = "ribbon1";
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Panels.Add(this.ribbonPanel1);
            this.ribbonTab1.Panels.Add(this.ribbonPanel2);
            this.ribbonTab1.Panels.Add(this.ribbonPanel3);
            this.ribbonTab1.Panels.Add(this.ribbonPanel5);
            this.ribbonTab1.Panels.Add(this.ribbonPanel4);
            this.ribbonTab1.Text = "Procesar Pedidos";
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.Items.Add(this.ribbonLabel2);
            this.ribbonPanel1.Items.Add(this.cmb_suc);
            this.ribbonPanel1.Items.Add(this.lblfecha);
            this.ribbonPanel1.Text = "";
            // 
            // ribbonLabel2
            // 
            this.ribbonLabel2.Text = "Sucursal de Envio:";
            // 
            // cmb_suc
            // 
            this.cmb_suc.Text = "";
            this.cmb_suc.TextBoxText = "";
            this.cmb_suc.TextBoxWidth = 150;
            this.cmb_suc.DropDownItemClicked += new System.Windows.Forms.RibbonComboBox.RibbonItemEventHandler(this.cmb_suc_DropDownItemClicked);
            // 
            // lblfecha
            // 
            this.lblfecha.Text = "Ultima Actualización: ";
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.Items.Add(this.cmb_soc);
            this.ribbonPanel2.Items.Add(this.cmb_bod);
            this.ribbonPanel2.Text = "Distribuidor";
            // 
            // cmb_soc
            // 
            this.cmb_soc.Text = "Proveedor:";
            this.cmb_soc.TextBoxText = "";
            this.cmb_soc.TextBoxWidth = 145;
            this.cmb_soc.DropDownItemClicked += new System.Windows.Forms.RibbonComboBox.RibbonItemEventHandler(this.cmb_soc_DropDownItemClicked);
            // 
            // cmb_bod
            // 
            this.cmb_bod.Text = "Bodega:";
            this.cmb_bod.TextBoxText = "";
            this.cmb_bod.TextBoxWidth = 159;
            this.cmb_bod.DropDownItemClicked += new System.Windows.Forms.RibbonComboBox.RibbonItemEventHandler(this.cmb_bod_DropDownItemClicked);
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.Items.Add(this.btn_actualizar_);
            this.ribbonPanel3.Items.Add(this.btn_eliminar);
            this.ribbonPanel3.Items.Add(this.btn_limpiar);
            this.ribbonPanel3.Items.Add(this.btn_pedido);
            this.ribbonPanel3.Text = "Procesos";
            // 
            // btn_actualizar_
            // 
            this.btn_actualizar_.Image = global::PedAuto.Properties.Resources.actualizar_restaure_todas_las_pestanas_icono_7808_32;
            this.btn_actualizar_.SmallImage = ((System.Drawing.Image)(resources.GetObject("btn_actualizar_.SmallImage")));
            this.btn_actualizar_.Text = "Actualizar Existencia";
            this.btn_actualizar_.ToolTip = "Actualizar";
            this.btn_actualizar_.Click += new System.EventHandler(this.btn_actualizar__Click);
            // 
            // btn_eliminar
            // 
            this.btn_eliminar.Image = global::PedAuto.Properties.Resources.borrar1;
            this.btn_eliminar.SmallImage = ((System.Drawing.Image)(resources.GetObject("btn_eliminar.SmallImage")));
            this.btn_eliminar.Text = "Eliminar Articulos";
            this.btn_eliminar.ToolTip = "Eliminar";
            this.btn_eliminar.Click += new System.EventHandler(this.btn_eliminar_Click);
            // 
            // btn_limpiar
            // 
            this.btn_limpiar.Image = global::PedAuto.Properties.Resources.clear;
            this.btn_limpiar.SmallImage = ((System.Drawing.Image)(resources.GetObject("btn_limpiar.SmallImage")));
            this.btn_limpiar.Text = "Articulos Pendientes";
            this.btn_limpiar.ToolTip = "Limpiar";
            this.btn_limpiar.Click += new System.EventHandler(this.btn_limpiar_Click);
            // 
            // btn_pedido
            // 
            this.btn_pedido.Image = global::PedAuto.Properties.Resources.nuevo;
            this.btn_pedido.SmallImage = ((System.Drawing.Image)(resources.GetObject("btn_pedido.SmallImage")));
            this.btn_pedido.Text = "Generar Pedido";
            this.btn_pedido.ToolTip = "Pedido";
            this.btn_pedido.Click += new System.EventHandler(this.btn_pedido_Click);
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.Items.Add(this.chb_existencias);
            this.ribbonPanel5.Items.Add(this.btn_filtrar);
            this.ribbonPanel5.Items.Add(this.btn_QuitarF);
            this.ribbonPanel5.Text = "Filtros";
            // 
            // chb_existencias
            // 
            this.chb_existencias.Checked = true;
            this.chb_existencias.Text = "Existencias  Mayor a 0";
            this.chb_existencias.CheckBoxCheckChanged += new System.EventHandler(this.chb_existencias_CheckBoxCheckChanged);
            // 
            // btn_filtrar
            // 
            this.btn_filtrar.Image = ((System.Drawing.Image)(resources.GetObject("btn_filtrar.Image")));
            this.btn_filtrar.SmallImage = ((System.Drawing.Image)(resources.GetObject("btn_filtrar.SmallImage")));
            this.btn_filtrar.Text = "Filtrar";
            this.btn_filtrar.ToolTip = "Filtrar";
            this.btn_filtrar.Click += new System.EventHandler(this.btn_filtrar_Click);
            // 
            // btn_QuitarF
            // 
            this.btn_QuitarF.Image = ((System.Drawing.Image)(resources.GetObject("btn_QuitarF.Image")));
            this.btn_QuitarF.SmallImage = ((System.Drawing.Image)(resources.GetObject("btn_QuitarF.SmallImage")));
            this.btn_QuitarF.Text = "Quitar Filtro";
            this.btn_QuitarF.ToolTip = "Quitar Filtro";
            this.btn_QuitarF.Click += new System.EventHandler(this.btn_QuitarF_Click);
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.Items.Add(this.btn_incializar);
            this.ribbonPanel4.Text = "";
            // 
            // btn_incializar
            // 
            this.btn_incializar.Image = global::PedAuto.Properties.Resources.reset;
            this.btn_incializar.SmallImage = ((System.Drawing.Image)(resources.GetObject("btn_incializar.SmallImage")));
            this.btn_incializar.Text = "Inicializar Tablero";
            this.btn_incializar.ToolTip = "Inicializar";
            this.btn_incializar.Click += new System.EventHandler(this.btn_incializar_Click);
            // 
            // ribbonTab2
            // 
            this.ribbonTab2.Panels.Add(this.ribbonPanel6);
            this.ribbonTab2.Text = "Reporte";
            // 
            // ribbonPanel6
            // 
            this.ribbonPanel6.Items.Add(this.rbbReporte);
            this.ribbonPanel6.Text = "";
            // 
            // rbbReporte
            // 
            this.rbbReporte.Image = ((System.Drawing.Image)(resources.GetObject("rbbReporte.Image")));
            this.rbbReporte.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbReporte.SmallImage")));
            this.rbbReporte.Text = "Reporte";
            this.rbbReporte.ToolTip = "Crear Reporte";
            this.rbbReporte.Click += new System.EventHandler(this.rbbReporte_Click);
            // 
            // chtodos
            // 
            this.chtodos.AutoSize = true;
            this.chtodos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chtodos.Location = new System.Drawing.Point(20, 119);
            this.chtodos.Name = "chtodos";
            this.chtodos.Size = new System.Drawing.Size(159, 20);
            this.chtodos.TabIndex = 12;
            this.chtodos.Text = "Seleccionar Todos";
            this.chtodos.UseVisualStyleBackColor = true;
            this.chtodos.CheckedChanged += new System.EventHandler(this.chtodos_CheckedChanged);
            // 
            // Dg
            // 
            this.Dg.AllowUserToAddRows = false;
            this.Dg.AllowUserToDeleteRows = false;
            this.Dg.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.Dg.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Dg.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Dg.DefaultCellStyle = dataGridViewCellStyle2;
            this.Dg.Location = new System.Drawing.Point(22, 145);
            this.Dg.Name = "Dg";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Dg.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.Dg.RowHeadersVisible = false;
            this.Dg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dg.Size = new System.Drawing.Size(965, 299);
            this.Dg.TabIndex = 9;
            this.Dg.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dg_CellClick);
            this.Dg.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dg_CellDoubleClick);
            this.Dg.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.Dg_CellFormatting);
            this.Dg.CurrentCellDirtyStateChanged += new System.EventHandler(this.Dg_CurrentCellDirtyStateChanged);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(234, 213);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(504, 43);
            this.progressBar1.TabIndex = 13;
            this.progressBar1.Visible = false;
            // 
            // lbl_procesando
            // 
            this.lbl_procesando.AutoSize = true;
            this.lbl_procesando.Location = new System.Drawing.Point(473, 259);
            this.lbl_procesando.Name = "lbl_procesando";
            this.lbl_procesando.Size = new System.Drawing.Size(73, 13);
            this.lbl_procesando.TabIndex = 15;
            this.lbl_procesando.Text = "Procesando...";
            this.lbl_procesando.Visible = false;
            // 
            // lblconteo
            // 
            this.lblconteo.AutoSize = true;
            this.lblconteo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblconteo.Location = new System.Drawing.Point(859, 122);
            this.lblconteo.Name = "lblconteo";
            this.lblconteo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblconteo.Size = new System.Drawing.Size(41, 13);
            this.lblconteo.TabIndex = 16;
            this.lblconteo.Text = "label1";
            // 
            // ep1
            // 
            this.ep1.BlinkRate = 150;
            this.ep1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink;
            this.ep1.ContainerControl = this;
            // 
            // FrmPaut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1009, 456);
            this.Controls.Add(this.lblconteo);
            this.Controls.Add(this.lbl_procesando);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.Dg);
            this.Controls.Add(this.chtodos);
            this.Controls.Add(this.ribbon1);
            this.Name = "FrmPaut";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmPaut_FormClosed);
            this.Load += new System.EventHandler(this.FrmPaut_Load);
            this.Resize += new System.EventHandler(this.FrmPaut_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.Dg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ep1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Ribbon ribbon1;
        private System.Windows.Forms.RibbonTab ribbonTab1;
        private System.Windows.Forms.RibbonPanel ribbonPanel1;
        private System.Windows.Forms.RibbonPanel ribbonPanel2;
        private System.Windows.Forms.RibbonPanel ribbonPanel3;
        private System.Windows.Forms.RibbonPanel ribbonPanel4;
        private System.Windows.Forms.RibbonLabel ribbonLabel2;
        public System.Windows.Forms.RibbonComboBox cmb_suc;
        private System.Windows.Forms.RibbonComboBox cmb_soc;
        private System.Windows.Forms.RibbonComboBox cmb_bod;
        private System.Windows.Forms.RibbonButton btn_eliminar;
        private System.Windows.Forms.RibbonButton btn_limpiar;
        private System.Windows.Forms.RibbonButton btn_pedido;
        private System.Windows.Forms.RibbonButton btn_incializar;
        private System.Windows.Forms.CheckBox chtodos;
        private System.Windows.Forms.DataGridView Dg;
        private System.Windows.Forms.RibbonPanel ribbonPanel5;
        private System.Windows.Forms.RibbonCheckBox chb_existencias;
        private System.Windows.Forms.RibbonButton btn_actualizar_;
        public System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label lbl_procesando;
        private System.Windows.Forms.Label lblconteo;
        private System.Windows.Forms.RibbonLabel lblfecha;
        private System.Windows.Forms.ErrorProvider ep1;
        private System.Windows.Forms.RibbonButton btn_filtrar;
        private System.Windows.Forms.RibbonButton btn_QuitarF;
        private System.Windows.Forms.RibbonTab ribbonTab2;
        private System.Windows.Forms.RibbonPanel ribbonPanel6;
        private System.Windows.Forms.RibbonButton rbbReporte;
    }
}