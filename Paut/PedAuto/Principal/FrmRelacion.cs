﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PedAuto.Formularios
{
    public partial class FrmRelacion : PedAuto.Formularios.FrmBase
    {
        public FrmRelacion()
        {
            InitializeComponent();
        }


        private void cargarDatos() {

            try 
            {
                DgCatalogo.AutoGenerateColumns = false;

            Dictionary<string, string> par = new Dictionary<string, string>();
            par.Add("@ART", TxtArticulo.TextBoxText);
            par.Add("@DESC", TxtDescripcion.TextBoxText);
            par.Add("@CLA4", TxtCla4.TextBoxText);
            par.Add("@CLA6", TxtCla6.TextBoxText);
            string Estado = "";
            if (rbactivo.Checked)
            {
                Estado = "S";
            }
            else {
                if (rbinactivo.Checked) {
                    Estado = "N";                
                }          
            }

            par.Add("@ACT", Estado);

            string query = "SELECT ARTICULO,DESCRIPCION,PROVEEDOR FROM " +
          " Paut_FnArticulos (@ART,@DESC,@ACT,@CLA4,@CLA6) ";

          DataTable TbDatos = _Sql.ExecuteQuery(query, par);
          DgCatalogo.ClearSelection();
          DgCatalogo.DataSource = TbDatos;
          DgCatalogo.ClearSelection();
          DgArt.Rows.Clear();
          DgArt.ClearSelection();
        
            }
            catch (Exception Ex)
            {

                _Desing.SendMessage(Ex.Message, "1");
            }
        
        }

        private void FrmRelacion_Load(object sender, EventArgs e)
        {
            cargarDatos();
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            cargarDatos();
           
        }
        private void buscarEquivalente(DataGridViewCellEventArgs e)
        {
            try
            {
                             
                Dictionary<string, string> par = new Dictionary<string, string>();
                par.Add("@ART", DgCatalogo .Rows[e.RowIndex].Cells [0].Value .ToString ().Trim ());

                string query = "SELECT COD_DIS,DESCRIPCION,ACTIVO FROM " +
                "Paut_FnArtEquiv (@ART) ";

                DataTable TbDatos = _Sql.ExecuteQuery(query, par);

                DgArt.Rows.Clear();
                if (TbDatos.Rows.Count > 0)
                {
                  DgArt.Rows.Add(TbDatos.Rows[0]["COD_DIS"], TbDatos.Rows[0]["DESCRIPCION"], TbDatos.Rows[0]["ACTIVO"]);

                  habilitar(false);
                }
                else
                {
                    DgArt.Rows.Add("", "", "");
                    habilitar(true);
                }
                  
            }
            catch (Exception Ex)
            {

                _Desing.SendMessage(Ex.Message, "1");
            }
        }


        private void habilitar(bool estado) {
            GrpAct.Enabled = estado;          
            GrpSearch.Enabled = estado;
        }

      

        private void DgCatalogo_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            buscarEquivalente(e);
        }

        private void DgArt_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
           try
           {
            
            if (e.RowIndex >=0)
            {
                string dato = DgArt .Rows[e.RowIndex ].Cells [0].Value ==null ? "" :  DgArt .Rows[e.RowIndex ].Cells [0].Value.ToString ().Trim ();
                if (dato == "*" && dato != "")
                {
                    DataTable TbCatalogo = _Sql.ExecuteQuery("SELECT ARTICULO,DESCRIPCION,ACTIVO FROM Pdd_IArtDist");
                    FrmBusqueda FrmB = new FrmBusqueda(1, TbCatalogo);
                    FrmB.ShowDialog();
                    if (FrmB.Codigo != "")
                    {
                        buscarArticulo(FrmB.Codigo);
                    }
                }
                else {
                    if (dato =="")
                    {                       
                        limpiarlinea();
                    }
                }
            }

            }
            catch (Exception Ex)
            {

                _Desing.SendMessage(Ex.Message, "1");
            }
        }


        private void buscarArticulo(string articulo) {

            Dictionary<string, string> par = new Dictionary<string, string>();
            par.Add("@ART", articulo);
            DataTable TbCatalogo = _Sql.ExecuteQuery("SELECT ARTICULO,DESCRIPCION,ACTIVO FROM Paut_VwArtDist WHERE ARTICULO=@ART", par);
            if (TbCatalogo.Rows.Count > 0)
            {
                DgArt.Rows[0].Cells[0].Value = TbCatalogo.Rows[0]["ARTICULO"];
                DgArt.Rows[0].Cells[1].Value = TbCatalogo.Rows[0]["DESCRIPCION"];
                DgArt.Rows[0].Cells[2].Value = TbCatalogo.Rows[0]["ACTIVO"];

               
            }
            else {
                _Desing.SendMessage("Articulo no Encontrado", "3");
                limpiarlinea();
            }
        }

        private void limpiarlinea() {
            DgArt.Rows[0].Cells[0].Value = "";
            DgArt.Rows[0].Cells[1].Value = "";
            DgArt.Rows[0].Cells[2].Value = "";
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            limpiarlinea();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            
           try
           {

               if (DgArt.Rows[0].Cells[1].Value.ToString().Trim() != "" && DgArt.Rows[0].Cells[0].Value.ToString().Trim() != "*")
               {
                   Dictionary<string, string> par = new Dictionary<string, string>();
                   par.Add("@int", DgCatalogo.Rows[DgCatalogo.CurrentCell.RowIndex].Cells[0].Value.ToString().Trim());
                   par.Add("@dist", DgArt.Rows[0].Cells[0].Value.ToString().Trim());

                   string query = "INSERT INTO Paut_ArticuloAsoc (COD_INT,COD_DIS) " +
                                  " VALUES ( @int  , @dist )";
                   bool rs = _Sql.ExecuteNoResult(query, par);
                   if (rs == true)
                   {
                       _Desing.SendMessage("Guardado Correctamente", "2");
                       habilitar(false);
                   }
               }
               else {
                   _Desing.SendMessage("Por favor, elija un articulo", "3");               
               }

            }
            catch (Exception Ex)
            {

                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable TbCatalogo = _Sql.ExecuteQuery("SELECT ARTICULO,DESCRIPCION,ACTIVO FROM Paut_VwArtDist ");
                  FrmBusqueda FrmB = new FrmBusqueda(1, TbCatalogo);
                  if (RbCodigo.Checked)
                  {
                      FrmB.textBox1.Text = DgCatalogo.Rows[DgCatalogo.CurrentCell.RowIndex].Cells[0].Value.ToString().Trim();
                  }
                  else {
                      if (RbDesc .Checked)
                      {
                          FrmB.textBox1.Text = DgCatalogo.Rows[DgCatalogo.CurrentCell.RowIndex].Cells[1].Value.ToString().Trim();
                      }
                  }

                  FrmB.ShowDialog();
                  if (FrmB.Codigo != "")
                  {
                     buscarArticulo(FrmB.Codigo);
                  }           

            }
            catch (Exception Ex)
            {

                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void TxtCla4_DoubleClick(object sender, EventArgs e)
        {
            FrmPedAuto frm = new FrmPedAuto();
            frm.BuscaClasificacion(TxtCla4, 4);
        }

        private void TxtCla6_DoubleClick(object sender, EventArgs e)
        {
            FrmPedAuto frm = new FrmPedAuto();
            frm.BuscaClasificacion(TxtCla6, 6);
        }

        
    }//fin clase
}
