﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PedAuto.Formularios
{
    public partial class FrmPrincipal : PedAuto.Formularios.FrmBase
    {
        public MdiClient ctlMDI { get; set; }
        
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        #region metodos

            public bool  control_pantalla(string pantalla)
            {
                bool a = false; 
                try
                {                   
                    DataTable tb = _Sql.ExecuteQuery(string.Format("SELECT  * FROM Paut_control_pantalla where username = '" + Clases.Statics.usuario + "' and pantalla= '"+ pantalla +"'  and sociedad = '"+ Clases.Statics.sociedad +"'"));

                    if (tb.Rows.Count > 0)
                    {
                        _Desing.SendMessage("Esta opción esta siendo utilizada por otro usuario", "1");
                        return a;
                    }
                    else
                    {
                        a = true;
                        string query = string.Format("exec Paut_spControlAcceso 1, '" + Clases.Statics.usuario + "',  '" + pantalla + "', '" + Clases.Statics.sociedad + "' ");
                        _Sql.ExecuteNoResult(query);
                    }
                    
                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
                return a;
            }
        
        #endregion

        private void configuracionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmConfig FrmC = new FrmConfig();
            _Desing.ShowAsForm(FrmC, "");
       
        }
     
        //public void cargarSucursal(Clases.DB Query, ComboBox cmbEmp)
        //{
        //    string sql = " SELECT  codsoc ,modulo FROM Pa_VwEmpresas ORDER BY MODULO ";
        //    DataTable TbSoc = Query.ExecuteQuery(sql);
        //    cmbEmp.DisplayMember = "modulo";
        //    cmbEmp.ValueMember = "codsoc";
        //    cmbEmp.DataSource = TbSoc;       
          
        //}

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            try
            {
                foreach (Control ctl in this.Controls)
                {
                    if (ctl .GetType().Name .ToString () == "MdiClient")
                    {
                        ctlMDI = (MdiClient)ctl;

                        ctlMDI.BackColor = this.BackColor;
                    }                   
                }

                if (Properties.Settings.Default.autServer == null )
                {
                    configuracionToolStripMenuItem_Click(null, null);
                }
                else 
                {   
                    FrmLogueo FrmL = new FrmLogueo(this);
                    _Desing.ShowAsForm(FrmL, "");                 
                }
            }
            catch (Exception Ex)
            {

                _Desing.SendMessage(Ex.Message, "1");
            }
        }         
   
        private void TxtSucursal_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void TxtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void configuracionToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Clases.Statics.pantalla_configuracion = "Configuración";
            if (control_pantalla(Clases.Statics.pantalla_configuracion) == true)
            {
                FrmConfig FrmC = new FrmConfig();
                _Desing.ShowAsForm(FrmC, "Configuración");
            }          
        }
              
        private void PAUT_Click(object sender, EventArgs e)
        {
            Clases.Statics.pantalla_paut = "Pedidos Automaticos";
            if (control_pantalla(Clases.Statics.pantalla_paut) == true)
            {
                FrmPaut FrmP = new FrmPaut();
                _Desing.ShowAsParentForm(FrmP, this, "Pedidos Automaticos");
            }                      
        }

        private void UsuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clases.Statics.pantalla_registro = "Registro de Acceso";
            if (control_pantalla(Clases.Statics.pantalla_registro) == true)
            {
                FrmUsuario FrmP = new FrmUsuario();
                _Desing.ShowAsForm(FrmP, "Registro de Acceso");
            }                 
        }                    

        private void pedidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clases.Statics.pantalla_consulta = "Consulta de Pedidos";
            if (control_pantalla(Clases.Statics.pantalla_consulta) == true)
            {
                FrmConsulta_ped FrmP = new FrmConsulta_ped();
                _Desing.ShowAsForm(FrmP, "Consulta de Pedidos");
            }             
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clases.Statics.usuario = "";
            Clases.Statics.conjunto = "";
            Clases.Statics.empresa = "";
            this.Close();
        }

        private void exToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Clases.Statics.pantalla_excluir = "Excluir Articulos";
                if (control_pantalla(Clases.Statics.pantalla_excluir) == true)
                {
                    FrmExcluirArticulos FrmP = new FrmExcluirArticulos();
                    _Desing.ShowAsParentForm(FrmP, this, "Excluir Artículos del Sistema");
                }
            }
            catch (Exception Ex)
            {

                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void tool_Salir_Click(object sender, EventArgs e)
        {
            try
            {
                string query = string.Format("exec Paut_spControlAcceso 3, '" + Clases.Statics.usuario + "',  '', '"+ Clases.Statics.sociedad +"' ");
                _Sql.ExecuteNoResult(query);
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void ToolsInventario_Click(object sender, EventArgs e)
        {
            FrmCompInv FrmP = new FrmCompInv();
            _Desing.ShowAsParentForm(FrmP, this, "Inventario");
        }

       
    }
}
