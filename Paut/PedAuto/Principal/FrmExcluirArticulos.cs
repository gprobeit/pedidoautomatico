﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PedAuto.Clases;
using PedAuto.Formularios;
using System.Data.SqlClient;
using System.Configuration;

namespace PedAuto.Formularios
{
    public partial class FrmExcluirArticulos : Form
    {
        public FrmExcluirArticulos()
        {
            InitializeComponent();
        }

        #region var
            public static string correlativo = "";
            public static int estado = 3;
            public static string articulo = "";
            public static string sucursal = "";
            public static string descripcion = "";
            public static string cod_proveedor = "";
            Desing _Desing = new Desing();
            public DB _Sql = new DB();   
        #endregion

        #region metodos

            public void control_pantalla(string pantalla)
            {
                try
                {
                    string query = string.Format("exec Paut_spControlAcceso 2, '" + Clases.Statics.usuario + "',  '" + pantalla + "', '' ");
                    _Sql.ExecuteNoResult(query);

                }
                catch (Exception Ex)
                {
                    _Desing.SendMessage(Ex.Message, "1");
                }
            }

            public void Filtrar()
            {
            try
            {
                //string query = " SELECT distinct E.Articulo,  A.DESCRIPCION, A.COD_PROVEEDOR, S.codsuc, S.nomsuc as Sucursal, CASE WHEN E.estado = 1 then 'Activo' ELSE 'Inactivo' end Estado, E.correlativo" +
                //                " FROM PAUT_excluir_articulos AS E INNER JOIN Paut_VwCatalogoArticulos AS A ON E.articulo = A.ARTICULO " +
                //                "   INNER JOIN Paut_VwSucursal AS S ON E.sucursal = S.codsuc " +
                //                " WHERE ('" + Clases.Statics.conjunto + "') like '%'+A.CONJUNTO+'%' AND E.ARTICULO LIKE '%" + articulo + "%' " +
                //                "   AND A.DESCRIPCION LIKE '%" + descripcion + "%'	AND A.COD_PROVEEDOR LIKE '%" + cod_proveedor + "%' ";

                //if (estado != 3)
                //{
                //    query += " and estado = " + estado;
                //}

                //if (Clases.Statics.conjunto == "4") // PARA TBS
                //{
                //    query += " AND E.sucursal IN(" + sucursal + ")";// AND fecha_crea > '2022-01-01'";
                //}
                //else   // VIDALS Y SENTO
                //{
                //    if (Clases.Statics.usuario_sucursal == "01")
                //    {
                //        query += " AND E.sucursal LIKE '%" + sucursal + "%' ";
                //    }
                //    else
                //    {
                //        query += " AND E.sucursal LIKE '%" + Clases.Statics.usuario_sucursal + "%' ";
                //    }
                //}


                string suc = Clases.Statics.usuario_sucursal;

                SqlDataAdapter da = new SqlDataAdapter();
                DataTable dt = new DataTable();

                _Sql.connect();

                SqlCommand sql_cmnd = new SqlCommand("Paut_spArticulosBloqueados", _Sql.connection);
                //string procedure = "GPROBE.dbo.US_SP_Usuario_B";
                sql_cmnd.CommandType = CommandType.StoredProcedure;


                sql_cmnd.Parameters.Add(new SqlParameter("@conjunto", Clases.Statics.conjunto));
                sql_cmnd.Parameters.Add(new SqlParameter("@articulo", articulo));
                sql_cmnd.Parameters.Add(new SqlParameter("@descripcion", descripcion));
                sql_cmnd.Parameters.Add(new SqlParameter("@proveedor", cod_proveedor));

                if (Clases.Statics.conjunto == "4")
                {
                    sql_cmnd.Parameters.Add(new SqlParameter("@sucursal", Clases.Statics.usuario_sucursal.Replace("'", "")));

                }
                else if(Clases.Statics.conjunto == "1,2") {
                    sql_cmnd.Parameters.Add(new SqlParameter("@sucursal", Clases.Statics.usuario_sucursal.Replace("'", "")));
                }

                da.SelectCommand = sql_cmnd;

                da.Fill(dt);

                _Sql.disconect();

                 //DataTable dt = _Sql.ExecuteQuery(query);


                    if (dt.Rows.Count > 0)
                    {
                        Dg.DataSource = dt;
                        Dg.Columns["codsuc"].Visible = false;
                        lbl_total.Text = "Total de Registros: " + Dg.RowCount.ToString();
                    }
                    else
                    {
                        lbl_total.Text = "Total de Registros: 0";
                    }                    
                }
                catch (Exception ex)
                {
                    _Desing.SendMessage(ex.Message, "1");
                }
            }

            public void cargar_sucursal_TBS_privilegio()
            {
                if (Clases.Statics.conjunto == "4") // PARA TBS
                {
                    sucursal = "";
                    foreach (DataRow dr in Clases.Statics.TbAccesos.Rows)
                    {
                        if (dr[0].ToString().Trim() == "8.1.1")
                        {
                            sucursal = "'TNDMTS'";
                        }
                        else if (dr[0].ToString().Trim() == "8.1.2")
                        {
                            sucursal += (sucursal == "") ? "'TNDLMT'" : "," + "'TNDLMT'";
                        }
                        else if (dr[0].ToString().Trim() == "8.1.3")
                        {
                            sucursal += (sucursal == "") ? "'TNDMRT'" : "," + "'TNDMRT'";
                        }
                        else if (dr[0].ToString().Trim() == "8.1.4")
                        {
                            sucursal += (sucursal == "") ? "'TNDSM1'" : "," + "'TNDSM1'";
                        }
                    else if (dr[0].ToString().Trim() == "8.1.5")
                    {
                        sucursal += (sucursal == "") ? "'TNDPSV'" : "," + "'TNDPSV'";
                    }
                    else if (dr[0].ToString().Trim() == "8.1.6")
                    {
                        sucursal += (sucursal == "") ? "'TNDSSO'" : "," + "'TNDSSO'";
                    }
                }
                }  
            }

        #endregion

        private void rbb_Nuevo_Click(object sender, EventArgs e)
        {
            try
            {
                string accion = "I";
                FrmExcluirArticulos_UID frm = new FrmExcluirArticulos_UID(accion);
                frm.ShowDialog();
                Filtrar();
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void rbb_Filtrar_Click(object sender, EventArgs e)
        {
            try
            {
                FrmExcluirArticulos_B frm = new FrmExcluirArticulos_B();
                frm.ShowDialog();
                Filtrar();
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void rbb_Actualizar_Click(object sender, EventArgs e)
        {
            try
            {
                Filtrar();
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }   
        }

        private void FrmExcluirArticulos_FormClosed(object sender, FormClosedEventArgs e)
        {
            control_pantalla(Clases.Statics.pantalla_excluir);
        }

        private void FrmExcluirArticulos_Load(object sender, EventArgs e)
        {
            try 
	        {
                if (Clases.Statics.usuario_sucursal != "")
                {
                    articulo = "";
                    cargar_sucursal_TBS_privilegio();  
                    estado = 3;
                    cod_proveedor = "";
                    descripcion = "";
                    Filtrar();
                }
                else
                {
                    _Desing.SendMessage("El usuario no posee sucursal", "1");
                    this.Close();
                }                
	        }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }         
        }

        private void Dg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                string accion = "U";
                correlativo = Dg.CurrentRow.Cells["correlativo"].Value.ToString().Trim();
                articulo =  Dg.CurrentRow.Cells["articulo"].Value.ToString().Trim();
                sucursal = Dg.CurrentRow.Cells["codsuc"].Value.ToString().Trim();
                if (Dg.CurrentRow.Cells["estado"].Value.ToString().Trim() == "Activo" )
                {
                    estado = 1;
                }
                else
	            {
                    estado = 0;
	            }

                FrmExcluirArticulos_UID frm = new FrmExcluirArticulos_UID(accion);
                frm.ShowDialog();
                Filtrar();

            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            } 
        }

        private void rbb_Qfiltro_Click(object sender, EventArgs e)
        {
            try
            {
                articulo = "";
                sucursal = "";
                estado = 3;
                cod_proveedor = "";
                descripcion = "";
                Filtrar();
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            } 
        }

     
    }
}
