﻿namespace PedAuto.Formularios
{
    partial class FrmExcluirArticulos_UID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_articulo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmb_sucursal = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.btn_Buscar = new System.Windows.Forms.Button();
            this.btn_buscar_articulos = new System.Windows.Forms.Button();
            this.txt_codigo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.opt_inactivo = new System.Windows.Forms.RadioButton();
            this.opt_activo = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txt_articulo
            // 
            this.txt_articulo.Location = new System.Drawing.Point(87, 73);
            this.txt_articulo.MaxLength = 15;
            this.txt_articulo.Name = "txt_articulo";
            this.txt_articulo.Size = new System.Drawing.Size(163, 20);
            this.txt_articulo.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Articulo:";
            // 
            // cmb_sucursal
            // 
            this.cmb_sucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_sucursal.FormattingEnabled = true;
            this.cmb_sucursal.Location = new System.Drawing.Point(87, 43);
            this.cmb_sucursal.Name = "cmb_sucursal";
            this.cmb_sucursal.Size = new System.Drawing.Size(224, 21);
            this.cmb_sucursal.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Sucursal:";
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Image = global::PedAuto.Properties.Resources.borrar_small;
            this.btn_cancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancelar.Location = new System.Drawing.Point(162, 171);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(88, 38);
            this.btn_cancelar.TabIndex = 9;
            this.btn_cancelar.Text = "Cancelar";
            this.btn_cancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // btn_Buscar
            // 
            this.btn_Buscar.Image = global::PedAuto.Properties.Resources.save_small;
            this.btn_Buscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Buscar.Location = new System.Drawing.Point(54, 171);
            this.btn_Buscar.Name = "btn_Buscar";
            this.btn_Buscar.Size = new System.Drawing.Size(77, 38);
            this.btn_Buscar.TabIndex = 8;
            this.btn_Buscar.Text = "Guardar";
            this.btn_Buscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_Buscar.UseVisualStyleBackColor = true;
            this.btn_Buscar.Click += new System.EventHandler(this.btn_Buscar_Click);
            // 
            // btn_buscar_articulos
            // 
            this.btn_buscar_articulos.Image = global::PedAuto.Properties.Resources.buscar2_buscar_ampliar_icono_6486_16;
            this.btn_buscar_articulos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_buscar_articulos.Location = new System.Drawing.Point(256, 73);
            this.btn_buscar_articulos.Name = "btn_buscar_articulos";
            this.btn_buscar_articulos.Size = new System.Drawing.Size(23, 22);
            this.btn_buscar_articulos.TabIndex = 33;
            this.btn_buscar_articulos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_buscar_articulos.UseVisualStyleBackColor = true;
            this.btn_buscar_articulos.Click += new System.EventHandler(this.btn_buscar_articulos_Click);
            // 
            // txt_codigo
            // 
            this.txt_codigo.Location = new System.Drawing.Point(87, 15);
            this.txt_codigo.MaxLength = 15;
            this.txt_codigo.Name = "txt_codigo";
            this.txt_codigo.ReadOnly = true;
            this.txt_codigo.Size = new System.Drawing.Size(89, 20);
            this.txt_codigo.TabIndex = 35;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 34;
            this.label6.Text = "Codigo:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.opt_inactivo);
            this.groupBox1.Controls.Add(this.opt_activo);
            this.groupBox1.Location = new System.Drawing.Point(72, 110);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(152, 43);
            this.groupBox1.TabIndex = 36;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Estado";
            // 
            // opt_inactivo
            // 
            this.opt_inactivo.AutoSize = true;
            this.opt_inactivo.Location = new System.Drawing.Point(83, 19);
            this.opt_inactivo.Name = "opt_inactivo";
            this.opt_inactivo.Size = new System.Drawing.Size(63, 17);
            this.opt_inactivo.TabIndex = 34;
            this.opt_inactivo.TabStop = true;
            this.opt_inactivo.Text = "Inactivo";
            this.opt_inactivo.UseVisualStyleBackColor = true;
            // 
            // opt_activo
            // 
            this.opt_activo.AutoSize = true;
            this.opt_activo.Location = new System.Drawing.Point(10, 19);
            this.opt_activo.Name = "opt_activo";
            this.opt_activo.Size = new System.Drawing.Size(55, 17);
            this.opt_activo.TabIndex = 33;
            this.opt_activo.TabStop = true;
            this.opt_activo.Text = "Activo";
            this.opt_activo.UseVisualStyleBackColor = true;
            // 
            // FrmExcluirArticulos_UID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(333, 219);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txt_codigo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btn_buscar_articulos);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmb_sucursal);
            this.Controls.Add(this.txt_articulo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.btn_Buscar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmExcluirArticulos_UID";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.FrmExcluirArticulos_UID_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_articulo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.Button btn_Buscar;
        private System.Windows.Forms.ComboBox cmb_sucursal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_buscar_articulos;
        private System.Windows.Forms.TextBox txt_codigo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton opt_inactivo;
        private System.Windows.Forms.RadioButton opt_activo;
    }
}