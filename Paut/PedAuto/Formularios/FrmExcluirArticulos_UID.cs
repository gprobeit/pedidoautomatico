﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PedAuto.Clases;
using PedAuto.Formularios;

namespace PedAuto.Formularios
{
    public partial class FrmExcluirArticulos_UID : Form
    {
        public FrmExcluirArticulos_UID(string accion)
        {
            InitializeComponent();
            _accion = accion;
        }

        #region var
            string _accion;
            Desing _Desing = new Desing();
            public DB _Sql = new DB();
        #endregion
           
        #region metodos

            private void cargarSuc()
            {
                try
                {
                    DataTable TbSuc = new DataTable();
                    if (Clases.Statics.sociedad == "VS")
                    {
                        TbSuc = _Sql.ExecuteQuery(string.Format(" SELECT DISTINCT CODSUC,NOMSUC  FROM Paut_VwSucursal WHERE TIPO='VTA' and CONJUNTO IN (" + Clases.Statics.conjunto + ")"));
                    }
                    else
                    {
                        TbSuc = _Sql.ExecuteQuery(string.Format(" SELECT DISTINCT CODSUC,NOMSUC  FROM Paut_VwSucursal WHERE TIPO='VTA' AND CONJUNTO IN (" + Clases.Statics.conjunto + ")"));
                    }

                    DataRow row = TbSuc.NewRow();
                    row["CODSUC"] = "";
                    row["NOMSUC"] = "TODAS";
                    TbSuc.Rows.InsertAt(row, 0);

                    cmb_sucursal.DataSource = TbSuc;
                    cmb_sucursal.ValueMember = "CODSUC";
                    cmb_sucursal.DisplayMember = "NOMSUC";
                    cmb_sucursal.SelectedIndex = 0;

                    if (Clases.Statics.usuario_sucursal != "01")
                    {
                        cmb_sucursal.Enabled = false;
                        cmb_sucursal.SelectedValue = Clases.Statics.usuario_sucursal;
                    }
                    else
                    {
                        cmb_sucursal.Enabled = true;
                    }
                }
                catch (Exception ex)
                {
                    _Desing.SendMessage(ex.Message, "1");
                }
            }

            private void guardar_modificar()
            {
                try
                {
                    DataTable TbSuc = new DataTable();
                    TbSuc = _Sql.ExecuteQuery(string.Format(" SELECT DISTINCT CODSUC,NOMSUC  FROM Paut_VwSucursal WHERE TIPO='VTA' and CONJUNTO IN (" + Clases.Statics.conjunto + ")"));
                    Dictionary<string, string> parm = new Dictionary<string, string>();
                    string query = "";
                    int i = 0;
                    int estado = 0;

                    if (_accion == "I")
                    {
                        if (cmb_sucursal.SelectedValue != "")
                        {
                            parm.Clear();
                            estado = 0;
                            if (opt_activo.Checked == true)
                            {
                                estado = 1;
                            }
                            else
                            {
                                estado = 0;
                            }

                            query += " INSERT INTO GLOBAL.[dbo].[PAUT_excluir_articulos] " +
                            " (articulo, sucursal, estado, usuario_crea, fecha_crea) VALUES" +
                            " ('" + txt_articulo.Text.Trim() + "', '" + cmb_sucursal.SelectedValue.ToString() + "', " + estado + ", '" + Clases.Statics.usuario + "', GETDATE()) ";                                                       
                        }
                        else
                        {                            
                            foreach (DataRow row in TbSuc.Rows)
                            {
                                parm.Clear();                               
                                
                                if (opt_activo.Checked == true)
                                {
                                    estado = 1;
                                }
                                else
                                {
                                    estado = 0;
                                }

                                query += " INSERT INTO GLOBAL.[dbo].[PAUT_excluir_articulos] " +
                                " (articulo, sucursal, estado, usuario_crea, fecha_crea) VALUES" +
                                " ('" + txt_articulo.Text.Trim() + "', '" + TbSuc.Rows[i]["codsuc"].ToString() + "', " + estado + ", '" + Clases.Statics.usuario + "', GETDATE()) ";
                                i++;
                            }
                        }
                    }
                    else if (_accion == "U")
                    {
                        parm.Clear();
                        estado = 0;
                        if (opt_activo.Checked == true)
                        {
                            estado = 1;
                        }
                        else
                        {
                            estado = 0;
                        }

                        query += "DELETE FROM GLOBAL.[dbo].[PAUT_excluir_articulos] WHERE correlativo = " + txt_codigo.Text.Trim() + "";

                        //query += "UPDATE GLOBAL.[dbo].[PAUT_excluir_articulos] set estado = " + estado + " , usuario_modi = '" + Clases.Statics.usuario + "' , " +
                        //         " fecha_modi = GETDATE() WHERE correlativo = " + txt_codigo.Text.Trim() + "";
                    }

                    bool result = _Sql.ExecuteNoResult(query);
                    if (result == true)
                    {
                        _Desing.SendMessage("Proceso Realizado con exito", "2");
                        FrmExcluirArticulos.articulo = "";
                        FrmExcluirArticulos.correlativo = "";
                        FrmExcluirArticulos.sucursal = "";
                        FrmExcluirArticulos.estado = 3;

                        this.Close();
                    }
                    else
                    {
                        _Desing.SendMessage("El proceso no realizado con exito", "3");
                    }
                         
                }
                catch (Exception ex)
                {
                    _Desing.SendMessage(ex.Message, "1");
                }
            }

            private bool validar_existe()
            {
                bool boo = false;
                try
                {
                    DataTable dt_existe = new DataTable();

                    if (cmb_sucursal.SelectedValue.ToString() != "")
                    {
                        dt_existe = _Sql.ExecuteQuery(" SELECT count(*) from PAUT_excluir_articulos WHERE articulo = '" + txt_articulo.Text.Trim() + "' and sucursal = '" + cmb_sucursal.SelectedValue.ToString() + "'");

                        if (_accion == "I")
                        {
                            if (Convert.ToInt16(dt_existe.Rows[0][0]) == 0)
                            {
                                boo = true;
                            }
                            else
                            {
                                _Desing.SendMessage("Articulo existente", "3");
                            }
                        }
                        else
                        {
                            boo = true;
                        }
                    }
                    else 
                    {
                        int i = 0;
                        DataTable TbSuc = _Sql.ExecuteQuery(string.Format(" SELECT DISTINCT CODSUC,NOMSUC  FROM Paut_VwSucursal WHERE TIPO='VTA' and CONJUNTO IN (" + Clases.Statics.conjunto + ")"));
                        
                        foreach (DataRow row in TbSuc.Rows)
                        {
                            string query = " SELECT count(*) from PAUT_excluir_articulos WHERE articulo = '" + txt_articulo.Text.Trim() + "' and sucursal = '" + TbSuc.Rows[i]["CODSUC"].ToString() + "'";

                            dt_existe = _Sql.ExecuteQuery(query);

                            if (_accion == "I")
                            {
                                if (Convert.ToInt16(dt_existe.Rows[0][0]) == 0)
                                {
                                    boo = true;
                                }
                                else
                                {
                                    _Desing.SendMessage("Articulo existente en esta lista, verificar", "3");
                                }
                            }
                            else
                            {
                                boo = true;
                            }
                            i++;
                        }
                    }
                   
                }
                catch (Exception ex)
                {
                    _Desing.SendMessage(ex.Message, "1");
                }
                return boo;
            }

            private bool validar_articulo()
            {
                bool boo = false;
                try
                {
                    if (txt_articulo.Text.Trim() != "")
                    {
                        DataTable dt = _Sql.ExecuteQuery(" SELECT articulo from Paut_VwCatalogoArticulos WHERE articulo = '" + txt_articulo.Text.Trim() + "'");

                        if (dt.Rows.Count > 0)
                        {
                            boo = true;
                        }
                    }
                    else
                    {
                        _Desing.SendMessage("Ingrese un articulo", "3");
                    }
                }
                catch (Exception ex)
                {
                    _Desing.SendMessage(ex.Message, "1");
                }
                return boo;
            }

        #endregion

        private void FrmExcluirArticulos_UID_Load(object sender, EventArgs e)
        {
            try
            {
                cargarSuc();
                if (_accion == "I")
                {
                    opt_activo.Checked = true;
                    opt_inactivo.Enabled = false;
                }
                else if (_accion == "U")
                {
                    cmb_sucursal.Enabled = false;
                    txt_articulo.Enabled = false;
                    txt_codigo.Text = FrmExcluirArticulos.correlativo;
                    txt_articulo.Text = FrmExcluirArticulos.articulo;
                    cmb_sucursal.SelectedValue = FrmExcluirArticulos.sucursal;
                    if (FrmExcluirArticulos.estado == 1)
                    {
                        opt_activo.Checked = true;
                    }
                    else
                    {
                        opt_inactivo.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _Desing.SendMessage(ex.Message, "1");
            }
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_buscar_articulos_Click(object sender, EventArgs e)
        {
            try
            {
                FrmBuscarArt frm = new FrmBuscarArt(txt_articulo);
                frm.ShowDialog();
                FrmBuscarArt frm_a = new FrmBuscarArt(txt_articulo);
            }
            catch (Exception ex)
            {
                _Desing.SendMessage(ex.Message, "1");
            }
        }

        private void btn_Buscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validar_articulo() == true)
                {
                    if (validar_existe() == true)
                    {
                        guardar_modificar();
                    }
                }
                //else
                //{
                //    _Desing.SendMessage("Verifique Artículo", "3");
                //}
            }
            catch (Exception ex)
            {
                _Desing.SendMessage(ex.Message, "1");
            }
        }
    }
}
