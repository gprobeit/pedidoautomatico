﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using PedAuto.Clases;

namespace PedAuto
{
    public partial class FrmReporte : Form
    {
        public FrmReporte(DataTable v_dt)
        {
            InitializeComponent();
            dt = v_dt;
        }

        #region var
            DataTable dt = new DataTable();
            public Desing _Desing = new Desing();
            public string cod_sucursal  {get; set;}
        #endregion
        
        #region metodos
        
        #endregion

        private void FrmReporte_Load(object sender, EventArgs e)
        {
            try
            {
                ReportDataSource rds = new ReportDataSource();
                ReportParameter[] parameters = new ReportParameter[1];
                parameters[0] = new ReportParameter("parametro_suc", cod_sucursal);

                if (Clases.Statics.conjunto == "3")
                {
                    rds.Name = "Ds_Cargar_pendientes";
                    rds.Value = dt;
                    reportViewer1.LocalReport.DataSources.Clear();                    
                    reportViewer1.LocalReport.DataSources.Add(rds);

                    this.reportViewer1.LocalReport.ReportEmbeddedResource = "PedAuto.Informe.rpt_Cargar_pendientes.rdlc";
                }
                else
                {
                    rds.Name = "Ds_Cargar_pendientes_VS";
                    rds.Value = dt;
                    reportViewer1.LocalReport.DataSources.Clear();                    
                    reportViewer1.LocalReport.DataSources.Add(rds);
                   
                    this.reportViewer1.LocalReport.ReportEmbeddedResource = "PedAuto.Informe.rpt_Cargar_pendientes_VS.rdlc";
                }              

                reportViewer1.LocalReport.Refresh();
                reportViewer1.LocalReport.SetParameters(parameters);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }           
        }
    }
}
