﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PedAuto.Clases;

namespace PedAuto.Formularios
{
    public partial class FrmBuscarArticulo : Form
    {
        public FrmBuscarArticulo()
        {
            InitializeComponent();
        }

        #region var
            public Desing _Desing = new Desing();
            public string articulo { get ; set; }
            public string proveedor { get; set; }
            public string descripcion { get; set; }      
        #endregion

        #region metodos

           
        #endregion

        private void FrmBuscarArticulo_Load(object sender, EventArgs e)
        {            
            txt_articulo.Text = articulo;
            txt_proveedor.Text = proveedor;
            txt_descripcion.Text = descripcion;
        }
            

        private void btn_Buscar_Click(object sender, EventArgs e)
        {            
            try
            {                
                articulo = txt_articulo.Text.Trim();               
                proveedor = txt_proveedor.Text.Trim();
                descripcion = txt_descripcion.Text.Trim();        
                Close();
            }
            catch (Exception Ex)
            {
                _Desing.SendMessage(Ex.Message, "1");
            }
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
